/****************************************************************************
 *  Copyright (C) 2018 RoboMaster.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of�
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.� See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
/** @file imu_task.c
 *  @version 1.1
 *  @date June 2017
 *
 *  @brief imu attitude calculation task
 *
 *  @copyright 2017 DJI RoboMaster. All rights reserved.
 *
 */
  
#include "imu_task.h"
#include "cmsis_os.h"
#include "bsp_imu.h"
#include "pid.h"
#include "math.h"
#include "MadgwickAHRS.h"

UBaseType_t imu_stack_surplus;

#define ACCEL_MULT 0.00239257813f

/* imu task global parameter */
mpu_data_t     mpu_data;
imu_data_t     imu;
imu_attitude_t atti;
imu_xyz_accel_t xyz_accel;

/* imu task static parameter */

// from MadgwickAHRS.h
extern volatile float q0, q1, q2, q3;	// quaternion of sensor frame relative to auxiliary frame

static volatile uint32_t last_update, now_update;
static volatile float exInt, eyInt, ezInt;
static volatile float gx, gy, gz, ax, ay, az, mx, my, mz;   //

#define BOARD_DOWN 1   //

static void init_quaternion(void)
{
  int16_t hx, hy;
  float temp;

  hx = imu.mx;
  hy = imu.my;

  if (hy != 0)
    temp = hx/hy;
  else
    return ;

  #ifdef BOARD_DOWN
  if(hx<0 && hy <0)   //OK
  {
    if(fabs(temp) >= 1)
    {
      q0 = -0.005;
      q1 = -0.199;
      q2 = 0.979;
      q3 = -0.0089;
    }
    else
    {
      q0 = -0.008;
      q1 = -0.555;
      q2 = 0.83;
      q3 = -0.002;
    }
    
  }
  else if (hx<0 && hy > 0) //OK
  {
    if(fabs(temp) >= 1)   
    {
      q0 = 0.005;
      q1 = -0.199;
      q2 = -0.978;
      q3 = 0.012;
    }
    else
    {
      q0 = 0.005;
      q1 = -0.553;
      q2 = -0.83;
      q3 = -0.0023;
    }
    
  }
  else if (hx > 0 && hy > 0)   //OK
  {
    if(fabs(temp) >= 1)
    {
      q0 = 0.0012;
      q1 = -0.978;
      q2 = -0.199;
      q3 = -0.005;
    }
    else
    {
      q0 = 0.0023;
      q1 = -0.83;
      q2 = -0.553;
      q3 = 0.0023;
    }
    
  }
  else if (hx > 0 && hy < 0)     //OK
  {
    if(fabs(temp) >= 1)
    {
      q0 = 0.0025;
      q1 = 0.978;
      q2 = -0.199;
      q3 = 0.008;
    }
    else
    {
      q0 = 0.0025;
      q1 = 0.83;
      q2 = -0.56;
      q3 = 0.0045;
    }
  }
  #else
    if(hx<0 && hy <0)
  {
    if(fabs(temp) >= 1)
    {
      q0 = 0.195;
      q1 = -0.015;
      q2 = 0.0043;
      q3 = 0.979;
    }
    else
    {
      q0 = 0.555;
      q1 = -0.015;
      q2 = 0.006;
      q3 = 0.829;
    }
    
  }
  else if (hx<0 && hy > 0)
  {
    if(fabs(temp) >= 1)
    {
      q0 = -0.193;
      q1 = -0.009;
      q2 = -0.006;
      q3 = 0.979;
    }
    else
    {
      q0 = -0.552;
      q1 = -0.0048;
      q2 = -0.0115;
      q3 = 0.8313;
    }
    
  }
  else if (hx>0 && hy > 0)
  {
    if(fabs(temp) >= 1)
    {
      q0 = -0.9785;
      q1 = 0.008;
      q2 = -0.02;
      q3 = 0.195;
    }
    else
    {
      q0 = -0.9828;
      q1 = 0.002;
      q2 = -0.0167;
      q3 = 0.5557;
    }
    
  }
  else if (hx > 0 && hy < 0)
  {
    if(fabs(temp) >= 1)
    {
      q0 = -0.979;
      q1 = 0.0116;
      q2 = -0.0167;
      q3 = -0.195;
    }
    else
    {
      q0 = -0.83;
      q1 = 0.014;
      q2 = -0.012;
      q3 = -0.556;
    }
  }
  #endif
}

static void imu_attitude_update(void)
{

	imu.rol = RAD_TO_DEG( atan2(2*q2*q3 + 2*q0*q1, -2*q1*q1 - 2*q2*q2 + 1) ); // roll       -pi----pi
  imu.pit = RAD_TO_DEG( asin(-2*q1*q3 + 2*q0*q2) );                         // pitch    -pi/2----pi/2 
  imu.yaw = RAD_TO_DEG( atan2(2*q1*q2 + 2*q0*q3, -2*q2*q2 - 2*q3*q3 + 1) ); // yaw        -pi----pi

	imu.tilt_angle = RAD_TO_DEG( acos(cos(imu_pit_rad()) * cos(imu_rol_rad())) );
	
  if (imu.yaw - atti.last_yaw > 330)
    atti.yaw_cnt--;
  else if (imu.yaw - atti.last_yaw < -330)
    atti.yaw_cnt++;
  
  atti.last_yaw = imu.yaw;
  
  atti.current_yaw   = imu.yaw + atti.yaw_cnt*360;
  atti.current_pitch = -imu.rol;
  atti.current_roll  = imu.pit;
}

static void imu_xyz_accel_update(void)
{
	float sinA = sinf(imu_yaw_rad());
	float cosA = cosf(imu_yaw_rad());
	float sinB = sinf(imu_pit_rad());
	float cosB = cosf(imu_pit_rad());
	float sinC = sinf(imu_rol_rad());
	float cosC = cosf(imu_rol_rad());
	float sinAsinB = sinA * sinB;
	float cosAsinB = cosA * sinB;
	
	int16_t ax_corr = imu.ax;
	int16_t ay_corr = imu.ay;
	int16_t az_corr = imu.az;
	
	xyz_accel.x = ACCEL_MULT * ( cosA * cosB * ax_corr
															+ (cosAsinB * sinC - sinA * cosC) * ay_corr 
															+ (cosAsinB * cosC + sinA * sinC) * az_corr );
	xyz_accel.y = ACCEL_MULT * ( sinA * cosB * ax_corr
															+ (sinAsinB * sinC + cosA * cosC) * ay_corr 
															+ (sinAsinB * cosC - cosA * sinC) * az_corr );
	xyz_accel.z = ACCEL_MULT * ( -sinB * ax_corr
															+ cosB * sinC * ay_corr 
															+ cosB * cosC * az_corr ) - 9.8f;
}

void imu_param_init(void)
{
  init_quaternion();
  imu_temp_ctrl_init();
}

float yaw = 0.0f;

uint32_t imu_time_last;
int imu_time_ms;
float prev_yaw;
void imu_task(void const *argu)
{
  
  uint32_t imu_wake_time = osKernelSysTick();
  while(1)
  {
    imu_time_ms = osKernelSysTick() - imu_time_last;
    imu_time_last = osKernelSysTick();
    
    imu_temp_keep();
    
    mpu_get_data();
		MadgwickAHRSupdate(imu.wx, imu.wy, imu.wz, imu.ax, imu.ay, imu.az, imu.mx, imu.my, imu.mz);

    imu_attitude_update();
		imu_xyz_accel_update();
		
		osDelay(1);
  }

}

void imu_temp_ctrl_init(void)
{
  PID_struct_init(&pid_imu_tmp, POSITION_PID, 300, 150,
                  10, 1, 0);
}

void imu_temp_keep(void)
{
  pid_calc(&pid_imu_tmp, imu.temp, imu.temp_ref);
}
	
float imu_yaw_rad() {
	return DEG_TO_RAD(imu.yaw);
}

float imu_pit_rad() {
	return DEG_TO_RAD(imu.pit);
}

float imu_rol_rad() {
	return DEG_TO_RAD(imu.rol);
}
