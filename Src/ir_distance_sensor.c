#include "ir_distance_sensor.h"
#include "gpio.h"
#include "math_user_utils.h"
#include "math.h"

#define SHORT_IR_DISTANCE(volt) (13.0f * pow(volt, -1) * INCHES_TO_MM_SCALER)
#define LONG_IR_DISTANCE(volt) (62.808f * pow(volt, -1.577f) * 10)

#define MAX_READ_VOLT (3.7f)
#define MIN_READ_VOLT (0.4f)
#define INCHES_TO_MM_SCALER (25.4f)

// Converting the values of the IR distance sensor to centimeters
// Returns -1, if the conversion did not succeed.
float ir_get_distance(uint16_t pin, ir_distance_sensor_t ir_sensor) {
	float volt_read = GPIO_Analog_Read(pin) * MV_TO_V_SCALER;
	if (volt_read > MAX_READ_VOLT || volt_read < MIN_READ_VOLT)	{
		return -1;
	}
	if (ir_sensor == SHORT_IR_SENSOR)
		return SHORT_IR_DISTANCE(volt_read);
	else if (ir_sensor == LONG_IR_SENSOR)
		return LONG_IR_DISTANCE(volt_read);
	else
		return -1;
}
