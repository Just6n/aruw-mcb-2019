#include "position_motor_control.h"
#include "control_mode.h"

typedef struct {
	dji_motor_t* motor;
	
	// variables initially set by the user BEFORE calling position_control_motor_init()
	int32_t max_ang; // min unwrapped angle // to have no limiting, set min and max angle to the same number
	int32_t min_ang; // max unwrapped angle
	uint8_t gear_ratio; // motor gear ratio
	int32_t zero_angle;
	bool motor_inverted; // are the motor angle values inverse of the true motor angle	
	float ang_setpoint; // user set angle to rotate the motor
	float shift_ang; // Way to readjust coordinate system to account for a nonzero zero_angle
	bool position_motor_inited;
	float avg_output; // accumulator for exponential smoothing of output
	float avg_output_factor; // factor for averaging current and previous value, smaller factor = more smoothing, between 0 and 1
} position_control_motor_t;	// if you want to use this, you MUST initialize all these variables other than the dji_motor_t

position_control_motor_t position_motor_store[NUMBER_POSITION_MOTORS];

// helper angle to encoder_wrapped and encoder_wrapped to angle calculations taking into account gear ratio
static float position_motor_ang_to_enc(float ang, position_control_motor_t* motor) {
	return (ang) * ENC_MAX * (motor)->gear_ratio / (360.0f);
}

static float position_motor_enc_to_ang(float enc_val, position_control_motor_t* motor) {
	return (enc_val) * 360.0f / (ENC_MAX * motor->gear_ratio);
}

bool check_id(position_control_motor_id_t id) {
	return id < NUMBER_POSITION_MOTORS;
}

/*
 * Initialize everything to base values.
 */
motor_control_error_t position_motor_init(position_control_motor_id_t id, dji_motor_t* dji_motor) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR; 
	} else if (position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}	
	position_motor_store[id].motor = dji_motor;
	PID_struct_init(&position_motor_store[id].motor->pid, AVERAGING_PID, 20000, 0.0f, 0.0f, 0.0f, 0.0f);
	position_motor_store[id].zero_angle = 0;
	position_motor_store[id].max_ang = 0;
	position_motor_store[id].min_ang = 0;
	position_motor_store[id].gear_ratio = 1;
	position_motor_store[id].motor_inverted = false;
	position_motor_store[id].ang_setpoint = 0.0f;
	position_motor_store[id].shift_ang = 0.0f;
	position_motor_store[id].avg_output = 0.0f;
	position_motor_store[id].position_motor_inited = true;
	position_motor_store[id].avg_output_factor = 1.0f;
	return POSITION_MOTOR_OK;
}

motor_control_error_t position_motor_init_avg_output_factor(position_control_motor_id_t id, float factor) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	
	position_motor_store[id].avg_output_factor = factor;
	
	return POSITION_MOTOR_OK;
}

motor_control_error_t position_motor_init_pid(position_control_motor_id_t id, uint32_t pid_mode, float p, float i, float d) {
	if (!check_id(id) || p < 0 || i < 0 || d < 0) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}

	position_motor_store[id].motor->pid.p = p;
	position_motor_store[id].motor->pid.i = i;
	position_motor_store[id].motor->pid.d = d;
	position_motor_store[id].motor->pid.pid_mode = pid_mode;

	return POSITION_MOTOR_OK;
}

motor_control_error_t position_motor_set_masked_d(position_control_motor_id_t id, bool mask_d) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	}

	position_motor_store[id].motor->pid.mask_derivative_from_current_updates = mask_d;
	
	return POSITION_MOTOR_OK;
}

motor_control_error_t position_motor_set_integral_accum_limit(position_control_motor_id_t id, uint32_t accum_limit) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}

	position_motor_store[id].motor->pid.integral_limit = accum_limit;

	return POSITION_MOTOR_OK;	
}

/* 
 * Initialize shift angle for a particular motor, must be done after motor encoder values are being read, so must 
 * be done in a thread
 */
motor_control_error_t init_shift_ang(position_control_motor_id_t id, int16_t zero_encoder) {
    if (!check_id(id)) {
			return POSITION_MOTOR_ID_ERROR;
		} else if (!position_motor_store[id].position_motor_inited) {
			return POSITION_MOTOR_INIT_ERROR;
		}
		
		if (zero_encoder != 0) {
			position_motor_store[id].shift_ang = position_motor_enc_to_ang(zero_encoder, &position_motor_store[id]) 
				- position_motor_store[id].zero_angle;
			if (position_motor_store[id].ang_setpoint - position_motor_get_ang_actual(id) < -180.0f) {
				position_motor_store[id].shift_ang += 360.0f;
			}
		} else {
			position_motor_store[id].shift_ang = position_motor_enc_to_ang(position_motor_store[id].motor->encoder_wrapped, &position_motor_store[id]) 
				- position_motor_store[id].zero_angle;
		}
		
		// wrap around the zero_angle
		if (position_motor_store[id].zero_angle + position_motor_get_ang_actual(id) < 0) {
			position_motor_store[id].motor->rotation_count++;
		}
			
    return POSITION_MOTOR_OK;
}

/*
 * User set specific motor angle constants
 */
motor_control_error_t position_motor_init_angles(position_control_motor_id_t id, int32_t max_ang, int32_t min_ang, int32_t zero_angle) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}

	if (max_ang < min_ang) {
		position_motor_store[id].max_ang = 0;
		position_motor_store[id].min_ang = 0;
		return POSITION_MOTOR_ID_ERROR;
	}
	if (zero_angle < min_ang || zero_angle > max_ang) {
		return POSITION_MOTOR_ID_ERROR;
	}
	
	position_motor_store[id].max_ang = max_ang;
	position_motor_store[id].min_ang = min_ang;
	position_motor_store[id].zero_angle = zero_angle;
	position_motor_store[id].ang_setpoint = zero_angle;
	return POSITION_MOTOR_OK;
}

/* 
 * user set motor_inverted 
 */
motor_control_error_t position_motor_init_inverted(position_control_motor_id_t id, bool inverted) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	position_motor_store[id].motor_inverted = inverted;
	return POSITION_MOTOR_OK;
}

/* 
 * If min and max angle have the same value, the motor setpoint will not be limited
 * Limits the motor setpoint to within the min and max bounds.
 * Returns true if desired_ang_setpoint is in bounds, inclusively, returns false if it is out of bounds
 */
motor_control_error_t set_ang_des(position_control_motor_id_t id, float desired_ang_setpoint) {
	if(!check_id(id)) {
        return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	if(position_motor_store[id].max_ang != position_motor_store[id].min_ang){	// if the min and max angle are the same, no limiting will occur
		if(desired_ang_setpoint < position_motor_store[id].min_ang){
			position_motor_store[id].ang_setpoint = position_motor_store[id].min_ang;
			return POSITION_MOTOR_SET_ANGLE_ERROR;
		} else if(desired_ang_setpoint > position_motor_store[id].max_ang){
			position_motor_store[id].ang_setpoint = position_motor_store[id].max_ang;
			return POSITION_MOTOR_SET_ANGLE_ERROR;
		}
	} 
	position_motor_store[id].ang_setpoint = desired_ang_setpoint;
	return POSITION_MOTOR_OK;
}

motor_control_error_t position_motor_run_pid(position_control_motor_id_t id){
	if(!check_id(id)) {
			return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	
	// insure the user did not forget to limit motor, this should be unnecesscary if the user 
	set_ang_des(id, position_motor_store[id].ang_setpoint);

	// find error
	float m_error = -position_motor_store[id].ang_setpoint + position_motor_get_ang_actual(id);

	float m_output_result = pid_calc_err(&position_motor_store[id].motor->pid, position_motor_ang_to_enc(m_error, &position_motor_store[id]));	

	// smoothing
	m_output_result = m_output_result * position_motor_store[id].avg_output_factor 
		+ (1 - position_motor_store[id].avg_output_factor) * (position_motor_store[id].avg_output);
	position_motor_store[id].avg_output = m_output_result;

	// set motor output
	if (robot_mode != KILL_MODE) {
		can_dji_motor_set(position_motor_store[id].motor, position_motor_store[id].motor_inverted ? m_output_result : -m_output_result);
	} else {
		can_dji_motor_set(position_motor_store[id].motor, 0);
	}
	return POSITION_MOTOR_OK;
}

float position_motor_get_ang_actual(position_control_motor_id_t id) {
	if(!check_id(id)) {
			return (float) POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	float ang = position_motor_enc_to_ang(position_motor_store[id].motor->encoder_unwrapped, (&position_motor_store[id])) - position_motor_store[id].shift_ang;
	return (position_motor_store[id].motor_inverted) ? 2 * position_motor_store[id].zero_angle - ang : ang;
}

int16_t position_motor_get_encoder_wrapped(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return (int16_t) POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].motor->encoder_wrapped;
}

float position_motor_get_curr_ang_setpoint(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return (float) POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].ang_setpoint;
}

bool position_motor_get_motor_inverted(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].motor_inverted;
}

int16_t position_motor_get_max_ang(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return (int16_t) POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].max_ang;
}

int16_t position_motor_get_min_ang(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return (int16_t) POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].min_ang;
}

int16_t position_motor_get_current_actual(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].motor->current_actual;
}

motor_control_error_t position_motor_init_gear_ratio(position_control_motor_id_t id, uint8_t gear_ratio) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	position_motor_store[id].gear_ratio = gear_ratio;
	return POSITION_MOTOR_OK;
}

motor_control_error_t position_motor_adjust_voltage_desired(position_control_motor_id_t id, int32_t offset_amount) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	position_motor_store[id].motor->voltage_desired += offset_amount;
	return POSITION_MOTOR_OK;
}

bool position_motor_get_motor_online(position_control_motor_id_t id) {
	if (!check_id(id) || !position_motor_store[id].position_motor_inited) {
		return false;
	}
	return position_motor_store[id].motor->online;
}

int16_t position_motor_get_rpm(position_control_motor_id_t id) {
	if (!check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	} else if (!position_motor_store[id].position_motor_inited) {
		return POSITION_MOTOR_INIT_ERROR;
	}
	return position_motor_store[id].motor->rpm;
}
