/*
file: engineer_specific_task.c
v1.0

this is the engineer task thread for all of the specific engineer task
(e.g. towing, collecting, and dumping)

*/

#if defined(TARGET_ENGINEER)

#include "stm32f4xx_hal.h"
#include "can.h"
#include "dji_motor.h"
#include "engineer_specific_task.h"
#include "math_user_utils.h"
#include "cmsis_os.h"
#include "pid.h"
#include "gpio.h"
#include "operator_interface_bindings.h"
#include "hardware_contract.h"
#include "position_motor_control.h"
#include "control_mode.h"

#define PID_KP_GRABBER 1.0f
#define PID_KI_GRABBER 0.0f
#define PID_KD_GRABBER 8.0f

#define PID_KP_LIFT 0.5f
#define PID_KI_LIFT 0.0f
#define PID_KD_LIFT 4.0f

//Lift motor range values
#define ZERO_ANG_LIFT 0
#define LIFT_ANGLE_MIN 0

//This max is never reached (due to too large of a value). It only reaches about 4 rotations
#define LIFT_ANGLE_MAX 2.5 * 360.0f

//Grabber motor range values
#define ZERO_ANG_GRABBER 0
#define GRABBER_ANGLE_MIN 0
#define GRABBER_ANGLE_MAX 180

//Divisor
#define GRABBER_ANGLE_STEP 0.15f
#define LIFT_ANGLE_STEP 0.3f

static void engineer_init(void);
static void engineer_init_pos_and_range(void);
static void engineer_tow_handler(void);
static void engineer_dump_handler(void);
static void engineer_collect_handler(void);
static void engineer_increment_angle(position_control_motor_id_t motor_id, int8_t direction, int8_t inverted_sign, float angle_step);

bool pos_and_range_init = false;

static void engineer_init(){
	position_motor_init(MOTOR_LIFT_ID, get_motor(&can1, LIFT_MOTOR));
	position_motor_init_gear_ratio(MOTOR_LIFT_ID, 19);
	position_motor_init_angles(MOTOR_LIFT_ID, ZERO_ANG_LIFT + (uint32_t) LIFT_ANGLE_MAX, 
			ZERO_ANG_LIFT + LIFT_ANGLE_MIN, ZERO_ANG_LIFT);
	position_motor_init_pid(MOTOR_LIFT_ID, POSITION_PID, PID_KP_LIFT, PID_KI_LIFT, PID_KD_LIFT);

	position_motor_init(MOTOR_GRABBER_ID, get_motor(&can1, GRABBER_MOTOR));
	position_motor_init_gear_ratio(MOTOR_GRABBER_ID, 19);
	position_motor_init_angles(MOTOR_GRABBER_ID, ZERO_ANG_GRABBER + GRABBER_ANGLE_MAX,
			ZERO_ANG_GRABBER + GRABBER_ANGLE_MIN, ZERO_ANG_GRABBER);
	position_motor_init_pid(MOTOR_GRABBER_ID, POSITION_PID, PID_KP_GRABBER, PID_KI_GRABBER, PID_KD_GRABBER);
	
	robot_init[GRABBER_INIT] = true;
	robot_init[LIFT_INIT] = true;
	waiting_for_init();
}

static void engineer_init_pos_and_range() {
	init_shift_ang(MOTOR_LIFT_ID, 0);
	init_shift_ang(MOTOR_GRABBER_ID, 0);
	pos_and_range_init = true;
}

void engineer_specific_task(void const* argu){
	engineer_init();
	while(1){
		if(!pos_and_range_init){
			engineer_init_pos_and_range();
			continue;
		}
		if(robot_mode != KILL_MODE){
			engineer_tow_handler();
			engineer_dump_handler();
			engineer_collect_handler();
		}
		osDelay(1);
	}
}

static void engineer_tow_handler() {
	GPIO_Digital_Write(TOW_PIN, (GPIO_PinState) interface_bind_request_tow());
}

static void engineer_dump_handler() {
	GPIO_Digital_Write(DUMP_PIN, (GPIO_PinState) interface_bind_request_dump());
}

static void engineer_collect_handler(){
	// Lift
	if (interface_bind_request_lift_toggle()) {
		engineer_increment_angle(MOTOR_LIFT_ID, -1, -1, LIFT_ANGLE_STEP);
	} else {
		engineer_increment_angle(MOTOR_LIFT_ID, 1, -1, LIFT_ANGLE_STEP);
	}
	position_motor_run_pid(MOTOR_LIFT_ID);
	
	// Grabber
	if (interface_bind_request_grab_toggle()) {
		engineer_increment_angle(MOTOR_GRABBER_ID, -1, -1, GRABBER_ANGLE_STEP);
	} else {
		engineer_increment_angle(MOTOR_GRABBER_ID, 1, -1, GRABBER_ANGLE_STEP);
	}
	position_motor_run_pid(MOTOR_GRABBER_ID);
	
	// activate pistons to grab
	GPIO_Digital_Write(GRAB_PIN, (GPIO_PinState) interface_bind_request_grab());
}

static void engineer_increment_angle(position_control_motor_id_t motor_id, int8_t direction, int8_t inverted_sign, float angle_step) {
	set_ang_des(motor_id, position_motor_get_curr_ang_setpoint(motor_id) + direction * inverted_sign * angle_step);
}
#endif
