/* 
file: drive_task.c 
v2.0
this is the drive task thread for the robot chassis
*/

#include "drive_task.h"
#include "stdbool.h"
#include "can.h"
#include "wiggle_task.h"
#include "ref_comms.h"
#include "operator_interface_bindings.h"
#include "math.h"
#include "cmsis_os.h"
#include "pid.h"
#include "control_mode.h"
#include "imu_task.h"
#include "turret_task.h"
#include "math.h"

#if defined (TARGET_SOLDIER) || defined (TARGET_HERO)

#define PID_KP_DRIVE 15.0f;
#define PID_KI_DRIVE 0.0f
#define PID_KD_DRIVE 30.0f;

#define PID_KP_CHASSIS_ROTATE_WIGGLE 500.0f
#define PID_KI_CHASSIS_ROTATE_WIGGLE 8.0f
#define PID_KD_CHASSIS_ROTATE_WIGGLE 3000.0f
#define PID_INTEGRAL_LIMIT_CHASSIS_ROTATE_WIGGLE 3000

#define PID_KP_CHASSIS_ROTATE_AUTO 100.0f
#define PID_KI_CHASSIS_ROTATE_AUTO 0.10f
#define PID_KD_CHASSIS_ROTATE_AUTO 1000.0f
#define PID_INTEGRAL_LIMIT_CHASSIS_ROTATE_AUTO 100

#define PID_MAX_OUT_CHASSIS_ROTATE 10000

// control scalars
#define KEYBOARD_SCALAR 500	// Magnitude that we multiply the keyboard values by. So if we hit 'w', we will send a base value of 500 to the motor 
#define SPEED_BOOST_MULTIPLIER 2	// Multiply by this scalar when using keyboard and want to go faster
#define CHASSIS_MAGNITUDE_MAX (660.0f)
#define DRIVE_SPEED_FACTOR (6.4f * 2)	// The max value for the remote is 660 660 * DRIVE_SPEED_FACTOR is the max current we can send to the motor
#define POWER_BUF_MULTI 2.0f // When we are driving up a slope, multiply the speed by this number
#define MIN_POWER_BOOST_TILT_ANG (15.0f) // minimum angle that we will get a power boost
#define get_curr_pwr_buf(void) referee.power_and_heat.chassis_power_buffer	// this is defined to shorten code	
#define TILT_MULTIPLIER_DIVISOR 1
#define AUTO_ALIGN_TO_CENTER_TOLERANCE (5)

#define CHASSIS_ACCELERATION_ALPHA_ROTATIONAL (0.01f) // higher acceleration for vertical/rotational, lower for horizontal tranalation
#define CHASSIS_ACCELERATION_ALPHA_VERTICAL (0.01f)
#define CHASSIS_ACCELERATION_ALPHA_HORIZONTAL (0.5f)

#define CHASSIS_ACCELERATION_ALPHA (0.5f) // very minimal acceleratio

#define AUTO_ROTATE_ENABLED true

// rotation via rpm variables
float rotation_rpm = 0.0f;
float rotate_angle_to_turret = 0.0f;
bool chassis_user_rotating = false;
bool chassis_rotate_to_turret = false;
bool chassis_auto_rotate_wait_for_turret_init = false;

// rotation to angle variables
float rotate_set_ang = 0.0f;
float rotate_initial_angle = 0.0f;
float rotate_ang_rel = 0.0f;
float rotate_ang_dif = 0.0f;
float rotate_chassis_ang = 0.0f;
float rotate_chassis_prev_ang = 0.0f;

static float chassis_horizontal_curr_val = 0.0f;
static float chassis_vertical_curr_val = 0.0f;
static float chassis_rotate_curr_val = 0.0f;

dji_motor_t* motor_rf;
dji_motor_t* motor_lf;
dji_motor_t* motor_lb;
dji_motor_t* motor_rb;

pid_t chassis_rotation_pid;

float auto_rotate_chassis_turret_center = false;

void chassis_rotate_turret_to_center(void);
void chassis_manual_handler(float x, float y, float r);
void chassis_run_pid(dji_motor_t* m);			
void rotate_chassis_rpm(float rpm);
void chassis_limit_range(float *val, float mult);
static void initialize_chassis_rotation_pid(void);
void chassis_set_rotate_ang(float ang);

/*
 * This function decreases the output to the robot's drive motors if the robot is inside the power buffer
 * If it is not, the value is simply multiplied by 1
 */
void chassis_power_buffer_limiter(float* val){
	if (referee.online && referee.robot_data.chassis.power_buffer < MAX_ENERGY_BUFF) {
		*val *= (float) referee.robot_data.chassis.power_buffer / MAX_ENERGY_BUFF;
	}
}

/* 
 * This function gives the user a boost if the chassis is going up a slope of MIN_POWER_BOOST_TILT_ANG or greater
 */
void chassis_slope_power_boost(float* x, float* y){
	if(fabs(imu.tilt_angle) > MIN_POWER_BOOST_TILT_ANG){		
	  if(referee.robot_data.chassis.power_buffer == 0 || referee.robot_data.chassis.power_buffer > MAX_ENERGY_BUFF / 2){	// because of how variable our current output is, we want to eat into the energy buffer sparingly.
			/* 
			 * This calculation is valid if the imu is situated so mcb is oriented so that the robomaster label is forward. This calculation 
			 * compares the robot's orientation to the motor output that a user gives it and boosts the user's speed based on the angle
			 * difference between the user input vector and the chassis direction
			 */
			float ang_tilt_rel_user_input = acos((*y * sin(imu_rol_rad()) * cos(imu_pit_rad()) - *x * sin(imu_pit_rad())) / 
				(sqrt(pow(*x, 2) + pow(*y, 2)) * sqrt(pow(sin(imu_rol_rad()) * cos(imu_pit_rad()), 2) + pow(sin(imu_pit_rad()), 2))));
			ang_tilt_rel_user_input *= imu_pit_rad() / fabs(imu_pit_rad());
			float chassis_tilt_boost = fabs(ang_tilt_rel_user_input) > PI / 2 ? fabs(ang_tilt_rel_user_input/TILT_MULTIPLIER_DIVISOR) : 1.0f;			
			*x *= chassis_tilt_boost;
			*y *= chassis_tilt_boost;
		}
	}
}

/*
 * Initialize the pointers to the drive motors and the pid constants for each drive motor
 */
void drive_init(void) {
	initialize_chassis_rotation_pid();
	
	motor_rf = get_motor(&can1, MOTOR_1);
	motor_lf = get_motor(&can1, MOTOR_2);
	motor_lb = get_motor(&can1, MOTOR_3);
	motor_rb = get_motor(&can1, MOTOR_4);

	drive_pid_init(motor_rf);
	drive_pid_init(motor_lf);
	drive_pid_init(motor_lb);
	drive_pid_init(motor_rb);
}

/* 
 * Referenced in drive_init, setup pid
 */
void drive_pid_init(dji_motor_t *motor) {
	motor->pid.p = PID_KP_DRIVE;
	motor->pid.i = PID_KI_DRIVE;
	motor->pid.d = PID_KD_DRIVE;
	motor->pid.pid_mode = POSITION_PID;
	motor->pid.max_out = 10000;
}

/* 
 * Handles state changes for robot_control enum and robot_mode enum
 * referenced Freertos thread
 */
void drive_task(void const* argu) {
	drive_init();
	robot_init[DRIVE_INIT] = true;
	waiting_for_init();
	while (1) {
		
		motor_rf->pid.set = 0;
		motor_lf->pid.set = 0; 
		motor_lb->pid.set = 0;
		motor_rb->pid.set = 0;
		
		#if defined(TARGET_SOLDIER)
		if(robot_mode != KILL_MODE){
			if(robot_mode == WIGGLE_MODE && robot_aim_mode != AIM_CV){
				chassis_wiggle_handler(interface_bind_rotate_chassis());
			} else {
				set_chassis_wiggling(false);
			}
			
			chassis_horizontal_curr_val = low_pass_filter(chassis_horizontal_curr_val, 
				interface_bind_move_horizontal(), CHASSIS_ACCELERATION_ALPHA_HORIZONTAL);
			chassis_vertical_curr_val = low_pass_filter(chassis_vertical_curr_val, 
				interface_bind_move_vertical(), CHASSIS_ACCELERATION_ALPHA_VERTICAL);
			if (fabs(interface_bind_rotate_chassis()) > 0.0f) {
				chassis_rotate_curr_val = low_pass_filter(chassis_rotate_curr_val,
					interface_bind_rotate_chassis(), CHASSIS_ACCELERATION_ALPHA_ROTATIONAL);
			} else {
				chassis_rotate_curr_val = 0.0f;
			}
			
			chassis_manual_handler(chassis_horizontal_curr_val, chassis_vertical_curr_val, chassis_rotate_curr_val);
			
			chassis_run_pid(motor_rf);
			chassis_run_pid(motor_lf);
			chassis_run_pid(motor_lb);
			chassis_run_pid(motor_rb);
			
		} else {
			/* 
			 * If in kill mode, nothing is sent to the chassis. 
			 * when wig_direction_chassis is set to zero, wiggling stops and wiggle variables are reinitialized
			 * we would probably want to take this out for competition, this is just for testing
			 */ 
			can_dji_motor_set(motor_rf, 0);
			can_dji_motor_set(motor_lf, 0);
			can_dji_motor_set(motor_lb, 0);
			can_dji_motor_set(motor_rb, 0);
		}
		#else
		chassis_manual_handler(interface_bind_move_horizontal(), interface_bind_move_vertical(), interface_bind_rotate_chassis());
		chassis_run_pid(motor_rf);
		chassis_run_pid(motor_lf);
		chassis_run_pid(motor_lb);
		chassis_run_pid(motor_rb);
		#endif
		osDelay(1);
	}
}

/*
 * Manually control the chassis given x, y, and rotation r values derived from either remote or keyboard 
 */
void chassis_manual_handler(float x, float y, float r) {
	
	//Magnitude of translational vector to determine speed	
	float h = sqrt(pow(x, 2) + pow(y, 2));
	h *= DRIVE_SPEED_FACTOR;
	//Angle of translational vector
	float theta;
	//Determine exact angle based on the x and y from controller
  //theta : [0, 2PI]
  theta = atan2(y,x);
	// offsets the input angle to be relative to the direction of the turret (turret relative driving) 
  theta += DEG_TO_RAD(position_motor_get_ang_actual(MOTOR_YAW_TURRET_17_ID) - 90);
	//set the speeds
	float speed_lf = (h * sin(theta + PI / 4));
	float speed_rf = (h * cos(theta + PI / 4));
	float speed_lb = (h * -cos(theta + PI / 4));
	float speed_rb = (h * -sin(theta + PI / 4));
	
	chassis_rotate_turret_to_center();
	rotate_chassis_rpm(r);
	
	// set desired speed
	motor_rf->pid.set += speed_rf;
	motor_lf->pid.set += speed_lf; 
	motor_lb->pid.set += speed_lb;
	motor_rb->pid.set += speed_rb; 
}

/*
 * Sets the RPM desired for the requested motor
 */
void chassis_run_pid(dji_motor_t* m){	
	m->pid.set *= interface_bind_keyboard_slow_drive() * get_wiggle_drive_speed_factor();
	chassis_power_buffer_limiter(&m->pid.set);
	m->pid.get = m->rpm; 
	float m_output_error = pid_calc(&m->pid, m->pid.get, m->pid.set); 
	can_dji_motor_set(m, m_output_error);
}

/*
 * Rotates the chassis at a desired wheel RPM magnitude
 */
void rotate_chassis_rpm(float rpm){
	if(robot_mode != WIGGLE_MODE){
		rotation_rpm = rpm * DRIVE_SPEED_FACTOR;
		motor_rf->pid.set += rotation_rpm;
		motor_lf->pid.set += rotation_rpm;
		motor_lb->pid.set += rotation_rpm;
		motor_rb->pid.set += rotation_rpm;
	}
}

/*
 * Referenced in turret_task
 */
float chassis_get_rotation_rpm(void){
	return rotation_rpm;
}

/* 
 * Takes in a pointer to the value you wish to limit and limits that value to CHASSIS_MAGNITUDE_MAX * multi
 */
void chassis_limit_range(float *val, float mult) {
	if(*val > CHASSIS_MAGNITUDE_MAX * mult) {
		*val = CHASSIS_MAGNITUDE_MAX * mult;
	} else if (*val < -CHASSIS_MAGNITUDE_MAX * mult) {
		*val = -CHASSIS_MAGNITUDE_MAX * mult;
	}
}

static void initialize_chassis_rotation_pid() {	
	chassis_rotation_pid.p = PID_KP_CHASSIS_ROTATE_AUTO;
	chassis_rotation_pid.i = PID_KI_CHASSIS_ROTATE_AUTO;
	chassis_rotation_pid.d = PID_KD_CHASSIS_ROTATE_AUTO;
	chassis_rotation_pid.integral_limit = PID_INTEGRAL_LIMIT_CHASSIS_ROTATE_AUTO;
	chassis_rotation_pid.pid_mode = POSITION_PID;
	chassis_rotation_pid.max_out = PID_MAX_OUT_CHASSIS_ROTATE;
}

void chassis_set_rotate_ang(float ang) {
	if (fabs(ang) > 180.0f) {
		rotate_set_ang = 0;
	} else {
		rotate_set_ang = ang;
	}
	rotate_initial_angle = imu.yaw;
	rotate_ang_rel = find_shortest_wrapped_distance(-180.0f, 180.0f, imu.yaw, rotate_initial_angle);
	rotate_ang_dif = find_shortest_wrapped_distance(-180.0f, 180.0f, rotate_ang_rel, rotate_set_ang);
}

/* 
 * Rotates chassis to specified angle from the chassis's current angle, which must be between -180 and 180
 */
void chassis_rotate_angle(){
	// shortest angle between imu yaw value and the initial angle
	rotate_ang_rel = find_shortest_wrapped_distance(-180.0f, 180.0f, imu.yaw, rotate_initial_angle);
	// shortest angle between this and the desired angle
	
	rotate_ang_dif = find_shortest_wrapped_distance(-180.0f, 180.0f, rotate_ang_rel, rotate_set_ang);
	float rpm_output = pid_calc_err(&chassis_rotation_pid, rotate_ang_dif);

	motor_rf->pid.set += rpm_output;
	motor_lf->pid.set += rpm_output;
	motor_lb->pid.set += rpm_output;
	motor_rb->pid.set += rpm_output;
}

bool get_chassis_user_rotating(void){
	return chassis_user_rotating;
}

void chassis_rotate_turret_to_center() {
	if (interface_bind_toggle_disable_auto_rotate_chassis()
	    || (!interface_bind_yaw_ctrl()
	    && robot_mode != WIGGLE_MODE 
	    && (fabs(position_motor_get_ang_actual(MOTOR_YAW_TURRET_17_ID) - TURRET_ZERO_ANG) <= AUTO_ALIGN_TO_CENTER_TOLERANCE
	    || robot_aim_mode == AIM_CV)))
	{
		chassis_rotate_to_turret = false;
		end_turret_lock(TURRET_17_ID);
	} else if (robot_mode != WIGGLE_MODE && chassis_auto_rotate_wait_for_turret_init) {
		if (AUTO_ROTATE_ENABLED) {
			chassis_rotation_pid.p = PID_KP_CHASSIS_ROTATE_AUTO;
			chassis_rotation_pid.i = PID_KI_CHASSIS_ROTATE_AUTO;
			chassis_rotation_pid.d = PID_KD_CHASSIS_ROTATE_AUTO;
			chassis_rotation_pid.integral_limit = PID_INTEGRAL_LIMIT_CHASSIS_ROTATE_AUTO;

			// auto rotation
			if (
				position_motor_get_motor_online(MOTOR_YAW_TURRET_17_ID) 
				&& !auto_rotate_chassis_turret_center
				&& fabs(position_motor_get_ang_actual(MOTOR_YAW_TURRET_17_ID) - TURRET_ZERO_ANG) > AUTO_ALIGN_TO_CENTER_TOLERANCE
				&& robot_aim_mode != AIM_CV
			) {
				auto_rotate_chassis_turret_center = true;
			} else if (fabs(rotate_ang_dif) < AUTO_ALIGN_TO_CENTER_TOLERANCE) {
				auto_rotate_chassis_turret_center = false;
			}
			
			// handle general chassis rotation to the turret
			if (fabs(position_motor_get_ang_actual(MOTOR_YAW_TURRET_17_ID) - TURRET_ZERO_ANG) > 2.0f * AUTO_ALIGN_TO_CENTER_TOLERANCE) {
				rotate_angle_to_turret = position_motor_get_ang_actual(MOTOR_YAW_TURRET_17_ID) - TURRET_ZERO_ANG;
				chassis_set_rotate_ang(rotate_angle_to_turret);
				chassis_rotate_angle();
			}
			
			// initialize user request rotation as well as handle turret initialization and de-initialization for 
			// auto rotation
			if (auto_rotate_chassis_turret_center 
				  && robot_aim_mode != AIM_CV
				  && !chassis_rotate_to_turret) 
			{
					reset_chassis_rotation_pid_out();
					
					chassis_rotation_pid.p = PID_KP_CHASSIS_ROTATE_AUTO;
					chassis_rotation_pid.i = PID_KI_CHASSIS_ROTATE_AUTO;
					chassis_rotation_pid.d = PID_KD_CHASSIS_ROTATE_AUTO;
					chassis_rotation_pid.integral_limit = PID_INTEGRAL_LIMIT_CHASSIS_ROTATE_AUTO;

					robot_mode = BASE_CTRL_MODE;
					chassis_rotate_to_turret = true;
					start_turret_lock(TURRET_17_ID);
			} else if (fabs(rotate_ang_dif) < AUTO_ALIGN_TO_CENTER_TOLERANCE) {
				end_turret_lock(TURRET_17_ID);
				chassis_rotate_to_turret = false;
			}
		}
	} else {
		if (chassis_rotate_to_turret) {
			end_turret_lock(TURRET_17_ID);
		}
		chassis_rotation_pid.p = PID_KP_CHASSIS_ROTATE_WIGGLE;
		chassis_rotation_pid.i = PID_KI_CHASSIS_ROTATE_WIGGLE;
		chassis_rotation_pid.d = PID_KD_CHASSIS_ROTATE_WIGGLE;
		chassis_rotation_pid.integral_limit = PID_INTEGRAL_LIMIT_CHASSIS_ROTATE_WIGGLE;
	}
}

void reset_chassis_rotation_pid_out() {
	chassis_rotation_pid.iout = 0;
	chassis_rotation_pid.pout = 0;
	chassis_rotation_pid.dout = 0;
	chassis_rotation_pid.out = 0;
}

#endif 
