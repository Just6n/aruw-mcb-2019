#include "cv_comms.h"
#include "serial.h"
#include "led_error_handler.h"
#include "turret_task.h"
#include "dji_motor.h"
#include "remote.h"
#include "imu_task.h"
#include "ref_comms.h"
#include "cmsis_os.h"
#include "operator_interface_bindings.h"
#include "sentinel_drive_task.h"

// control vars
static uint8_t msg_switch_index;
#if !defined(TARGET_ENGINEER)
static uint8_t msg_switch_arr[CV_MESSAGE_TYPE_SIZE] = {CV_MESSAGE_TYPE_TURRET_TELEMETRY, CV_MESSAGE_TYPE_IMU, CV_MESSAGE_TYPE_ROBOT_ID, CV_MESSAGE_TYPE_AUTO_AIM_REQUEST};
#else
static uint8_t msg_switch_arr[CV_MESSAGE_TYPE_SIZE] = {CV_MESSAGE_TYPE_IMU, CV_MESSAGE_TYPE_REQUEST_TASK, CV_MESSAGE_TYPE_ROBOT_ID};
#endif
#define TIME_BETWEEN_ROBOT_ID_SEND_MS (5000) // time between each robot id send to CV in milliseconds 
static uint32_t prev_timestamp_robot_id_sent = 0; // tracks previous ms that robot id was sent to CV

bool cv_has_auto_aim_request_queued = false;
bool cv_auto_aim_request_state = false;

// imu vars
extern imu_data_t imu;
extern imu_xyz_accel_t xyz_accel;
#if defined (TARGET_SOLDIER) || defined (TARGET_HERO)
extern dji_motor_t* motor_rf;
extern dji_motor_t* motor_lf;
extern dji_motor_t* motor_lb;
extern dji_motor_t* motor_rb;
#endif

/**
 * Transmits current imu data:
 * accelerometer x, y, yaw, wz, rpm of all 4 wheels
 * over HUART2 for odometry
 * @see serial_transmit;
 */
void cv_comms_send_imu(void);

/**
 * Transmits current robot type and team data:
 * referee.robot_data.robot_id
 * over HUART2 for team calibration
 * @see serial_transmit;
 *      ref_robot_id_t in ref_comms.h
 * @retval true if the robot id has bent sent, false otherwise
 */
bool cv_comms_send_robot_id(void);

/**
 * Success callback for serial transmit
 * increments <msg_switch> so next loop sends next msg
 * @see serial_transmit;
 */
void inc_msg_switch() {
	msg_switch_index = (msg_switch_index + 1) % CV_MESSAGE_TYPE_SIZE;
}

#if !defined (TARGET_ENGINEER)
// static vars
static cv_comms_turret_aim_data_t last_aim_data;
static bool has_aim_data = false;

// func declarations
void cv_comms_turret_send_data(float pitch, float yaw);
bool cv_comms_get_last_aim_data(cv_comms_turret_aim_data_t* out);
bool cv_comms_parse_turret_aim_data(uint8_t* buffer, uint16_t length, cv_comms_turret_aim_data_t *out_aim_data);

/**
 * Transmits current pitch and yaw over HUART2
 * for aiming feedback
 * @see serial_transmit;
 */
void cv_comms_turret_send_data(float pitch, float yaw) {
	int16_t data[2] = 
	{ 
		(int16_t)(pitch * 100), 
		(int16_t)(yaw * 100)
	};
	serial_transmit(SERIAL_HUART2, CV_MESSAGE_TYPE_TURRET_TELEMETRY, 4, (uint8_t*)data, &inc_msg_switch);
}

bool cv_comms_get_last_aim_data(cv_comms_turret_aim_data_t* out_data) {
	if (has_aim_data) {
		*out_data = last_aim_data;
		return true;
	}
	return false;
}

void cv_comms_begin_tracking_target() {
	cv_has_auto_aim_request_queued = true;
	cv_auto_aim_request_state = true;
}

void cv_comms_stop_tracking_target() {
	cv_has_auto_aim_request_queued = true;
	cv_auto_aim_request_state = false;
}

void cv_comms_queued_request_send_success() {
	cv_has_auto_aim_request_queued = false;
	inc_msg_switch();
}

bool cv_comms_parse_turret_aim_data(uint8_t* buffer, uint16_t length, cv_comms_turret_aim_data_t *out_aim_data) {
	if (length != 5) {
		return false;
	}

	int16_t raw_pitch = *((int16_t*)buffer);
	int16_t raw_yaw = *((int16_t*)buffer + 1);
	uint8_t raw_has_target = buffer[4];

	out_aim_data->pitch = (float)raw_pitch / 100;
	out_aim_data->yaw = (float)raw_yaw / 100;
	out_aim_data->has_target = raw_has_target;

	return true;
}

void cv_comms_handle_turret_aim(cv_comms_turret_aim_data_t *aim_data) {
	last_aim_data = *aim_data;
	has_aim_data = true;
}

#else //engineer
// static vars
static bool request_sent = true;
static char curr_request;
static bool is_align_active = false;
static cv_comms_align_data_t align_control;
static bool has_align_control = false;

// func declarations
void cv_comms_task_request_success(void);
void cv_comms_send_task_request(void);
void cv_comms_handle_align_control(cv_comms_align_data_t *align_command);
bool cv_comms_parse_align_control_data(uint8_t* buffer, uint16_t length, cv_comms_align_data_t *out_align_data);
void cv_comms_set_align_complete(void) { is_align_active = false; }
bool cv_comms_parse_align_complete_data(uint8_t* buffer, uint16_t length);

void cv_comms_task_request_success() {
	request_sent = true;
	is_align_active = true;
	inc_msg_switch(); 
}

bool cv_comms_get_last_control(cv_comms_align_data_t* out) {
	if (has_align_control && is_align_active) {
		*out = align_control;
		return true;
	}
	return false;
}

bool cv_comms_is_align_complete(void) { return !is_align_active; }

void cv_comms_new_task_request(char task) {
	request_sent = false;
	curr_request = task;
}

/**
 * Requests a new auto-alignment task 
 * from the jetson over HUART2
 * @see serial_transmit;
 */
void cv_comms_send_task_request(void) {
	if (!request_sent && !is_align_active) {
		serial_transmit(SERIAL_HUART2, CV_MESSAGE_TYPE_REQUEST_TASK, 1, (uint8_t*)(&curr_request), &cv_comms_task_request_success);	
	} else { // no need to request again, increment serial cycle
		inc_msg_switch();
	}
}

void cv_comms_handle_align_control(cv_comms_align_data_t *align_command) {
	align_control = *align_command;
	has_align_control = true;
}

bool cv_comms_parse_align_control_data(uint8_t* buffer, uint16_t length, cv_comms_align_data_t *out_align_data) {
	if (length != 6) {
		return false;
	}

	uint16_t raw_x = *((uint16_t*)buffer);
	uint16_t raw_y = *((uint16_t*)buffer + 1);
	uint16_t raw_r = *((uint16_t*)buffer + 2);

	out_align_data->x = (float)raw_x / 100;
	out_align_data->y = (float)raw_y / 100;
	out_align_data->r = (float) raw_r / 100;

	return true;
}

bool cv_comms_parse_align_complete_data(uint8_t* buffer, uint16_t length) {
	if (length != 1) {
		return false;
	}
	return *((bool*)buffer);
}
#endif

void cv_comms_serial_rx_handler(uint16_t message_type, uint8_t* buffer, uint16_t length) {
	switch (message_type) {
		#if !defined (TARGET_ENGINEER)
		case CV_MESSAGE_TYPE_TURRET_AIM:
		{
			cv_comms_turret_aim_data_t aim_data;
			bool parsed_data = cv_comms_parse_turret_aim_data(buffer, length, &aim_data);
			if (!parsed_data) {
				led_report_general_error(ERROR_AREA_CV_COMMS);
				return;
			}

			aim_data.receive_timestamp = osKernelSysTick();
			cv_comms_handle_turret_aim(&aim_data);
			return;
		}
		#else
		case CV_MESSAGE_TYPE_ALIGN_CONTROL:
		{
			cv_comms_align_data_t align_data;
			bool parsed_data = cv_comms_parse_align_control_data(buffer, length, &align_data);
			if (!parsed_data) {
				led_report_general_error(ERROR_AREA_CV_COMMS);
				return;
			}
			cv_comms_handle_align_control(&align_data);
			return;
		}
		
		case CV_MESSAGE_TYPE_ALIGN_COMPLETE:
		{
			if (cv_comms_parse_align_complete_data(buffer, length)) {
				cv_comms_set_align_complete();
			}
			return;
		}
		#endif
		default:
			led_report_general_error(ERROR_AREA_CV_COMMS);
			return;
	}
} 

void cv_comms_send_imu() {
	float motor_rf_rpm_to_send = 0;
	float motor_lf_rpm_to_send = 0;
	float motor_lb_rpm_to_send = 0;
	float motor_rb_rpm_to_send = 0;
	#if defined(TARGET_SENTINEL)
	extern bool sentinel_drive_is_calibrated;
	if (sentinel_drive_is_calibrated) {
		extern sentinel_drive_state_t sentinel_drive_motor_front;
		extern sentinel_drive_state_t sentinel_drive_motor_back;
		motor_rf_rpm_to_send = sentinel_drive_motor_back.motor->rpm;
		motor_lf_rpm_to_send = sentinel_drive_motor_front.motor->rpm;
		motor_lb_rpm_to_send = sentinel_drive_motor_front.motor->rpm;
		motor_rb_rpm_to_send = sentinel_drive_motor_back.motor->rpm;
	}
	#else
	motor_rf_rpm_to_send = motor_rf->rpm;
	motor_lf_rpm_to_send = motor_lf->rpm;
	motor_lb_rpm_to_send = motor_lb->rpm;
	motor_rb_rpm_to_send = motor_rb->rpm;
	#endif
	
	int16_t data[13] = {
		// Accelerometer readings in static frame
		(int16_t) (xyz_accel.x * 100),
		(int16_t) (xyz_accel.y * 100),
		(int16_t) (xyz_accel.z * 100),
		// MCB IMU angles are in degrees
		(int16_t) (imu.rol * 100),
		(int16_t) (imu.pit * 100),
		(int16_t) (imu.yaw * 100), 
		// MCB IMU angular velocities are in radians/s
		(int16_t) (imu.wx * 100), 
		(int16_t) (imu.wy * 100), 
		(int16_t) (imu.wz * 100), 
		// Wheel RPMs
		motor_rf_rpm_to_send,
		motor_lf_rpm_to_send,
		motor_lb_rpm_to_send,
		motor_rb_rpm_to_send
	};
	serial_transmit(SERIAL_HUART2, CV_MESSAGE_TYPE_IMU, 26, (uint8_t*)data, &inc_msg_switch);
}

bool cv_comms_send_robot_id() {
	uint8_t data[1] = {referee.robot_data.robot_id};
	return serial_transmit(SERIAL_HUART2, CV_MESSAGE_TYPE_ROBOT_ID, 1, data, &inc_msg_switch);
}

void cv_comms_task(void const* argu) {
	serial_init_listener(SERIAL_HUART2, cv_comms_serial_rx_handler);
	msg_switch_index = 0;
	while (true) {
		switch (msg_switch_arr[msg_switch_index]) {
			#if defined(TARGET_SENTINEL) || defined(TARGET_SOLDIER)
			case CV_MESSAGE_TYPE_TURRET_TELEMETRY:
			{
				cv_comms_turret_send_data(
					turret_get_pitch_ang_actual(TURRET_17_ID),
					turret_get_yaw_ang_actual(TURRET_17_ID)
				);
				break;
			}
			#elif defined (TARGET_HERO)
			case CV_MESSAGE_TYPE_TURRET_TELEMETRY:
			{
				cv_comms_turret_send_angles(
					turret_get_pitch_ang_actual(TURRET_42_ID),
					turret_get_yaw_ang_actual(TURRET_42_ID)
				);
				break;
			}
			#else
			case CV_MESSAGE_TYPE_REQUEST_TASK:
			{
				cv_comms_send_task_request();
				break;
			}
			#endif
				
			case CV_MESSAGE_TYPE_IMU:
			{
				cv_comms_send_imu();
				break;
			}
			
			case CV_MESSAGE_TYPE_ROBOT_ID:
			{	
				if (HAL_GetTick() - prev_timestamp_robot_id_sent > TIME_BETWEEN_ROBOT_ID_SEND_MS &&
					  referee.online) 
				{
					if (cv_comms_send_robot_id()) {
						prev_timestamp_robot_id_sent = HAL_GetTick();
					}
					break;
				} else {
					inc_msg_switch();
					continue;
				}
			}
			
			case CV_MESSAGE_TYPE_AUTO_AIM_REQUEST:
			{
				if (cv_has_auto_aim_request_queued) {
					uint8_t data = cv_auto_aim_request_state;
					serial_transmit(SERIAL_HUART2, CV_MESSAGE_TYPE_AUTO_AIM_REQUEST, 1, &data, &cv_comms_queued_request_send_success);
					break;
				} else {
					inc_msg_switch();
					continue;
				}
			}
			
			/* DO NOT MAKE A DEFAULT CASE */
		}
		osDelay(1);
	}
}

