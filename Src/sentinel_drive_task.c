/*
file: sentinel_drive_task.c
v1.0
this is the drive task thread for the robot chassis

ORIENTATION OF THE CHASSIS
FORWARD: towards to open field / resource island, turret facing this direction
BACKWARD: towards the team's base, HP bar facing this direction
LEFT, RIGHT: axes along the sentinel rail
*/

#include "sentinel_drive_task.h"

#if defined (TARGET_SENTINEL)

#define RPM_PID_KP_DRIVE (8.0f)
#define RPM_PID_KI_DRIVE (1.0f)
#define RPM_PID_KD_DRIVE (70.0f)
#define RPM_PID_MAX_I_ACCUM (250.0f)

#define POSITION_PID_KP_DRIVE (400.0f)
#define POSITION_PID_KI_DRIVE (1.0f)
#define POSITION_PID_KD_DRIVE (100000.0f)
#define POSITION_PID_MAX_I_ACCUM (2000.0f)
#define POSITION_PID_DIST_CM (5) // distance away from the setpoint in cm when the motors swap to use position pid

#define PID_MAX_OUT_DRIVE (10000.0f)

#define SENTINEL_ERROR_DIST_MARGIN_CM (2.54) // 1 inch(2.54cm) allowed distance error margin from the setpoint
#define SENTINEL_MOTOR_GEAR_RATIO (19)
#define SENTINEL_WHEEL_CIRCUM_CM (7.3f * PI) // circumference of the new sentinel wheel (in cm)
#define SENTINEL_WHEEL_CM_PER_ENC (SENTINEL_WHEEL_CIRCUM_CM / (float) (SENTINEL_MOTOR_GEAR_RATIO * ENC_MAX))

#define SENTINEL_MOTOR_LEFT_INVERTED true
#define SENTINEL_MOTOR_RIGHT_INVERTED false

#define SENTINEL_RAIL_FULL_DIST_MAX_CM (200.0f) // distance of the whole sentinel rail (in cm)
#define SENTINEL_RAIL_FULL_DIST_MIN_CM (0.0f) // starting point distance of the whole sentinel rail (in cm)
#define SENTINEL_RAIL_INNER_DIST_MAX_CM (200.0f) // max distance of the inner section of the sentinel rail desired to travel (in cm)
#define SENTINEL_RAIL_INNER_DIST_MIN_CM (0.0f) // min distance of the inner section of the sentinel rail desired to travel (in cm)

#define SENTINEL_AUTO_DRIVE_MIN_DIST (50.0f) // min distance of the sentinel autonomous drive to
                                             // travel between different sentinel_drive_dist_setpoint(s)
#define SENTINEL_AUTO_DRIVE_MAX_RPM (2000.0f) // max rpm of the sentinel autonomous drive
#define SENTINEL_AUTO_DRIVE_MIN_RPM (660.0f) // min rpm of the sentinel autonomous drive
#define SENTINEL_AUTO_DRIVE_RPM_SPEEDS (10) // number of different rpm speeds that the randomizer chooses

// POWER BUFFER USAGE DEFINES
#define SENTINEL_POWER_BUF_SAFETY (10) // amount of joules left in the power buffer before exiting
#define SENTINEL_POWER_BUF_BOOST (3) // output power multiplier when using the power buffer
#define SENTINEL_POWER_BUF_MAX_USAGE_TIME (5000) // max usage time of the power buffer after being hit in ms
// IR SENSOR DEFINES
#define MIN_IR_SENSOR_DISTANCE_BOUND (170) // any readings under IR_SENSOR_DISTANCE_BOUND mm are invalidated

sentinel_drive_state_t sentinel_drive_motor_front = {0};
sentinel_drive_state_t sentinel_drive_motor_back = {0};

static void init_sentinel_drive_motor(
  sentinel_drive_state_t *motor_state,
  dji_motor_t* dji_motor,
  bool is_inverted
);

// sentinel static function declarations
static void sentinel_drive_init(void);
static void sentinel_drive_calibrate_here(void);
static float sentinel_motor_get_dist(sentinel_drive_state_t *motor);
static float sentinel_motor_get_rpm(sentinel_drive_state_t *sentinel_drive_motor);
static bool sentinel_drive_get_dist(float* out_dist);
static void sentinel_drive_set_dist_setpoint(float dist);
static void sentinel_drive_set_rpm_setpoint(float rpm);
static bool is_sentinel_drive_at_setpoint(void);
static void sentinel_drive_run_pid(void);
static void sentinel_check_rail_bounds(void);
static void sentinel_check_power_buffer(void);
static void update_motor_dist_setpoint(void);
static void update_motor_rpm_setpoint(void);
static void sentinel_drive_manual_handler(void);
static void sentinel_auto_drive_handler(void);
static void reset_sentinel_drive_dist_to_min(void);
static void reset_sentinel_drive_dist_to_max(void);
static void stop_sentinel_drive_to_fire(void);

bool sentinel_power_buffer_mode = false;
bool sentinel_drive_is_stopped = false;
bool sentinel_drive_is_calibrated = false;
bool sentinel_auto_drive_reset = true;
uint32_t sentinel_damaged_timestamp = 0;
uint32_t sentinel_drive_stopped_timestamp = 0;
float sentinel_drive_dist_setpoint = 0;
float sentinel_drive_rpm_setpoint = 0;
sentinel_drive_follow_direction_t sentinel_follow_direction = NOT_FOLLOWING_ENEMIES;

void sentinel_drive_task(void const* argu) {
  sentinel_drive_init();
  while (1) {
    if (!sentinel_drive_is_calibrated) {
      sentinel_drive_calibrate_here();
      continue;
    }
    if (robot_mode == BASE_CTRL_MODE) {
      sentinel_drive_manual_handler();
    } else if (robot_mode == AUTO_MODE) {
      sentinel_auto_drive_handler();
    }
    sentinel_drive_run_pid();
    osDelay(1);
  }
}

static void init_sentinel_drive_motor(
  sentinel_drive_state_t *motor_state,
  dji_motor_t* dji_motor,
  bool is_inverted
) {
  motor_state->motor = dji_motor;
  motor_state->is_inverted = is_inverted;

  PID_struct_init(
    &dji_motor->pid,
    SMOOTHED_POSITION_PID,
    PID_MAX_OUT_DRIVE,
    RPM_PID_MAX_I_ACCUM,
    RPM_PID_KP_DRIVE,
    RPM_PID_KI_DRIVE,
    RPM_PID_KD_DRIVE
  );

  PID_struct_init(
    &motor_state->position_pid,
    SMOOTHED_POSITION_PID,
    PID_MAX_OUT_DRIVE,
    POSITION_PID_MAX_I_ACCUM,
    POSITION_PID_KP_DRIVE,
    POSITION_PID_KI_DRIVE,
    POSITION_PID_KD_DRIVE
  );
}

static void sentinel_drive_init() {
  init_sentinel_drive_motor(&sentinel_drive_motor_front, get_motor(&can1, MOTOR_3), SENTINEL_MOTOR_LEFT_INVERTED);
  init_sentinel_drive_motor(&sentinel_drive_motor_back, get_motor(&can1, MOTOR_2), SENTINEL_MOTOR_RIGHT_INVERTED);
}

static void sentinel_drive_manual_handler() {
  sentinel_auto_drive_reset = true;
  sentinel_drive_rpm_setpoint = interface_bind_move_horizontal() * 4;
  sentinel_check_power_buffer();
}

static void sentinel_auto_drive_handler() {
  sentinel_check_rail_bounds();
  sentinel_check_power_buffer();
  if (is_sentinel_drive_at_setpoint() || sentinel_auto_drive_reset) {
    // reset back to auto drive mode, get a new distance and rpm setpoint
    sentinel_auto_drive_reset = false;
    update_motor_dist_setpoint();
  }
  stop_sentinel_drive_to_fire();
}

static void sentinel_drive_calibrate_here() {
  if (sentinel_drive_motor_front.motor->online
      && sentinel_drive_motor_back.motor->online)
  {
    sentinel_drive_motor_front.calibrated_encoder_start_value = sentinel_drive_motor_front.motor->encoder_unwrapped;
    sentinel_drive_motor_back.calibrated_encoder_start_value = sentinel_drive_motor_back.motor->encoder_unwrapped;
    sentinel_drive_set_dist_setpoint(sentinel_drive_dist_setpoint);
    sentinel_drive_is_calibrated = true;
  }
}

static float sentinel_motor_get_dist(sentinel_drive_state_t *sentinel_drive_motor) {
  return ((float) sentinel_drive_motor->motor->encoder_unwrapped
          - (float) sentinel_drive_motor->calibrated_encoder_start_value)
          * (sentinel_drive_motor->is_inverted ? -1.0f : 1.0f)
          * SENTINEL_WHEEL_CM_PER_ENC;
}

static float sentinel_motor_get_rpm(sentinel_drive_state_t *sentinel_drive_motor) {
  return sentinel_drive_motor->motor->rpm * (sentinel_drive_motor->is_inverted ? -1.0f : 1.0f);
}

static bool sentinel_drive_get_dist(float *out_dist) {
  if (!sentinel_drive_is_calibrated) {
    return false;
  }
  float left_dist = sentinel_motor_get_dist(&sentinel_drive_motor_front);
  float right_dist = sentinel_motor_get_dist(&sentinel_drive_motor_back);

  *out_dist = (left_dist + right_dist) / 2.0f;
  return true;
}

static void sentinel_drive_set_dist_setpoint(float dist) {
  sentinel_drive_dist_setpoint = limit_float(dist, SENTINEL_RAIL_INNER_DIST_MIN_CM, SENTINEL_RAIL_INNER_DIST_MAX_CM);
}

static void sentinel_drive_set_rpm_setpoint(float rpm) {
  sentinel_drive_rpm_setpoint = limit_float(rpm, SENTINEL_AUTO_DRIVE_MIN_RPM, SENTINEL_AUTO_DRIVE_MAX_RPM);
}

static bool is_sentinel_drive_at_setpoint() {
  float curr_sentinel_drive_dist = 0.0f;
  sentinel_drive_get_dist(&curr_sentinel_drive_dist);
  return fabs(curr_sentinel_drive_dist - sentinel_drive_dist_setpoint) < SENTINEL_ERROR_DIST_MARGIN_CM;
}

static void sentinel_motor_run_pid(sentinel_drive_state_t *sentinel_drive_motor) {
  float motor_output = 0.0f;
  float curr_motor_dist = sentinel_motor_get_dist(sentinel_drive_motor);
  float dist_motor_error = sentinel_drive_dist_setpoint - curr_motor_dist;
  if (robot_mode == AUTO_MODE && fabs(dist_motor_error) < POSITION_PID_DIST_CM) {
    motor_output = pid_calc_err(&sentinel_drive_motor->position_pid, dist_motor_error);
  } else if (robot_mode == BASE_CTRL_MODE ||
            (robot_mode == AUTO_MODE && fabs(dist_motor_error) > POSITION_PID_DIST_CM))
  {
    float rpm_motor_error = sentinel_drive_rpm_setpoint - sentinel_motor_get_rpm(sentinel_drive_motor);
    motor_output = pid_calc_err(&sentinel_drive_motor->motor->pid, rpm_motor_error);
  }
  if ((curr_motor_dist <= SENTINEL_RAIL_FULL_DIST_MIN_CM && motor_output < 0) ||
      (curr_motor_dist >= SENTINEL_RAIL_FULL_DIST_MAX_CM && motor_output > 0))
  {
    motor_output = 0;
  }
  can_dji_motor_set(
    sentinel_drive_motor->motor,
    sentinel_drive_motor->is_inverted ? -motor_output : motor_output
  );
}

static void sentinel_drive_run_pid() {
  if (sentinel_drive_is_calibrated) {
    sentinel_motor_run_pid(&sentinel_drive_motor_front);
    sentinel_motor_run_pid(&sentinel_drive_motor_back);
  }
}

// check if sentinel has been hit and utilizes power buffer energy if it has
static void sentinel_check_power_buffer() {
  // if the sentinel has received damage or is currently using the power buffer, continue to
  // speed up the chassis up to SENTINEL_POWER_BUF_MULTI times speed and slowing down depending
  //  on amount of joules left in the power buffer
  if ((referee.robot_data.damage_type == ARMOR_DAMAGE
      && referee.robot_data.received_dps > 0)
      && !sentinel_power_buffer_mode)
  {
    sentinel_power_buffer_mode = true;
    sentinel_drive_rpm_setpoint *= SENTINEL_POWER_BUF_BOOST;
    sentinel_damaged_timestamp = osKernelSysTick();
  }
  if ((sentinel_damaged_timestamp >= SENTINEL_POWER_BUF_MAX_USAGE_TIME
     || referee.robot_data.chassis.power_buffer <= SENTINEL_POWER_BUF_SAFETY)
     && sentinel_power_buffer_mode) {
    // sentinel has been damaged, so exit when all of the buffer has been used
    sentinel_power_buffer_mode = false;
    sentinel_drive_rpm_setpoint /= (float) SENTINEL_POWER_BUF_BOOST;
  } else if (
     referee.online
     && referee.robot_data.chassis.power_buffer >= SOFT_MAX_ENERGY_BUFF
     && referee.robot_data.chassis.power_buffer <= HARD_MAX_ENERGY_BUFF)
  {
    // standard buffer usage amount
    sentinel_drive_rpm_setpoint *= (referee.robot_data.chassis.power_buffer
                                   - SOFT_MAX_ENERGY_BUFF)
                                   / (float) SOFT_MAX_ENERGY_BUFF;
  } else {
    // over the soft max energy buff amount used when sentinel has taken damage
    sentinel_drive_rpm_setpoint = 0;
  }
}

// check with ir_sensor, encoder position, and limit switch to reset the target position
static void sentinel_check_rail_bounds() {
  float left_ir_distance = ir_get_distance(LEFT_IR_SENSOR, SHORT_IR_SENSOR);
  float right_ir_distance = ir_get_distance(RIGHT_IR_SENSOR, SHORT_IR_SENSOR);
  if (left_ir_distance < MIN_IR_SENSOR_DISTANCE_BOUND && left_ir_distance != -1) {
    reset_sentinel_drive_dist_to_min();
    sentinel_drive_dist_setpoint = SENTINEL_RAIL_FULL_DIST_MAX_CM / 2.0f;
    update_motor_rpm_setpoint();
  }
  if (right_ir_distance < MIN_IR_SENSOR_DISTANCE_BOUND && right_ir_distance != -1) {
    reset_sentinel_drive_dist_to_max();
    sentinel_drive_dist_setpoint = SENTINEL_RAIL_FULL_DIST_MAX_CM / 2.0f;
    update_motor_rpm_setpoint();
  }
}

static void update_motor_dist_setpoint() {
  float next_sentinel_drive_dist_setpoint = RAND(SENTINEL_RAIL_INNER_DIST_MIN_CM, SENTINEL_RAIL_INNER_DIST_MAX_CM);
  if (fabs(next_sentinel_drive_dist_setpoint - sentinel_drive_dist_setpoint) < SENTINEL_AUTO_DRIVE_MIN_DIST)
  {
    if (sentinel_drive_dist_setpoint >= next_sentinel_drive_dist_setpoint) {
      next_sentinel_drive_dist_setpoint = sentinel_drive_dist_setpoint - SENTINEL_AUTO_DRIVE_MIN_DIST;
    } else {
      next_sentinel_drive_dist_setpoint = sentinel_drive_dist_setpoint + SENTINEL_AUTO_DRIVE_MIN_DIST;
    }
  }
  sentinel_drive_set_dist_setpoint(next_sentinel_drive_dist_setpoint);
  update_motor_rpm_setpoint();
}

static void update_motor_rpm_setpoint() {
  float sentinel_auto_drive_rpm =
    RAND(1, SENTINEL_AUTO_DRIVE_RPM_SPEEDS)
    * (SENTINEL_AUTO_DRIVE_MAX_RPM - SENTINEL_AUTO_DRIVE_MIN_RPM)
    / (float) SENTINEL_AUTO_DRIVE_RPM_SPEEDS
    + SENTINEL_AUTO_DRIVE_MIN_RPM;
  sentinel_drive_set_rpm_setpoint(sentinel_auto_drive_rpm);
  float curr_dist;
  // motor direction
  if (sentinel_drive_get_dist(&curr_dist) && curr_dist > sentinel_drive_dist_setpoint) {
    sentinel_drive_rpm_setpoint *= -1;
  }
  // power buf multipliers
  if (sentinel_power_buffer_mode) {
    sentinel_drive_rpm_setpoint *= SENTINEL_POWER_BUF_BOOST;
  }
}

static void reset_motor_dist_to_min(sentinel_drive_state_t *sentinel_drive_motor) {
  sentinel_drive_motor->calibrated_encoder_start_value = sentinel_drive_motor->motor->encoder_unwrapped;
}

static void reset_sentinel_drive_dist_to_min() {
  reset_motor_dist_to_min(&sentinel_drive_motor_front);
  reset_motor_dist_to_min(&sentinel_drive_motor_back);
}

static void reset_motor_dist_to_max(sentinel_drive_state_t *sentinel_drive_motor) {
  int sentinel_drive_max_encoder =
    SENTINEL_RAIL_FULL_DIST_MAX_CM
    * (sentinel_drive_motor->is_inverted ? -1.0f : 1.0f)
    / (float) SENTINEL_WHEEL_CM_PER_ENC
    + (float) sentinel_drive_motor->calibrated_encoder_start_value;
  sentinel_drive_motor->motor->rotation_count = sentinel_drive_max_encoder / ENC_MAX;
}

static void reset_sentinel_drive_dist_to_max() {
  reset_motor_dist_to_max(&sentinel_drive_motor_front);
  reset_motor_dist_to_max(&sentinel_drive_motor_back);
}

static void stop_sentinel_drive_to_fire() {
  extern bool sentinel_ready_to_fire;
  if (sentinel_ready_to_fire)  {
    sentinel_drive_rpm_setpoint = 0;
    if (!sentinel_drive_is_stopped) {
      sentinel_drive_is_stopped = true;
      sentinel_drive_stopped_timestamp = osKernelSysTick();
    }
  } else if (sentinel_drive_is_stopped) {
    sentinel_drive_is_stopped = false;
    update_motor_rpm_setpoint();
  }
}

static void sentinel_drive_follow_enemy() {
  if (sentinel_follow_direction != NOT_FOLLOWING_ENEMIES) {
    sentinel_drive_get_dist(&sentinel_drive_dist_setpoint);
    if (sentinel_follow_direction == FOLLOW_ENEMY_RIGHT) {
      sentinel_drive_set_dist_setpoint (sentinel_drive_dist_setpoint + 50);
    } else if (sentinel_follow_direction == FOLLOW_ENEMY_LEFT) {
      sentinel_drive_set_dist_setpoint (sentinel_drive_dist_setpoint - 50);
    }
    update_motor_rpm_setpoint();
    sentinel_follow_direction = NOT_FOLLOWING_ENEMIES;
  }
}

#endif
