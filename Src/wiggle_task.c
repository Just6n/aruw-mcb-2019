#include "wiggle_task.h"
#include "turret_task.h"

#if defined (TARGET_SOLDIER) || defined (TARGET_HERO)

#include "math_user_utils.h"

#if defined (TARGET_SOLDIER) 
#define LEAD_TURRET ((turret_id_t) TURRET_17_ID)
#elif defined (TARGET_HERO)
#define LEAD_TURRET ((turret_id_t) TURRET_42_ID)
#endif

#define CHASSIS_PERIOD 1500
#define KINEMATIC_STEP_TIME_MS 2 // how far in the future we should look ahead when we predict current chassis angle
#define TURRET_MIN_MAX_BUFFER 5.0f
#define WIGGLE_DRIVE_SPEED_FACTOR 0.333f // slows down the drive train for wiggling
#define WIGGLE_MIN_TIME_INCREMENT 0.5f // minimum time increment for wiggling upon the first period
static float wig_curr_max_angle = 0.0f;
static float wig_curr_min_angle = 0.0f;
static uint16_t wig_max_ang = 80;

static float wiggle_curr_time = 0;
static float wig_chassis_center_angle = 0.0f;
static float chassis_rotation_range = 45;
static float chassis_des_ang = 0.0f;
static float wig_rotate_ang_dif = 0.0f;
static float wiggle_chassis_shift_time = 0.0f;
static float turret_des_ang = 0;
static bool chassis_wiggling = false;


// calculating angular velocity and angular acceleration
static float chassis_angular_velocity[2];
static uint16_t curr_angular_velocity_step;
static float prev_imu_yaw = 0.0f;
static float chassis_angular_acceleration;

typedef struct turret_wiggle_variables_t {
	bool wiggle_enabled;
	float wig_turret_yaw_initial_ang;
	int16_t wig_adjusted_turret_min_ang_yaw;
	int16_t wig_adjusted_turret_max_ang_yaw;
	float yaw_dis_min;
	float yaw_dis_max;
	float yaw_curr_error;
	float chassis_initial_ang;
	bool turret_locked;
} turret_wiggle_variables_t;

turret_wiggle_variables_t turret_wiggle_var_store[NUMBER_OF_TURRETS];	// turret wiggle variables have the same index in their array as the turret array

static bool wig_turret_init(turret_id_t turret_id);
static void determine_wig_max_ang(void);
static motor_control_error_t wiggle_var_store_init(turret_id_t turret_id);
static void calc_chassis_acceleration_velocity(void);
static float calc_chassis_rel_init(turret_id_t turret_id);
extern bool chassis_rotate_to_turret;

/**
 * 	@brief signals that the turret specified may be allowed to wiggle. Initializes the wiggle variables
 *  			 of the turret specified
 * 	@param id the turret id
 * 	@retval none
 */
motor_control_error_t turret_enable_wiggle_add(uint16_t id) {
	turret_id_t turret_id = (turret_id_t) id;
	if(!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	}
	wiggle_var_store_init(turret_id);
	return TURRET_OK;
}

/* 
 * Initializes turret and chassis relevant variables
 * Call in chassis_task if you want to lock all turrets
 */
/**
 * 	@brief Initializes turret and chassis relevant variables. Call in chassis_task if you want to lock 
 *  all turrets.
 * 	@param none
 * 	@retval none
 */
void chassis_lock_handler(void){
	float chassis_ang = imu.yaw;
	
	// reinitialize wiggling
	if(!chassis_wiggling) {
		reset_chassis_rotation_pid_out();
		/*
		step 1: determine max angle to rotate
		step 2: see if any of the turrets have min or max distances away < wig_max_ang
		step 3: for those motors, see what the smallest distance away is and calculate 
						wig_curr_min_angle and wig_curr_max_angle
		*/
		determine_wig_max_ang();
		wig_curr_min_angle = -wig_max_ang;
		wig_curr_max_angle = wig_max_ang;
		for(uint16_t i = 0; i < NUMBER_OF_TURRETS; i++) {
			if (turret_wiggle_var_store[i].wiggle_enabled) {
				int16_t yaw_dis_min = turret_get_min_ang_yaw((turret_id_t)i) - 
					turret_get_yaw_ang_actual((turret_id_t)i) + TURRET_MIN_MAX_BUFFER;
				int16_t yaw_dis_max = turret_get_max_ang_yaw((turret_id_t)i) - 
					turret_get_yaw_ang_actual((turret_id_t)i) - TURRET_MIN_MAX_BUFFER;
				if (wig_curr_min_angle < yaw_dis_min) {
					wig_curr_min_angle = yaw_dis_min;
					wig_curr_max_angle = yaw_dis_min + 2 * wig_max_ang;
				} else if (wig_curr_max_angle > yaw_dis_max) {
					wig_curr_max_angle = yaw_dis_max;
					wig_curr_min_angle = wig_curr_max_angle - 2 * wig_max_ang;
				}
				turret_wiggle_var_store[i].yaw_dis_max = yaw_dis_max;
				turret_wiggle_var_store[i].yaw_dis_min = yaw_dis_min;
			}
		}
		
		// calc wig adjusted max and min angs for all wiggling turrets
		for (uint16_t i = 0; i < NUMBER_OF_TURRETS; i++) {
			turret_wiggle_var_store[i].wig_adjusted_turret_max_ang_yaw = 
				turret_get_max_ang_yaw((turret_id_t)i) - wig_curr_max_angle;
			turret_wiggle_var_store[i].wig_adjusted_turret_min_ang_yaw = 
				turret_get_min_ang_yaw((turret_id_t)i) - wig_curr_min_angle;
		}
		
		for (uint16_t i = 0; i < NUMBER_OF_TURRETS; i++) {
			if(turret_wiggle_var_store[i].wiggle_enabled){
				wig_turret_init((turret_id_t)i);
			}
		}
		
		chassis_wiggling = true;
		
		for (int i = 0; i < NUMBER_OF_TURRETS; i++) {
			turret_wiggle_var_store[i].chassis_initial_ang = chassis_ang;
			turret_wiggle_var_store[i].turret_locked = true;
		}
		
		wiggle_curr_time = 0;
		
		wig_chassis_center_angle = (wig_curr_max_angle + wig_curr_min_angle) / 2.0f;
		chassis_rotation_range = (wig_curr_max_angle - wig_curr_min_angle) / 2.0f;

		wiggle_chassis_shift_time = asin(wig_chassis_center_angle / chassis_rotation_range) * 
			(float)(CHASSIS_PERIOD) / (2.0f * PI);
		
	}
}

/**
 *  @brief  call this in chassis_task to rotate the chassis
 *  @param  chassis_desired_wheel_rpm_rotation the user's desired chassis wheel shaft rpm. This is converted
 *          into an angle which is than added onto the initial wiggle angle, allowing for user chassis rotation 
 *          control while wiggling
 *  @retval none
 */
void chassis_wiggle_handler(float chassis_desired_wheel_rpm_rotation) {
	chassis_lock_handler();

	// angle in rads
	// convert the chassis rotation aspect of the user's wheel rpm desired to a chassis rotation angle
	// first convert rotations / minute to rotations per second, multiply by change in time (1 ms),
	// giving a number of wheel rotations in that ms. Multiply by wheel radius to determine the arc length
	// of the chassis rotation angle in that 1 ms, then divice that by the wheel distance from the center
	// of the chassis to give the angle of desired chassis rotation, then convert that to degrees
	float chassis_desired_rotation_angle = chassis_desired_wheel_rpm_rotation * MINUTES_PER_SECOND
		* 0.001f * WHEEL_RADIUS / WHEEL_DISTANCE_FROM_CHASSIS_CENTER;
	chassis_desired_rotation_angle = RAD_TO_DEG(chassis_desired_rotation_angle);

	// add this angle on to initial rotation angle, which in turn will affect chassis_rel_init
	turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang -= chassis_desired_rotation_angle;
	// wrap from -180 to 180
	turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang = 
		wrap_around_angle(turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang + 180.0f, 360.0f) - 180.0f;
	
	// convert rpm to angle, assuming 1 ms intervals, should be good enough otherwise actually keep track of time
	
	chassis_des_ang = chassis_rotation_range * sin(2 * PI * ((float)wiggle_curr_time - 
		wiggle_chassis_shift_time) / (float)CHASSIS_PERIOD) + wig_chassis_center_angle;
	
	float chassis_rel_init = calc_chassis_rel_init(LEAD_TURRET);

	wig_rotate_ang_dif = chassis_rel_init - chassis_des_ang;
	//wig_rotate_ang_dif -= turret_wiggle_var_store[LEAD_TURRET].yaw_curr_error;
	float wiggle_time_incr = WIGGLE_MIN_TIME_INCREMENT;
	if (wiggle_curr_time < CHASSIS_PERIOD) {
 		wiggle_time_incr = ((float) wiggle_curr_time / (float) CHASSIS_PERIOD) 
												 * (1 - WIGGLE_MIN_TIME_INCREMENT) + WIGGLE_MIN_TIME_INCREMENT;
	} else {
		wiggle_time_incr = 1;
	}
	chassis_set_rotate_ang(wig_rotate_ang_dif);
	chassis_rotate_angle();
	
	wiggle_curr_time+= wiggle_time_incr;
}

/**
 *  @brief Call this if you want to lock an individual turret. Currently, locks a turret if in wiggle or 
 *  			 lock turret mode, and otherwise does not lock, you can modify this method if you want it to 
 *				 behave differently
 *  @param id the turret id that you would like to enable a lock on
 *  @retval none
 */
static void wiggle_enable_lock_handler(uint16_t id) {
	turret_id_t turret_id = (turret_id_t) id;
	if (robot_mode == WIGGLE_MODE || robot_mode == LOCK_TURRET_MODE || chassis_rotate_to_turret) {
		turret_wiggle_var_store[turret_id].turret_locked = true;
	} else {
		turret_wiggle_var_store[turret_id].turret_locked = false;
	}
}

/**
 *  @brief  calculates the error between the chassis imu and the turret angle, then returns an adjusted yaw turret angle 
 *          that takes into account the calculated error
 *  @param  id the turret id 
 *  @param  user_ang_desired_changed the difference between the current user angle setpoint and the previous angle setpoint.
 *          This is added to the yaw initial wiggle angle and allows for user control while wiggling.
 *  @param  yaw_ang_desired the current user desired angle
 *  @retval the adjusted desired current turret angle. If not wiggling, the same angle as passed in is returned
 */
float turret_wiggle_handler(uint16_t id, float user_ang_desired_changed, float yaw_ang_desired) {
	if (robot_mode != WIGGLE_MODE) {
		turret_id_t turret_id = (turret_id_t)id;
		wiggle_enable_lock_handler(turret_id);
		calc_chassis_acceleration_velocity();
		if(turret_wiggle_var_store[turret_id].turret_locked){
			// add on user change in angle
			float chassis_rel_init = calc_chassis_rel_init(turret_id);
			turret_wiggle_var_store[turret_id].chassis_initial_ang += user_ang_desired_changed;
			turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang = 
				wrap_around_angle(turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang + 180.0f, 360.0f) - 180.0f;

			// limit the angle that was just changed
			// kinematic motion equation
			float new_chassis_rel_init = chassis_rel_init - (0.5f) * (chassis_angular_acceleration) * pow(KINEMATIC_STEP_TIME_MS / 1000.0f, 2.0f)
				- chassis_angular_velocity[curr_angular_velocity_step] * (KINEMATIC_STEP_TIME_MS / 1000.0f);
			// calculate turret desired angle based on error between chassis imu and turret yaw
			float adjusted_yaw_ang_desired = turret_wiggle_var_store[turret_id].wig_turret_yaw_initial_ang + new_chassis_rel_init;
			// calculate curren turret error
			turret_wiggle_var_store[turret_id].yaw_curr_error = turret_get_yaw_ang_actual(turret_id) - adjusted_yaw_ang_desired;
			// return new updated desired yaw angle
			return adjusted_yaw_ang_desired;
		} else {
			// return same angle as was passed in if not wiggling
			return yaw_ang_desired;
		}
	} else {
		turret_id_t turret_id = (turret_id_t)id;
		wiggle_enable_lock_handler(turret_id);
		calc_chassis_acceleration_velocity();
		if(turret_wiggle_var_store[turret_id].turret_locked){
			// add on user change in angle
			float chassis_rel_init = calc_chassis_rel_init(turret_id);
			turret_wiggle_var_store[turret_id].chassis_initial_ang += user_ang_desired_changed;
			turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang = 
				wrap_around_angle(turret_wiggle_var_store[LEAD_TURRET].chassis_initial_ang + 180.0f, 360.0f) - 180.0f;

			turret_des_ang = chassis_rotation_range * sin(2 * PI * ((float)wiggle_curr_time) 
				/ (float) CHASSIS_PERIOD) + turret_wiggle_var_store[turret_id].wig_turret_yaw_initial_ang;
			float new_chassis_rel_init = chassis_rel_init - (0.5f) * (chassis_angular_acceleration) * pow(KINEMATIC_STEP_TIME_MS / 1000.0f, 2.0f)
				- chassis_angular_velocity[curr_angular_velocity_step] * (KINEMATIC_STEP_TIME_MS / 1000.0f);
						
			float adjusted_yaw_ang_desired = turret_wiggle_var_store[turret_id].wig_turret_yaw_initial_ang + new_chassis_rel_init;
					
			turret_wiggle_var_store[turret_id].yaw_curr_error = turret_get_yaw_ang_actual(turret_id) - adjusted_yaw_ang_desired;
			return turret_des_ang - turret_wiggle_var_store[turret_id].yaw_curr_error;
		} else {
			return yaw_ang_desired;
		}
	}
}
/**
 *  @brief Initialize initial_wiggle_angle and yaw_input_acumulated
 * 				 Referenced in chassis_wiggle_handler
 *  @param turret_id the turret you would like to initialize
 *  @retval none
 */
static bool wig_turret_init(turret_id_t turret_id){
	if(!turret_check_id(turret_id)) {
		return false;
	}
	turret_wiggle_var_store[turret_id].wig_turret_yaw_initial_ang = turret_get_yaw_ang_actual(turret_id);
	return true;
}

/**
 *  @brief calculates the max wiggle angle based all the turrets that are currently wiggling
 *  @param none
 *  @retval none
 */
static void determine_wig_max_ang() {
	for (uint16_t i = 0; i < NUMBER_OF_TURRETS; i++) {
		if (turret_wiggle_var_store[i].wiggle_enabled) {
			int16_t ang = (turret_get_max_ang_yaw((turret_id_t)i) - turret_get_min_ang_yaw((turret_id_t)i)) / 4; // allowed to wiggle half of min and max angle to allow for manual aiming
			if (wig_max_ang > ang) {
				wig_max_ang = ang;
			}
		}
	}
}

/**
 *  @brief initializes a specified wiggle_var_store index
 *  @param turret_id for specifying the index in wiggle_var_store to be initialized
 *  @retval motor_control_error_t whether or not initialization was completed properly
 */
static motor_control_error_t wiggle_var_store_init(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	}
	turret_wiggle_var_store[turret_id].wiggle_enabled = true;
	turret_wiggle_var_store[turret_id].wig_turret_yaw_initial_ang = 0.0f;
	turret_wiggle_var_store[turret_id].wig_adjusted_turret_max_ang_yaw = 0;
	turret_wiggle_var_store[turret_id].wig_adjusted_turret_min_ang_yaw = 0;
	turret_wiggle_var_store[turret_id].yaw_dis_max = 0;
	turret_wiggle_var_store[turret_id].yaw_dis_min = 0;
	turret_wiggle_var_store[turret_id].chassis_initial_ang = 0.0f;
	turret_wiggle_var_store[turret_id].turret_locked = false;
	return TURRET_OK;
}

/**
 *  @brief calculates and stores the current angular velocity and acceleration of the chassis
 *			   you can change the time step for calculation purposes by changing the value of 
 *			   ANGULAR_VELOCITY_STEPS
 *  @param none
 *  @retval none
 */
static void calc_chassis_acceleration_velocity() {		
	float alpha = 0.01f;
	
	// calculate angular velocity
	chassis_angular_velocity[curr_angular_velocity_step] = alpha 
		* (1000.0f * (find_shortest_wrapped_distance(-180.0f, 180.0f, imu.yaw, prev_imu_yaw)))
			+ (1.0f - alpha) * chassis_angular_velocity[(curr_angular_velocity_step + 1) % 2];;
	prev_imu_yaw = imu.yaw;
	
	chassis_angular_acceleration = alpha * ( (chassis_angular_velocity[curr_angular_velocity_step] 
		- chassis_angular_velocity[(curr_angular_velocity_step + 1) % 2]))
			+ (1.0f - alpha) * chassis_angular_acceleration;
	
	// increment step
	curr_angular_velocity_step = (curr_angular_velocity_step + 1) % 2;
}

/**
 *  @brief calculates and returns the current chassis angle relative to the angle at which the chassis started at
 * 		 		 not that this returns different values depending on the turret id, because locks can start at different
 * 	 		 	 times, so the initial chassis angle can be different for different turrets
 *  @param turret_id the specified turret
 *  @retval float that is the current chassis angle relative to when a turret lock was started
 */
static float calc_chassis_rel_init(turret_id_t turret_id) {
	float chassis_rel_init = imu.yaw - turret_wiggle_var_store[turret_id].chassis_initial_ang;
	chassis_rel_init -= (360.0f) * floor((chassis_rel_init + 360.0f/2) * (1.0f / (360.0f)));
	chassis_rel_init *= -1;
	return chassis_rel_init;
}

/**
 *  @brief sets chassis_wiggling to the parameter inputted
 *  @param is_wiggling whether or not the chassis should start wiggling
 *  @retval none
 */
void set_chassis_wiggling(bool is_wiggling) {
	chassis_wiggling = is_wiggling;
}

/**
 *  @brief If you want to lock a turret at its current angle, call this function to enable the lock, 
 * 			   specifically for locking a single turret, not for general wiggle task.
 *  @param id the turret id you would like to start locking
 *  @retval none
 */
void start_turret_lock(uint16_t id) {
	turret_id_t turret_id = (turret_id_t) id;
	
	float chassis_ang = imu.yaw;
	
	turret_wiggle_var_store[turret_id].wig_adjusted_turret_max_ang_yaw = 
		turret_get_max_ang_yaw(turret_id);
	turret_wiggle_var_store[turret_id].wig_adjusted_turret_min_ang_yaw = 
		turret_get_min_ang_yaw(turret_id);
	
	turret_wiggle_var_store[turret_id].yaw_dis_max = turret_get_max_ang_yaw(turret_id) - 
		turret_get_yaw_ang_actual(turret_id);
	turret_wiggle_var_store[turret_id].yaw_dis_min = turret_get_min_ang_yaw(turret_id) - 
		turret_get_yaw_ang_actual(turret_id);
		
	wig_turret_init(turret_id);
	
	turret_wiggle_var_store[turret_id].chassis_initial_ang = chassis_ang;
	turret_wiggle_var_store[turret_id].turret_locked = true;
}

/**
 *  @brief Stop a turret from locking to a position the lock, for either wiggling 
 * 				 with chassis or locking 
 *  @param id the turret id to have its turret lock stop
 *  @retval none
 */
void end_turret_lock(uint16_t id) {
	turret_id_t turret_id = (turret_id_t) id;
	turret_wiggle_var_store[turret_id].turret_locked = false;
}

/**
 *  @brief Returns the drive speed factor to be multiplied to the overall speed 
 *         of the mecanum drive train when wiggling
 *  @retval drive speed factor to be multipled to the drive train when wiggling
 */
float get_wiggle_drive_speed_factor() {
	if (robot_mode == WIGGLE_MODE) {
		return WIGGLE_DRIVE_SPEED_FACTOR;
	} else {
		return 1.0f;
	}
}

#endif
