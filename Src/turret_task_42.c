#include "turret_task_42.h"

#if defined(TARGET_HERO)

#define PID_KP_PITCH_42 60.0f
#define PID_KI_PITCH_42 0.0f
#define PID_KD_PITCH_42 400.0f

#define PID_KP_YAW_42 60.0f
#define PID_KI_YAW_42 0.0f
#define PID_KD_YAW_42 200.0f

#define PIT_ANG_MIN_42 (-20)
#define PIT_ANG_MAX_42 (80)
#define YAW_ANG_MIN_42 (-100)
#define YAW_ANG_MAX_42 (100)

#define SMOOTHING_FACTOR_YAW_42 (0.15f)
#define SMOOTHING_FACTOR_PITCH_42 (0.15f)

#define YAW_42_ZERO_ENCODER (0)
#define PITCH_42_ZERO_ENCODER (0)

#define GRAVITATIONAL_TORQUE_CONSTANT_42 (9000)

void specific_turret_init_42(uint8_t turret_id) {
	
	position_motor_init_angles(
		MOTOR_YAW_TURRET_42_ID, 
		TURRET_ZERO_ANG + YAW_ANG_MAX_42, 
		TURRET_ZERO_ANG + YAW_ANG_MIN_42,
		TURRET_ZERO_ANG
	);
	position_motor_init_angles(
		MOTOR_PITCH_TURRET_42_ID, 
		TURRET_ZERO_ANG + PIT_ANG_MAX_42,	
		TURRET_ZERO_ANG + PIT_ANG_MIN_42, 
		TURRET_ZERO_ANG
	);
	position_motor_init_pid(
		MOTOR_YAW_TURRET_42_ID,
	  AVERAGING_PID,
		PID_KP_YAW_42,
		PID_KI_YAW_42,
		PID_KD_YAW_42
	);
	position_motor_init_pid(
		MOTOR_PITCH_TURRET_42_ID,
		AVERAGING_PID,
		PID_KP_PITCH_42, 
		PID_KI_PITCH_42, 
		PID_KD_PITCH_42
	);
}

void turret_task_42(void const* argu) {
	turret_init(
		TURRET_42_ID, 
		MOTOR_YAW_TURRET_42_ID, 
		MOTOR_PITCH_TURRET_42_ID,
		get_motor(&can1, TURRET_YAW_MOTOR_42), 
		get_motor(&can1, TURRET_PITCH_MOTOR_42),
		YAW_42_ZERO_ENCODER,
		PITCH_42_ZERO_ENCODER, 	
		SMOOTHING_FACTOR_YAW_42,
		SMOOTHING_FACTOR_PITCH_42
	);
		
	specific_turret_init_42(TURRET_42_ID);
	
	float yaw_user_acumulated = TURRET_ZERO_ANG;
	float pitch_user_acumulated = TURRET_ZERO_ANG;
	
	robot_init[TURRET42_INIT] = true;
	waiting_for_init();

	while(1) {
		
		yaw_user_acumulated = turret_get_yaw_ang_setpoint(TURRET_42_ID) - interface_bind_yaw_ctrl();	// consider motor inverted
		pitch_user_acumulated = turret_get_pitch_ang_setpoint(TURRET_42_ID) - interface_bind_pitch_ctrl();
		
		turret_aim_handler(TURRET_42_ID, &yaw_user_acumulated, &pitch_user_acumulated);
		turret_run_pid(TURRET_42_ID);
			
			//add this in when you know the correct gravitational_torque_constant		
		//position_motor_adjust_voltage_desired(MOTOR_PITCH_TURRET_42_ID,
		//	- (int32_t) (GRAVITATIONAL_TORQUE_CONSTANT_42 * sinf( PI / 180 * turret_get_pitch_ang_actual(TURRET_42_ID))) );

		osDelay(1);
	}
}

#endif
