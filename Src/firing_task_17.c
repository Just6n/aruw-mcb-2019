#include "firing_task_17.h"
#include "turret_task_17.h"
#include "ref_comms.h"
#include "cv_comms.h"

#if defined(TARGET_SOLDIER) || (TARGET_DRONE) || (TARGET_SENTINEL)

#define PID_KP_17AGITATOR 2.0f
#define PID_KI_17AGITATOR 0.0f
#define PID_KD_17AGITATOR 60.0f

#define TURRET_HEAT_BUFFER_17 (70.0f) // 3 shot buffer, assuming shooting at roughly ~23 m/s

#define AGITATOR_17_GEAR_RATIO 36
#if defined(TARGET_SENTINEL)
#define BURST_COUNT_17 4
#define MIN_TIME_FROM_LAST_CV_AIM_DATA_TO_FIRE 100 // minimum time from last cv aim data before deciding to fire
#define STOP_TIME_TO_FIRE 300 // time of stopping the drive before shooting
#define MIN_TIME_BETWEEN_AUTO_FIRING 2000 // minimum time between auto firing
#else
#define BURST_COUNT_17 4
#endif
#define TIME_AGITATOR_ROTATE (125) // the maximum time in milliseconds it takes for the agitator to rotate one spot over, used for unjamming

// friction wheel defines
#define FRICTION_WHEEL_INIT_WAIT (2000)
#define FRICTION_WHEEL_ZERO_CYCLE (0.1f)
#define FRICTION_WHEEL_DUTY_CYCLE_FAST (0.155f) // this fires at ~20 m/s when firing multiple shots at a time, higher if you only fire once
#define FRICTION_WHEEL_DUTY_CYCLE_SLOW (0.135f) // slower speed, firing at ~10 m/s

// hopper defines
#define HOPPER_COVER_DUTY_CYCLE_OPEN (0.18f)
#define HOPPER_COVER_DUTY_CYCLE_CLOSE (0.05f)
#define HOPPER_COVER_SIGNAL_RUNTIME (500) // hopper cover receives a signal only for 500 ms after receiving an input

#define AUTO_GIMBAL_ERROR_ANGLE_MARGIN (5) // degrees away from target before deciding auto firing
#define AUTO_GIMBAL_ON_TARGET(motor) (fabs(position_motor_get_ang_actual(motor) - position_motor_get_curr_ang_setpoint(motor)) <= AUTO_GIMBAL_ERROR_ANGLE_MARGIN)
#define MAX_TAP_FIRE_TIME (200.0f) // max time in ms given to tap fire before fully auto firing when requesting full auto fire

static bool prev_single_triggered_17 = false;
static bool prev_burst_triggered_17 = false;
static bool prev_full_triggered_17 = false;
// does not turn on the friction wheels until a request to the agitator has been sent
static bool friction_wheels_wait_for_agitator_request = true;
static bool friction_wheels_calibrated = false;
static bool full_auto_single_shot_fired = false; // true if a single shot has already been fired in full auto firing mode
static uint32_t dt_brushless_init = 0; // variable for timer calculation determing if the friction wheel is initialized
static uint32_t friction_wheels_start_timestamp = 0; // timestamp in ms when friction wheels start running
static uint32_t last_non_full_auto_fire_request = 0; // timestamp of the most recent time that full auto firing was not requested
#if defined (TARGET_SENTINEL)
bool sentinel_ready_to_fire = false;
uint32_t sentinel_shot_fired_timestamp = 0;
#endif

static void friction_wheel_task(void);
static void agitator_init_17(void);
static void agitator_task_17(void);
static void hopper_cover_handler(void);

/*
 * spin friction wheels and turn agitator
 */
void firing_task_17(void const* argu) {
	agitator_init_17();
	while(1){
		friction_wheel_task();
		agitator_task_17();
		hopper_cover_handler();
		osDelay(1);
	}
}

static void agitator_task_17() {
	if (!referee.robot_data.gimbal_has_power || !referee.robot_data.shooter_has_power) {
		return;
	}
	if (!is_agitator_firing(AGITATOR_17) && referee.robot_data.turret.heat_17 < referee.robot_data.turret.heat_limit_17 - TURRET_HEAT_BUFFER_17) {
		#if defined(TARGET_SENTINEL)
		cv_comms_turret_aim_data_t aim_data;
		extern bool sentinel_power_buffer_mode;
		if (robot_mode == AUTO_MODE) {
			if(robot_aim_mode == AIM_CV
				 && cv_comms_get_last_aim_data(&aim_data)
				 // target is detected
			   && aim_data.has_target
				 // last target CV aim data was recent
				 && osKernelSysTick() - aim_data.receive_timestamp < MIN_TIME_FROM_LAST_CV_AIM_DATA_TO_FIRE
			   // has been some minimum time from the last burst fire
				 && osKernelSysTick() - sentinel_shot_fired_timestamp > MIN_TIME_BETWEEN_AUTO_FIRING
				 // turret is aiming within some margin of the received CV aim data
				 && AUTO_GIMBAL_ON_TARGET(MOTOR_YAW_TURRET_17_ID) 
				 && AUTO_GIMBAL_ON_TARGET(MOTOR_PITCH_TURRET_17_ID)
				 // sentinel has not been recently hit and is using the power bufferW
				 && !sentinel_power_buffer_mode)
			{
				sentinel_ready_to_fire = true;
				extern bool sentinel_drive_is_stopped;
				extern uint32_t sentinel_drive_stopped_timestamp;
				if (sentinel_drive_is_stopped 
						// stop for some time before firing
						&& osKernelSysTick() - sentinel_drive_stopped_timestamp > STOP_TIME_TO_FIRE)
				{
					agitator_request_fire(AGITATOR_17, BURST_COUNT_17);
					sentinel_shot_fired_timestamp = osKernelSysTick();
				}
			} else {
				sentinel_ready_to_fire = false;
			}
		} else {
		#endif
		if (friction_wheels_calibrated && osKernelSysTick() - friction_wheels_start_timestamp > FRICTION_WHEEL_INIT_WAIT) {
			if (!prev_single_triggered_17 && interface_bind_request_fire_single_17()) {
				agitator_request_fire(AGITATOR_17, 1);
			} else if (!prev_burst_triggered_17 && interface_bind_request_fire_burst_17()) {
				agitator_request_fire(AGITATOR_17, BURST_COUNT_17);
			} else if (interface_bind_request_fire_full_17()) {
				if (!full_auto_single_shot_fired && osKernelSysTick() - last_non_full_auto_fire_request < MAX_TAP_FIRE_TIME) {
					// single shot on tap fire
					full_auto_single_shot_fired = true;
					// burst count is reliable and consistent to have balls come out on tap with
					// the current soldier barrel rather than a single ball
					agitator_request_fire(AGITATOR_17, BURST_COUNT_17);
				} else if (osKernelSysTick() - last_non_full_auto_fire_request >= 200) {
					// faster firing rate on hold
					agitator_request_fire(AGITATOR_17, 2);
				}
			} else if (!interface_bind_request_fire_full_17()) {
				full_auto_single_shot_fired  = false;
				last_non_full_auto_fire_request = osKernelSysTick();
			}
		}
		#if defined(TARGET_SENTINEL)
		}
		#endif
	}
	agitator_handler(AGITATOR_17);
	position_motor_run_pid(MOTOR_AGITATOR_ID);
	prev_single_triggered_17 = interface_bind_request_fire_single_17();
	prev_burst_triggered_17 = interface_bind_request_fire_burst_17();
	prev_full_triggered_17 = interface_bind_request_fire_full_17();
	if (friction_wheels_wait_for_agitator_request 
		  && (prev_single_triggered_17 || prev_burst_triggered_17 || prev_full_triggered_17)) 
	{
		friction_wheels_wait_for_agitator_request = false;
	}		
}

static void agitator_init_17() {
	agitator_motor_init(
		AGITATOR_17, 
		MOTOR_AGITATOR_ID, 
		get_motor(&can1, AGITATOR_MOTOR_17), 
		PID_KP_17AGITATOR, 
		PID_KI_17AGITATOR, 
		PID_KD_17AGITATOR, 
		AGITATOR_17_GEAR_RATIO, 
		TIME_AGITATOR_ROTATE
	);
}

/* 
 * Spins friction wheels with constant signal
 */
static void friction_wheel_task() {
	if (!referee.robot_data.shooter_has_power || robot_mode == KILL_MODE) {
		// no power to friction wheels, reset time tracker to reinitialize
		// the friction wheels when there is power
		dt_brushless_init = osKernelSysTick();
		friction_wheels_wait_for_agitator_request = true;
		friction_wheels_calibrated = false;
	}
	if (osKernelSysTick() - dt_brushless_init < FRICTION_WHEEL_INIT_WAIT
			#if defined(TARGET_SOLDIER) || defined (TARGET_SENTINEL)
		  || friction_wheels_wait_for_agitator_request
			#endif
	)	{
		PWM_SetDuty(FRICTION_WHEEL_RIGHT_GPIO, FRICTION_WHEEL_ZERO_CYCLE);
		PWM_SetDuty(FRICTION_WHEEL_LEFT_GPIO, FRICTION_WHEEL_ZERO_CYCLE);
	} else {
		if (!friction_wheels_calibrated) {
			friction_wheels_calibrated = true;
			friction_wheels_start_timestamp = osKernelSysTick();
		}
		if (interface_bind_request_fire_slow_speed()) {
			PWM_SetDuty(FRICTION_WHEEL_RIGHT_GPIO, FRICTION_WHEEL_DUTY_CYCLE_SLOW);
			PWM_SetDuty(FRICTION_WHEEL_LEFT_GPIO, FRICTION_WHEEL_DUTY_CYCLE_SLOW);
		} else {
			PWM_SetDuty(FRICTION_WHEEL_RIGHT_GPIO, FRICTION_WHEEL_DUTY_CYCLE_FAST);
			PWM_SetDuty(FRICTION_WHEEL_LEFT_GPIO, FRICTION_WHEEL_DUTY_CYCLE_FAST);
		}
	}
}

static void hopper_cover_handler(void) {
	#if defined(TARGET_SOLDIER)
	if (interface_bind_request_open_hopper()) {
		PWM_SetDuty(HOPPER_COVER_PIN, HOPPER_COVER_DUTY_CYCLE_OPEN);
	} else {
		PWM_SetDuty(HOPPER_COVER_PIN, HOPPER_COVER_DUTY_CYCLE_CLOSE);
	}
	#endif
}
#endif
