#include "math_user_utils.h"
#include "math.h"

/** 
 *  @brief performs a rotation matrix on the given x and y components of a vector
 *  @param x the x component of the vector to be rotated
 *  @param y the y component of the vector to be rotated
 *  @param angle the angle by which to rotate the vector <x, y>
 *  @retval none
 */
void rotation_matrix_2d(float* x, float* y, float angle) {
	float x_temp = *x;
	*x = (*x) * cos(DEG_TO_RAD(angle)) - *y * sin(DEG_TO_RAD(angle));
	*y = x_temp * sin(DEG_TO_RAD(angle)) + *y * cos(DEG_TO_RAD(angle));
}

/** 
 *  @brief  Performs weighted averaging on previous and last value
 *          calculation performed and returned is as follows:
 *          alpha * new value + (1 - alpha) * previous weighted value
 *          so, alpha represents what weight you would like to place on 
 *          the current, new value.
 *          if alpha is not between 0 and 1 inclusive, 0 will be returned.
 *  @param  prev_value the previous value to be taken into the averaging
 *  @param  new_value the value new value to be averaged
 *  @param  alpha the weighting factor, between 0 and 1 inclusive
 *  @retval the new value with the weighted average, taking into account
 *          current and previous values
 */
float low_pass_filter(float prev_value, float new_value, float alpha) {
	if (alpha < 0.0f || alpha > 1.0f) {
		return 0.0f;
	}
	return alpha * new_value + (1.0f - alpha) * prev_value;
}

/** 
 *  @brief Evaluates the sign of the given number
 *  @param num the number to be evaluated
 *  @retval -1 if sign is negative, 1 if sign is positive
 */ 
int8_t sign(float num) {
	if (num > 0) return 1;
	if (num < 0) return -1;
	return 0;
}

float limit_float(float val, float min, float max) {
	if (val < min) {
		return min;
	} else if (val > max) {
		return max;
	} else {
		return val;
	}
}

void circular_buffer_init(circular_buffer_t *buffer, float initial_value) {
	buffer->head = 0;
	for (size_t i = 0; i < buffer->size; i++) {
		buffer->data[i] = initial_value;
	}
}

void circular_buffer_append(circular_buffer_t *buffer, float new_value) {
	buffer->data[buffer->head] = new_value;
	buffer->head = (buffer->head + 1) % buffer->size;
}

float circular_buffer_mean(circular_buffer_t *buffer) {
	float mean = 0;
	for (size_t i = 0; i < buffer->size; i++) {
		mean += buffer->data[i];
	}
	
	return mean / buffer->size;
}

/** 
 *  @brief  Computes the difference between two values (val1 - val2), accounting for
 *          wrapping between upper and lower bounds
 *  @note   credit to SeriouslyCommonLib: https://github.com/Team488/SeriouslyCommonLib/
 *          blob/master/src/main/java/xbot/common/math/ContiguousDouble.java
 *  @param  upper_bound, lower_bound the upper and lower wrapping bounds
 *  @param  val1, val2 the two values to compare
 *  @retval the computed difference 
 */
float find_shortest_wrapped_distance(float upper_bound, float lower_bound, float val1, float val2) {		
	float above_diff = val1 - (upper_bound + (val2 - lower_bound));
	float below_diff = val1 - (lower_bound - (upper_bound - val2));
	float std_diff = val1 - val2;
	double finalDiff = std_diff;

	if (fabs(above_diff) < fabs(below_diff)
		&& fabs(above_diff) < fabs(std_diff)
	) {
		finalDiff = above_diff;
	} else if (fabs(below_diff) < fabs(above_diff)
		&& fabs(below_diff) < fabs(std_diff)
	) {
			finalDiff = below_diff;
	}
	return finalDiff;
}

/** 
 *  @brief  calculates the wrapped version of angle_to_wrap wrapped from 0 to angle_to_wrap_around
 *  @param  angle_to_wrap angle to do wrapping calculations on 
 *  @param  angle_to_wrap_around the maximum angle, which the angle_to_wrap will be wrapped to
 *  @retval the wrapped value of angle_to_wrap
 */
float wrap_around_angle(float angle_to_wrap, float angle_to_wrap_around) {
  return angle_to_wrap - floor((angle_to_wrap) / angle_to_wrap_around) * angle_to_wrap_around;
}
