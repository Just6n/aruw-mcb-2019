#include "stdlib.h"
#include "stm32f4xx_hal.h"
#include "led_error_handler.h"

/**
 * @brief  Sets a pin on/off according to whether the bit-th bit of value is 1 or 0
 * @param  GPIOx where x can be (A..K) to select the GPIO peripheral for STM32F429X device or
 *   	     x can be (A..I) to select the GPIO peripheral for STM32F40XX and STM32F427X devices.
 * @param  GPIO_Pin specifies the port bit to be written.
 * @param  value is the byte to be compared  
 * @param  bit specifies which bit 0-7 (LSB is 0) of value to check whether it is 0 or 1
 * @retval none
 */
void led_set_pin_for_bit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, uint8_t value, int bit) {
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, ((value >> bit) & 0x1) ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

/**
 * @brief  Sets the G1-G8 LEDs to represent a given byte value
 * @param  errorCode is the value to be represented as a binary number in LEDs
 *         reportError(4) sets the LED pattern to 00000100 where 1 is on, 0 is off
 * @retval none
 */
void led_set_error_indicator(uint8_t errorCode) {
	led_set_pin_for_bit(GPIOG, GPIO_PIN_1, errorCode, 0);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_2, errorCode, 1);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_3, errorCode, 2);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_4, errorCode, 3);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_5, errorCode, 4);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_6, errorCode, 5);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_7, errorCode, 6);
	led_set_pin_for_bit(GPIOG, GPIO_PIN_8, errorCode, 7);
}

void led_report_hal_error(HAL_StatusTypeDef hal_error, uint8_t source_area) {
	uint8_t hal_error_code = 0x00;
	switch (hal_error) {
		case HAL_ERROR:
			hal_error_code = 0x01; break;
		case HAL_BUSY:
			hal_error_code = 0x02; break;
		case HAL_TIMEOUT:
			hal_error_code = 0x03; break;
		case HAL_OK:
			// We don't need to indicate success
			break;
	}

	led_set_error_indicator(hal_error_code | (source_area << 2));
}

void led_report_general_error(uint8_t source_area) {
	led_set_error_indicator(0x03 | (source_area << 2));
}

void can_handle_error(HAL_StatusTypeDef error_code) {
	if (error_code != HAL_OK) {
		led_report_hal_error(error_code, ERROR_AREA_CAN);
	}
}
