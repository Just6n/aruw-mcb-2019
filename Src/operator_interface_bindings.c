/*
 * operations_interface_binding.c
 * 
 * this file centralizes all remote and computer values in one place
 */

#include "operator_interface_bindings.h"
#include "remote.h"
#include "drive_task.h"
#include "control_mode.h"
#include <stdlib.h>
#include "math.h"

#define CHASSIS_MOTION_KEY_SCALAR 1000
#define KEYBOARD_SLOW_DRIVE_MULTIPLIER (0.25f)
#define REMOTE_DEADBAND 5

#define REMOTE_TURRET_DIVISOR 1200.0f
#define MOUSE_DIVISOR 100.0f	// divide mouse input by this value

extern bool chassis_rotate_to_turret;

float interface_bind_move_horizontal(){
	float computer_horizontal_val = 0.0f;
	if (KEY_LEFT_PRESSED()) {
		computer_horizontal_val -= CHASSIS_MOTION_KEY_SCALAR;
	}
	if (KEY_RIGHT_PRESSED()) {
		computer_horizontal_val += CHASSIS_MOTION_KEY_SCALAR;
	}
	return (computer_horizontal_val + REMOTE_CTRL_CHASSIS_HORIZONTAL());
}

float interface_bind_move_vertical(){
	float computer_vertical_val = 0.0f;
	if (KEY_FOWARD_PRESSED()) {
		computer_vertical_val += CHASSIS_MOTION_KEY_SCALAR;
	} 
	if (KEY_BACKWARD_PRESSED()) {
		computer_vertical_val -= CHASSIS_MOTION_KEY_SCALAR;
	}
	return (computer_vertical_val + REMOTE_CTRL_CHASSIS_VERTICAL());
}

float interface_bind_rotate_chassis(){
	float computer_rotate_val = 0.0f;
	if(KEY_ROTATE_CLOCK_PRESSED()){
		computer_rotate_val += CHASSIS_MOTION_KEY_SCALAR;
	} 
	if(KEY_ROTATE_COUNTERCLOCK_PRESSED()){
		computer_rotate_val -= CHASSIS_MOTION_KEY_SCALAR;
	}
	return ((REMOTE_CONTROLLING_CHASSIS_ROTATION() || REMOTE_CONTROLLING_AUTO_AIM()) * REMOTE_CTRL_ROTATE() + computer_rotate_val);
}

float interface_bind_keyboard_slow_drive(){
	if(key_toggled(KEY_LOW_CHASSIS_SPEED)){
		return KEYBOARD_SLOW_DRIVE_MULTIPLIER;
	} else {
		return 1.0f;
	}
}

float interface_bind_yaw_ctrl(){
	#if !defined(TARGET_ENGINEER)
	if(REMOTE_CONTROLLING_TURRET()){
		float yaw_val = 0.0f;
		if (fabs(REMOTE_CTRL_YAW()) > REMOTE_DEADBAND) {
			yaw_val += REMOTE_CTRL_YAW() / REMOTE_TURRET_DIVISOR;
		}
		return yaw_val + COMPUTER_CTRL_YAW() / MOUSE_DIVISOR;
	} else {
		return COMPUTER_CTRL_YAW() / MOUSE_DIVISOR;
	}
	#else
	return 0;
	#endif
}

float interface_bind_pitch_ctrl(){
	#if !defined(TARGET_ENGINEER)
	float pitch_val = 0.0f;
	if (REMOTE_CONTROLLING_TURRET() && fabs(REMOTE_CTRL_PITCH()) > REMOTE_DEADBAND) {
		pitch_val = -REMOTE_CTRL_PITCH() / REMOTE_TURRET_DIVISOR;
	}
	return pitch_val + COMPUTER_CTRL_PITCH() / MOUSE_DIVISOR;
	#else
	return 0;
	#endif
}

bool interface_bind_request_grab(){
	#if defined(TARGET_ENGINEER)
	return key_toggled(KEY_GRAB);
	#else
	return false;
	#endif
}

bool interface_bind_request_grab_toggle(){
	#if defined(TARGET_ENGINEER)
	return key_toggled(KEY_GRAB_TGGL);
	#else
	return false;
	#endif
}

bool interface_bind_request_lift_toggle(){
	#if defined(TARGET_ENGINEER)
	return key_toggled(KEY_LIFT_TGGL);
	#else
	return 0;
	#endif
}

bool interface_bind_request_sentinel_manual_mode() {
	#if defined(TARGET_SENTINEL)
	return REMOTE_CTRL_SENTINEL_MANUAL_MODE();
	#else
	return false;
	#endif
}

bool interface_bind_request_base_ctrl_mode(){
	#if defined(TARGET_SOLDIER)
	return REMOTE_CTRL_BASE_CTRL() || key_toggled(KEY_BASE_CTRL);
	#else
	// change this to false once we add other modes to other robots (currently def to BASE_CTRL_MODE)
	return true;
	#endif
}

bool interface_bind_request_wiggle(){
	#if defined(TARGET_SOLDIER)
	return (REMOTE_CTRL_WIGGLE() || key_toggled(KEY_WIGGLE)) && robot_aim_mode != AIM_CV;
	#else
	return false;
	#endif
}
bool interface_bind_request_turret_lock(){
	#if defined(TARGET_SOLDIER)
	return REMOTE_CTRL_LOCK_TURRET() || key_toggled(KEY_TURRET_LOCK);
	#else
	return false;
	#endif
}

bool interface_bind_request_kill(){
	return REMOTE_CTRL_KILL() || key_toggled(KEY_KILL);
}

bool interface_bind_request_dump(void){
	#if defined(TARGET_ENGINEER)
	return key_toggled(KEY_DUMP) || REMOTE_CTRL_DUMP();
	#else
	return false;
	#endif
}

bool interface_bind_request_tow(void){
	#if defined(TARGET_ENGINEER)
	return key_toggled(KEY_TOW) || REMOTE_CTRL_TOW();
	#else
	return false;
	#endif
}

#if !defined(TARGET_ENGINEER)

bool interface_bind_request_fire_single_17(){
	return REMOTE_CTRL_FIRE_SINGLE() || COMPUTER_FIRE_SINGLE();
}

bool interface_bind_request_fire_burst_17(){
	return REMOTE_CTRL_FIRE_BURST() || COMPUTER_FIRE_BURST();
}

bool interface_bind_request_fire_full_17(){
	return REMOTE_CTRL_FIRE_FULL() || COMPUTER_FIRE_AUTO();
}

bool interface_bind_request_fire_slow_speed() {
	#if defined(TARGET_SOLDIER)
	return KEY_LOW_FIRE_SPEED_PRESSED();
	#else
	return false;
	#endif
}

#endif

bool interface_bind_request_auto_aim(){
	#if defined(TARGET_SOLDIER)
	return REMOTE_CONTROLLING_AUTO_AIM() || COMPUTER_CONTROLLING_AUTO_AIM();
	#else
	return false;
	#endif
}

bool interface_bind_request_auto_fire() {
	#if defined(TARGET_SOLDIER)
	return REMOTE_CONTROLLING_CV_FIRE();
	#else
	return false;
	#endif
}

bool interface_bind_toggle_disable_auto_rotate_chassis() {
	#if defined(TARGET_SOLDIER)
	return key_toggled(KEY_DISABLE_AUTO_ROTATE);
	#else
	return false;
	#endif
}

bool interface_bind_request_user_unjam() {
	#if defined(TARGET_SOLDIER)
	return KEY_UNJAM_REQUEST_PRESSED();
	#else
	return false;
	#endif
}

bool interface_bind_request_open_hopper() {
	# if defined(TARGET_SOLDIER)
	return key_toggled(KEY_HOPPER);
	#else
	return false;
	#endif
}
