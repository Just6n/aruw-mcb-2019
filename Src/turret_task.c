/* 
file: turret_task.c 
v2.0

This file contains the implementation of multiturrets, so we can handle more than one turret easily
*/

#if !defined(TARGET_ENGINEER)

#include "turret_task.h"
#include "stm32f4xx_hal.h"
#include "can.h"
#include "dji_motor.h"
#include "math.h"
#include "cmsis_os.h"
#include "pid.h"
#include "stdbool.h"
#include "control_mode.h"
#include "drive_task.h"
#include "math.h"
#include "gpio.h"
#include "pwm.h"
#include "imu_task.h"
#include "wiggle_task.h"
#include "operator_interface_bindings.h"
#include <stdio.h>
#include "ref_comms.h"

typedef struct {
	position_control_motor_id_t yaw_motor;
	position_control_motor_id_t pitch_motor;
	bool position_and_range_init;
	bool turret_inited;
	int16_t yaw_zero_encoder;
	int16_t pitch_zero_encoder;
} turret_t;

turret_t turrets_data_store[NUMBER_OF_TURRETS];

bool turret_check_id(turret_id_t turret_id) {
	return turret_id < NUMBER_OF_TURRETS;
}

/*
 * Initialize the turret
 */
motor_control_error_t turret_init(
		turret_id_t turret_id, 
		position_control_motor_id_t yaw_position_control_motor, 
		position_control_motor_id_t pitch_position_control_motor, 
		dji_motor_t* yaw_motor, 
		dji_motor_t* pitch_motor, 
		int16_t yaw_zero_encoder,
		int16_t pitch_zero_encoder, 
		float motor_smoothing_factor_yaw,
		float motor_smoothing_factor_pitch
	) {
		
	if (!turret_check_id(turret_id)) {	// don't init more than once
		return TURRET_ID_ERROR;
	} else if (turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	
	turrets_data_store[turret_id].turret_inited = true;
	
	turrets_data_store[turret_id].position_and_range_init = false;

	position_motor_init(yaw_position_control_motor, yaw_motor);
	position_motor_init(pitch_position_control_motor, pitch_motor);
	
	turrets_data_store[turret_id].yaw_motor = yaw_position_control_motor;
	turrets_data_store[turret_id].pitch_motor = pitch_position_control_motor;
	
	turrets_data_store[turret_id].yaw_zero_encoder = yaw_zero_encoder;
	turrets_data_store[turret_id].pitch_zero_encoder = pitch_zero_encoder;
	
	position_motor_init_avg_output_factor(turrets_data_store[turret_id].yaw_motor, motor_smoothing_factor_yaw);
	position_motor_init_avg_output_factor(turrets_data_store[turret_id].pitch_motor, motor_smoothing_factor_pitch);
	
	return TURRET_OK;
}

/*
 * Initializes the motors' zero position to be the initial angle position if not initialized already.
 * Sets the encoder boundaries for the pitch and yaw motors.
 *
 * @retval true if the position and range of the turret has been successfully initialized, false otherwise.
 */
bool turret_init_position_and_range(turret_id_t turret_id) {
  if (!turrets_data_store[turret_id].position_and_range_init
			&& position_motor_get_motor_online(turrets_data_store[turret_id].yaw_motor)
			&& position_motor_get_motor_online(turrets_data_store[turret_id].pitch_motor)) 
	{
		turrets_data_store[turret_id].position_and_range_init = true;
		init_shift_ang(turrets_data_store[turret_id].yaw_motor, turrets_data_store[turret_id].yaw_zero_encoder);
		init_shift_ang(turrets_data_store[turret_id].pitch_motor, turrets_data_store[turret_id].pitch_zero_encoder);
	}
	return turrets_data_store[turret_id].position_and_range_init;
}

/*
 * Deals with determining the correct desired encoder values for pitch and yaw motor
 * Outputs these values via can_dji_motor_set
 */
motor_control_error_t turret_aim_handler(turret_id_t turret_id, float* yaw_ang, float* pitch_ang) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	} else if (!referee.robot_data.gimbal_has_power && referee.online) {
		return TURRET_NO_POWER;
	}
	// Initialize the center position of the turret
	if (!turrets_data_store[turret_id].position_and_range_init) { // exit turret_update_aim if the position and range are not initialized
		if (turret_init_position_and_range(turret_id)) {
			return TURRET_OK;
		} else {
			return TURRET_INIT_ERROR;
		}
	}
	
	// limit user yaw and pitch desired angles (different from position motor ang_setpoints)
	if (*yaw_ang < turret_get_min_ang_yaw(turret_id)) {
		*yaw_ang = turret_get_min_ang_yaw(turret_id);
	} else if (*yaw_ang > turret_get_max_ang_yaw(turret_id)) {
		*yaw_ang = turret_get_max_ang_yaw(turret_id);
	}
	
	if (*pitch_ang < turret_get_min_ang_pitch(turret_id)) {
		*pitch_ang = turret_get_min_ang_pitch(turret_id);
	} else if (*pitch_ang > turret_get_max_ang_pitch(turret_id)) {
		*pitch_ang = turret_get_max_ang_pitch(turret_id);
	}
	
	float prev_ang_setpoint = position_motor_get_curr_ang_setpoint(turrets_data_store[turret_id].yaw_motor);
	
	set_ang_des(turrets_data_store[turret_id].yaw_motor, *yaw_ang);
	set_ang_des(turrets_data_store[turret_id].pitch_motor, *pitch_ang);
	
	#if defined (TARGET_SOLDIER)
	*yaw_ang = turret_wiggle_handler(turret_id, position_motor_get_curr_ang_setpoint(turrets_data_store[turret_id].yaw_motor) - prev_ang_setpoint, *yaw_ang);
	set_ang_des(turrets_data_store[turret_id].yaw_motor, *yaw_ang);	// reset again after wiggling
	#endif
	return TURRET_OK;
}

motor_control_error_t turret_run_pid(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	} else if (!referee.robot_data.gimbal_has_power && referee.online) {
		return TURRET_NO_POWER;
	}
  position_motor_run_pid(turrets_data_store[turret_id].yaw_motor);
	position_motor_run_pid(turrets_data_store[turret_id].pitch_motor);	
	return TURRET_OK;
}

float turret_get_yaw_ang_setpoint(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return  position_motor_get_curr_ang_setpoint(turrets_data_store[turret_id].yaw_motor);
}

float turret_get_pitch_ang_setpoint(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_curr_ang_setpoint(turrets_data_store[turret_id].pitch_motor);
}

int16_t turret_get_max_ang_yaw(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return 0;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_max_ang(turrets_data_store[turret_id].yaw_motor);
}

int16_t turret_get_min_ang_yaw(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return NULL;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_min_ang(turrets_data_store[turret_id].yaw_motor);
}

int16_t turret_get_max_ang_pitch(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return NULL;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_max_ang(turrets_data_store[turret_id].pitch_motor);
}

int16_t turret_get_min_ang_pitch(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return NULL;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_min_ang(turrets_data_store[turret_id].pitch_motor);
}

float turret_get_yaw_ang_actual(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_ang_actual(turrets_data_store[turret_id].yaw_motor);
}

float turret_get_pitch_ang_actual(turret_id_t turret_id) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	return position_motor_get_ang_actual(turrets_data_store[turret_id].pitch_motor);
}

motor_control_error_t turret_set_yaw_motor_ang(turret_id_t turret_id, float ang) {
	if (!turret_check_id(turret_id)) {
		return TURRET_ID_ERROR;
	} else if (!turrets_data_store[turret_id].turret_inited) {
		return TURRET_INIT_ERROR;
	}
	set_ang_des(turrets_data_store[turret_id].yaw_motor, ang);
	return TURRET_OK;
}

#endif
