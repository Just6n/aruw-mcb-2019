#include "ref_comms.h"
#include "cv_comms.h"
#include "serial.h"
#include "led_error_handler.h"
#include "math_user_utils.h"
#include "cmsis_os.h"
#include "control_mode.h"
#include "agitator_task.h"

#define TIME_BETWEEN_REF_UI_DISPLAY_SEND_MS (100) // time between each referee ui display send in milliseconds 
serial_tx_msg_rate ref_ui_indicator_vals_rate = {0, TIME_BETWEEN_REF_UI_DISPLAY_SEND_MS};

// If the robot has received damage, mark the timestamp of occurrence
// to track received damage per second.
void ref_process_received_damage(int32_t damage_taken);
// If the first tracked damage received occurred more than a second ago,
// stop tracking it and decrease received_dps by that amount.
void update_received_dps(void);

ref_message_t referee;
received_dps_tracker_t received_dps_tracker;

float parse_float(uint8_t* start_byte) {
	uint32_t unsigned_value = (start_byte[3] << 24) | (start_byte[2] << 16) | (start_byte[1] << 8) | start_byte[0];
	return *(float*)&(unsigned_value);
}

static bool parse_game_status(uint8_t* buffer, uint16_t length) {
	if (length != 3) {
		return false;
	}
	referee.game_data.game_stage = (ref_game_stages_t) (buffer[0] >> 4);
	referee.game_data.stage_time_remaining = (buffer[2] << 8) | buffer[1];
	return true;
}

static bool parse_game_result(uint8_t* buffer, uint16_t length) {
	if (length != 1) {
		return false;
	}
	referee.game_data.game_winner = (ref_game_winner_t) buffer[0];
	return true;
}

static bool parse_all_robot_hp(uint8_t* buffer, uint16_t length) {
	if (length != 28) {
		return false;
	}
	referee.robot_data.all_robot_HP.red_hero_HP = (buffer[1] << 8) | buffer[0];
	referee.robot_data.all_robot_HP.red_engineer_HP = (buffer[3] << 8) | buffer[2];
	referee.robot_data.all_robot_HP.red_soldier_1_HP = (buffer[5] << 8) | buffer[4];
	referee.robot_data.all_robot_HP.red_soldier_2_HP = (buffer[7] << 8) | buffer[6];
	referee.robot_data.all_robot_HP.red_soldier_3_HP = (buffer[9] << 8) | buffer[8];
	referee.robot_data.all_robot_HP.red_sentinel_HP = (buffer[11] << 8) | buffer[10];
	referee.robot_data.all_robot_HP.red_base_HP = (buffer[13] << 8) | buffer[12];
	referee.robot_data.all_robot_HP.blue_hero_HP = (buffer[15] << 8) | buffer[14];
	referee.robot_data.all_robot_HP.blue_engineer_HP = (buffer[17] << 8) | buffer[16];
	referee.robot_data.all_robot_HP.blue_soldier_1_HP = (buffer[19] << 8) | buffer[18];
	referee.robot_data.all_robot_HP.blue_soldier_2_HP = (buffer[21] << 8) | buffer[20];
	referee.robot_data.all_robot_HP.blue_soldier_3_HP = (buffer[23] << 8) | buffer[22];
	referee.robot_data.all_robot_HP.blue_sentinel_HP = (buffer[25] << 8) | buffer[24];
	referee.robot_data.all_robot_HP.blue_base_HP = (buffer[27] << 8) | buffer[26];
	return true;
}

static bool parse_robot_status(uint8_t* buffer, uint16_t length) {
	if (length != 15) {
		return false;
	}
	referee.robot_data.robot_id = (ref_robot_id_t) buffer[0];
	referee.robot_data.robot_level = buffer[1];
	referee.robot_data.current_HP = (buffer[3] << 8) | buffer[2];
	referee.robot_data.max_HP = (buffer[5] << 8) | buffer[4];
	referee.robot_data.turret.heat_cooling_rate_17 = (buffer[7] << 8) | buffer[6];
	referee.robot_data.turret.heat_limit_17 = (buffer[9] << 8) | buffer[8];
	referee.robot_data.turret.heat_cooling_rate_42 = (buffer[11] << 8) | buffer[10];
	referee.robot_data.turret.heat_limit_42 = (buffer[13] << 8) | buffer[12];
	referee.robot_data.gimbal_has_power = buffer[14];
	referee.robot_data.chassis_has_power = (buffer[14] >> 1);
	referee.robot_data.shooter_has_power = (buffer[14] >> 2);
	
	if (referee.robot_data.previous_HP > referee.robot_data.current_HP) {
		ref_process_received_damage(referee.robot_data.previous_HP - referee.robot_data.current_HP);
		referee.robot_data.previous_HP = referee.robot_data.current_HP;
	}
	return true;
}

static bool parse_power_and_heat(uint8_t* buffer, uint16_t length) {
	if (length != 14) {
		return false;
	}
	referee.robot_data.chassis.volt = (buffer[1] << 8) | buffer[0];
	referee.robot_data.chassis.current = (buffer[3] << 8) | buffer[2];
	referee.robot_data.chassis.power = parse_float(&buffer[4]);
	referee.robot_data.chassis.power_buffer = (buffer[9] << 8) | buffer[8];
	referee.robot_data.turret.heat_17 = (buffer[11] << 8) | buffer[10];
	referee.robot_data.turret.heat_42 = (buffer[13] << 8) | buffer[12];
	return true;
}

static bool parse_robot_position(uint8_t* buffer, uint16_t length) {
	if (length != 16) {
		return false;
	}
	referee.robot_data.chassis.x = parse_float(&buffer[0]);
	referee.robot_data.chassis.y = parse_float(&buffer[4]);
	referee.robot_data.chassis.z = parse_float(&buffer[8]);
	referee.robot_data.turret.yaw = parse_float(&buffer[12]);
	return true;
}

static bool parse_receive_damage(uint8_t* buffer, uint16_t length) {
	if (length != 1) {
		return false;
	}
	referee.robot_data.damaged_armor_id = (ref_armor_id_t) buffer[0];
	referee.robot_data.damage_type = (ref_damage_type_t) (buffer[0] >> 4);
	referee.robot_data.previous_HP = referee.robot_data.current_HP;
	return true;
}

static bool parse_projectile_launch(uint8_t* buffer, uint16_t length) {
	if (length != 6) {
		return false;
	}
	referee.robot_data.turret.bullet_type = (ref_bullet_type_t) buffer[0];
	referee.robot_data.turret.firing_freq = buffer[1];
	referee.robot_data.turret.bullet_speed = parse_float(&buffer[2]);
	return true;
}

static bool parse_sentinel_drone_bullets_remain(uint8_t* buffer, uint16_t length) {
	if (length != 2) {
		return false;
	}
	referee.robot_data.turret.sentinel_drone_bullets_remain = (buffer[1] << 8) | buffer[0];
	return true;
}

static void ref_comms_serial_rx_handler(uint16_t message_type, uint8_t* buffer, uint16_t length) {
	bool ref_received_data = false;
	referee.online = true;
	switch(message_type) {
		case REF_MESSAGE_TYPE_GAME_STATUS:
		{
			ref_received_data = parse_game_status(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_GAME_RESULT:
		{
			ref_received_data = parse_game_result(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_ALL_ROBOT_HP:
		{
			ref_received_data = parse_all_robot_hp(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_ROBOT_STATUS: 
		{
			ref_received_data = parse_robot_status(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_POWER_AND_HEAT:
		{
			ref_received_data = parse_power_and_heat(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_ROBOT_POSITION:
		{
			ref_received_data = parse_robot_position(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_RECEIVE_DAMAGE:
		{
			ref_received_data = parse_receive_damage(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_PROJECTILE_LAUNCH:
		{
			ref_received_data = parse_projectile_launch(buffer, length);
			break;
		}
		case REF_MESSAGE_TYPE_SENTINEL_DRONE_BULLETS_REMAIN:
		{
			ref_received_data = parse_sentinel_drone_bullets_remain(buffer, length);
		}
		/* handle error messaging */
		default :
			led_report_general_error(ERROR_AREA_REF_COMMS);
			break;
	}
	if (!ref_received_data) {
		led_report_general_error(ERROR_AREA_REF_COMMS);
	}
}

static void ref_comms_init() {
	referee.online = false; // initially sets referee system to be offline
	referee.robot_data.received_dps = 0.0f;
	received_dps_tracker.head = 0;
	received_dps_tracker.tail = 0;
	
	serial_init_listener(SERIAL_HUART6, ref_comms_serial_rx_handler);
	serial_enable_rx_crc_enforcement(SERIAL_HUART6);
}

void update_received_dps() {
	// if current damage at head of circular array occurred more than a second ago,
	// decrease received_dps by that amount of damage and increment head index
	if (HAL_GetTick() - received_dps_tracker.damage_events[received_dps_tracker.head].timestamp_ms > 1000 && received_dps_tracker.head != received_dps_tracker.tail) {
		referee.robot_data.received_dps -= received_dps_tracker.damage_events[received_dps_tracker.head].damage_amount;
		received_dps_tracker.head = (received_dps_tracker.head + 1) % REF_DAMAGE_EVENT_SIZE; // increment head of circular array
	}
}

static void ref_process_received_damage(int32_t damage_taken) {
	if (damage_taken > 0) {
		// create a new received_damage_event with the damage_taken, and current time
		received_damage_event_t damage_token = { damage_taken, HAL_GetTick()};
		// add the recently received damage to the end of the circular array
		received_dps_tracker.damage_events[received_dps_tracker.tail] = damage_token;
		received_dps_tracker.tail = (received_dps_tracker.tail + 1) % REF_DAMAGE_EVENT_SIZE; // increment tail of circular array
		// increment the head of the circular array if the tail has overwritten the original head
		if (received_dps_tracker.tail == received_dps_tracker.head) {
			received_dps_tracker.head = (received_dps_tracker.head + 1) % REF_DAMAGE_EVENT_SIZE;
		}
		referee.robot_data.received_dps += damage_taken;
	}
}

/** 
 * @brief given 6 boolean variables to display to the referee ui, 
 *        packet them into an 8 bit integer and return that value.
 *        The ending bit is the first given boolean, 
 *        the next bit from the end is the second given boolean, and so on.
 * @params bool1, bool2, ..., bool6 the boolean indicator variables to display to the referee client ui.
 * @return the 8 bit variable packeting the 6 boolean indicators
 */
static uint8_t ref_comms_pack_bool_indicators(
	bool bool1, bool bool2, bool bool3, bool bool4, bool bool5, bool bool6)
{
	return ((uint8_t) bool1) |
	       ((uint8_t) bool2) << 1 |
	       ((uint8_t) bool3) << 2 |
	       ((uint8_t) bool4) << 3 |
	       ((uint8_t) bool5) << 4 |
	       ((uint8_t) bool6) << 5; // bits 6 and 7 are reserved by the ref system
}

/** 
 * @brief given robot_id, returns the client_id that the referee system uses to display 
 *        the received messages to the given client_id robot
 * @params robot_id the id of the robot received from the referee system to get the client_id of
 * @return the client_id of the robot requested
 */
static uint16_t ref_comms_get_robot_client_id(ref_robot_id_t robot_id) {
	// there are no client_id for sentinel robots because there are no ui display for them
	if (robot_id == RED_SENTINEL || robot_id == BLUE_SENTINEL) {
		led_report_general_error(ERROR_AREA_REF_COMMS);
		return 0;
	}
	uint16_t retval = 0x100;
	if (robot_id > 10) { // if robot_id is a blue robot
		retval += 6;
	}
	return retval + (uint16_t) robot_id;
}

static void ref_comms_send_ui_display() {
	if (!referee.online) {
		return;
	}
	
	if (!serial_tx_msg_rate_ready(&ref_ui_indicator_vals_rate)) {
		// not enough time has passed before next send
		// send at max every 100 ms (max frequency 10Hz)
		return;
	}
	
	// float vars are temporary for now until there are actual floats that we have decided to display
	// and will replace the place holders in ref_comms_float_to_display# below
	float float1 = (69.0f);
	float float2 = (69.0f);
	extern uint8_t tx_sequence_nums[SERIAL_NUM_HUARTS];
	float float3 = tx_sequence_nums[SERIAL_HUART6];
	// 3 float variables to display on the referee client UI
	// undecided floats are set to 69
	uint32_t ref_comms_float_to_display1 = *(uint32_t*) &(float1);
	uint32_t ref_comms_float_to_display2 = *(uint32_t*) &(float2);
	uint32_t ref_comms_float_to_display3 = *(uint32_t*) &(float3);
	
	// 6 boolean indicators to display on the referee client UI
	// undecided booleans are set as false
	#if defined (TARGET_SOLDIER)
	cv_comms_turret_aim_data_t aim_data;
	bool cv_online = false;
	if (cv_comms_get_last_aim_data(&aim_data) && osKernelSysTick() - aim_data.receive_timestamp < TIME_OFFLINE_CV_AIM_DATA_MS) {
		cv_online = true;
	}
	bool ref_comms_bool_to_display1 = robot_mode == WIGGLE_MODE; // wiggle indicator
	bool ref_comms_bool_to_display2 = cv_online; // CV online indicator
	bool ref_comms_bool_to_display3 = false;
	bool ref_comms_bool_to_display4 = false;
	bool ref_comms_bool_to_display5 = interface_bind_request_open_hopper(); // hopper indicator
	extern agitator_motor_t agitator_motors[NUMBER_OF_AGITATORS];
	// LED is green if the turret is availible to fire, red if it is jammed
	bool ref_comms_bool_to_display6 = !agitator_motors[AGITATOR_17].is_jammed;
	#else
	bool ref_comms_bool_to_display1 = false;
	bool ref_comms_bool_to_display2 = false;
	bool ref_comms_bool_to_display3 = false;
	bool ref_comms_bool_to_display4 = false;
	bool ref_comms_bool_to_display5 = false;
	bool ref_comms_bool_to_display6 = false;
	#endif
	
	uint8_t data[19] = {
		// data content ID / Packet Header
		(uint8_t) REF_UI_INDICATOR_VALS_PACKET_ID,
		(uint8_t) ((uint16_t) REF_UI_INDICATOR_VALS_PACKET_ID >> 8),
		// robot ID of the robot that the message is being sent from
		(uint8_t) referee.robot_data.robot_id,
		(uint8_t) ((uint16_t) (referee.robot_data.robot_id) >> 8),
		// client ID of the robot that the values in the message will be displayed to
		(uint8_t) ref_comms_get_robot_client_id(referee.robot_data.robot_id),
		(uint8_t) (ref_comms_get_robot_client_id(referee.robot_data.robot_id) >> 8),
		// 3 custom floats to display
		(uint8_t) ref_comms_float_to_display1,
		(uint8_t) (ref_comms_float_to_display1 >> 8),
		(uint8_t) (ref_comms_float_to_display1 >> 16),
		(uint8_t) (ref_comms_float_to_display1 >> 24),
		
		(uint8_t) ref_comms_float_to_display2,
		(uint8_t) (ref_comms_float_to_display2 >> 8),
		(uint8_t) (ref_comms_float_to_display2 >> 16),
		(uint8_t) (ref_comms_float_to_display2 >> 24),
		
		(uint8_t) ref_comms_float_to_display3,
		(uint8_t) (ref_comms_float_to_display3 >> 8),
		(uint8_t) (ref_comms_float_to_display3 >> 16),
		(uint8_t) (ref_comms_float_to_display3 >> 24),
		// 6 custom boolean indicators to display in a single 8-bit value
		(uint8_t) ref_comms_pack_bool_indicators(
		  ref_comms_bool_to_display1,
			ref_comms_bool_to_display2,
			ref_comms_bool_to_display3,
			ref_comms_bool_to_display4,
			ref_comms_bool_to_display5,
			ref_comms_bool_to_display6)
	};
	
	serial_transmit(
			SERIAL_HUART6,
			REF_MESSAGE_TYPE_UI_DISPLAY,
			19,
			(uint8_t*)data,
			NULL
	);
}

void ref_comms_task(void const* argu) {
	ref_comms_init();
	while (1) {
		update_received_dps();
		ref_comms_send_ui_display();
		osDelay(1);
	}
}
