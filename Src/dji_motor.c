#include "stm32f4xx_hal.h"
#include "can.h"
#include "dji_motor.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "led_error_handler.h"
#include "control_mode.h"
#include <limits.h>

can_motor_storage_t can1;
can_motor_storage_t can2;

CAN_TxHeaderTypeDef can_dji_motor_low_tx;	   // low are motors 201 - 204
CAN_TxHeaderTypeDef can_dji_motor_high_tx;     // high are motors 205 - 208	
CAN_RxHeaderTypeDef can_rx_handler;

CAN_FilterTypeDef can_filter;

// data buffers
uint8_t data_rx[8] = {0x00};
uint8_t data_dji_motor_low_tx[8] = {0x00};
uint8_t data_dji_motor_high_tx[8] = {0x00};

void can_init_IT(CAN_HandleTypeDef* hcan);
void can_init_motor_store(can_motor_storage_t* storage);

/*@pre: CAN_HandleTypeDef * hcan (&hcan1), CAN_FilterTypeDef* can_filter (can_filter)
post: initializes filter, FIFO, motor drive_store, tx header, rx header		*/

/**
 *  @brief Initializes can 1 and can 2 handlers, filters, and motor stores
 *  @retval none
 */
void can_dji_motor_init(void) {

	can_handle_error(HAL_CAN_Start(&hcan1));
	#if defined(TARGET_ENGINEER) || (TARGET_HERO)
	can_handle_error(HAL_CAN_Start(&hcan2));
	#endif
			
	// set up the can handlers, same for both can 1 and 2
	can_dji_motor_low_tx.StdId = 0x200;
  can_dji_motor_low_tx.IDE = CAN_ID_STD;
  can_dji_motor_low_tx.RTR = CAN_RTR_DATA;
  can_dji_motor_low_tx.DLC = 0x08;
	
	can_dji_motor_high_tx.StdId = 0x1FF;
  can_dji_motor_high_tx.IDE = CAN_ID_STD;
  can_dji_motor_high_tx.RTR = CAN_RTR_DATA;
	can_dji_motor_high_tx.DLC = 0x08;
	
	can_rx_handler.DLC = 0x08;
	can_rx_handler.IDE = CAN_ID_STD;
  can_rx_handler.StdId = 0x200;
  can_rx_handler.RTR = CAN_RTR_DATA;
	
	// set motor id's and in use
	can_init_motor_store(&can1);
	can_init_motor_store(&can2);

	// can filter the same for can 1 and 2
	can_filter.FilterMode           = CAN_FILTERMODE_IDMASK;
  can_filter.FilterScale          = CAN_FILTERSCALE_32BIT;
  can_filter.FilterIdHigh         = 0x0000;
  can_filter.FilterIdLow          = 0x0000;
  can_filter.FilterMaskIdHigh     = 0x0000;
  can_filter.FilterMaskIdLow      = 0x0000;
  can_filter.FilterBank           = 14;
  can_filter.FilterActivation     = ENABLE;
	can_filter.FilterFIFOAssignment = CAN_FilterFIFO0;
		
  HAL_CAN_ConfigFilter(&hcan1, &can_filter);
	can_init_IT(&hcan1);
	
	#if defined(TARGET_ENGINEER) || defined (TARGET_HERO)
	HAL_CAN_ConfigFilter(&hcan2, &can_filter);
	can_init_IT(&hcan2);
	#endif
	
}

/**
 *  @brief Initializes specified dji motor
 *  @param motor the pointer to the specified dji_motor
 *  @param index the motor's location the motor's can array of motors
 *  @retval none
 */
void can_init_motor(dji_motor_t* motor, uint8_t index) {
	motor->motor_id = 0x201 + index;
	motor->in_use = 0;
	motor->encoder_wrapped_prev = ENC_MAX / 2;
	motor->online = false;
}

/**
 *  @brief Initializes a motor store, all 8 motors for a given CAN
 *  @param storage Pointer to the specified storage
 *  @retval none
 */
void can_init_motor_store(can_motor_storage_t* storage){
	for (int i = 0; i < 8; i++) {
		can_init_motor(&(storage->dji_motor_store[i]), i);
	}
}

/**
 *  @brief Initializes specified handler
 *  @param hcan pointer to the the handler that handles CAN for either can1 or can2
 *  @retval none
 */
void can_init_IT(CAN_HandleTypeDef* hcan){
	can_handle_error(HAL_CAN_ActivateNotification(hcan,CAN_IT_RX_FIFO0_MSG_PENDING));
}

/**
 *  @brief Parses and processes input received from motors on the specified CAN line
 *  @param data pointer to array of raw data from CAN line
 *  @param id the motor id
 *  @param can_storage pointer to specified storage, either can1 or can2
 *  @retval none
 */
void can_process_data_dji_motor(uint8_t* data, uint32_t id, can_motor_storage_t* can_storage){
	id = id - MOTOR_1;
	can_storage->dji_motor_store[id].encoder_wrapped = data[0] << 8 | data[1];
	can_storage->dji_motor_store[id].rpm =  data[2] << 8 | data[3];
	can_storage->dji_motor_store[id].current_actual = data[4] << 8 | data[5];

	int16_t enc_dif = can_storage->dji_motor_store[id].encoder_wrapped - can_storage->dji_motor_store[id].encoder_wrapped_prev;
	if(enc_dif < -ENC_MAX / 2){
		can_storage->dji_motor_store[id].rotation_count++;
	} else if(enc_dif > ENC_MAX / 2){
		can_storage->dji_motor_store[id].rotation_count--;
	}
	can_storage->dji_motor_store[id].encoder_unwrapped = can_storage->dji_motor_store[id].encoder_wrapped + 
		can_storage->dji_motor_store[id].rotation_count * ENC_MAX;
	can_storage->dji_motor_store[id].encoder_wrapped_prev = can_storage->dji_motor_store[id].encoder_wrapped;
	can_storage->dji_motor_store[id].online = true;
}

/**
 *  @brief Called in IRQ handler every time Fifo reception is complete. processes data of specified CAN 
 *  @retval none
 */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
	can_handle_error(HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &can_rx_handler, data_rx));
	if(hcan == &hcan1){
		can_process_data_dji_motor(data_rx,can_rx_handler.StdId, &can1);
	} else if(hcan == &hcan2){
		can_process_data_dji_motor(data_rx,can_rx_handler.StdId, &can2);
	}
}

/**
 *  @brief parses voltage desired from int16 to two int8s, which can then be send over can as bytes
 *  @param can_motors the given can motor store
 *  @retval none
 */
void serialize_data_tx(can_motor_storage_t *can_motors){
	for(int i = 0; i < 8; i += 2){
		can_motors->data_dji_motor_low_tx[i] = can_motors->dji_motor_store[i / 2].voltage_desired >> 8;
		can_motors->data_dji_motor_low_tx[i + 1] = can_motors->dji_motor_store[i / 2].voltage_desired & 0xFF;
	}
	for (int i = 0; i < 8; i+= 2) {	// parsing to bytes currrent_output for motors 205 - 208
		can_motors->data_dji_motor_high_tx[i] = can_motors->dji_motor_store[4 + (i / 2)].voltage_desired >> 8;
		can_motors->data_dji_motor_high_tx[i + 1] = can_motors->dji_motor_store[4 + (i / 2)].voltage_desired & 0xFF;
	}	
}

/**
 *  @brief parses and adds messages for all 8 motors to the specified mailbox
 *  @param can_motors the specified can storage
 *  @param hcan the specified can handler
 *  @param mailbox the specified mailbox you would like to add a message to
 *  @retval none
 */
void send_data_tx(can_motor_storage_t* can_motors, CAN_HandleTypeDef* hcan, uint32_t* mailbox){
	if (robot_mode == KILL_MODE) {
		for (int i = 0; i < 8; i++) {
			can1.dji_motor_store[i].voltage_desired = 0;
			can2.dji_motor_store[i].voltage_desired = 0;
		}
	}
	serialize_data_tx(can_motors);
	can_handle_error(HAL_CAN_AddTxMessage(hcan, &can_dji_motor_low_tx, can_motors->data_dji_motor_low_tx, mailbox));	// can1
	can_handle_error(HAL_CAN_AddTxMessage(hcan, &can_dji_motor_high_tx, can_motors->data_dji_motor_high_tx, mailbox));
}

/**
 *  @brief freertos thread for sending data to can 1 and can 2
 *  @retval none
 */
void can_send_dji_motor_task (void const * argument) {
	while(1){				
		if (!HAL_CAN_IsTxMessagePending(&hcan1, CAN_TX_MAILBOX0)) {
			uint32_t mailbox_0 = 0x00000001U;
			send_data_tx(&can1, &hcan1, &mailbox_0);
		}
		#if defined(TARGET_ENGINEER) || (TARGET_HERO)
		if (!HAL_CAN_IsTxMessagePending(&hcan2, CAN_TX_MAILBOX1)) {
			uint32_t mailbox_1 = 0x00000002U;
			send_data_tx(&can2, &hcan2, &mailbox_1);
		}
		#endif
		osDelay(3);
	}
}

/**
 *  @brief Sets the given motor to the given output. limits value between int16
 *  @param motor the specified dji_motor
 *  @param output the voltage for the given motor
 *  @retval none
 */
void can_dji_motor_set(dji_motor_t* motor, float output){
	if (output >= SHRT_MAX) {
		output = SHRT_MAX - 1;
	} else if (output <= SHRT_MIN) {
		output = SHRT_MIN + 1;
	}
		
	motor->voltage_desired = (int16_t) output;
}

/**
 *  @brief returns pointer to specified motor
 *  @param can the specified can storage
 *  @param motor_number the spefieid motor (201 - 208)
 *  @retval pointer to dji_motor on the specified can and with the specified motor number
 */
dji_motor_t* get_motor(can_motor_storage_t* can, int motor_number){
	motor_number = motor_number - MOTOR_1;
	if(motor_number >= 0 && motor_number <= 7){
		if(can->dji_motor_store[motor_number].in_use == 0){
			can->dji_motor_store[motor_number].in_use = 1;
			return &(can->dji_motor_store[motor_number]);
		} else {
			can_handle_error(HAL_ERROR);
		}
	} else {
		can_handle_error(HAL_ERROR);
	}
	return NULL;
}

