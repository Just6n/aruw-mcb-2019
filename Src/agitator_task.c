
#if defined(TARGET_SOLDIER) || defined (TARGET_DRONE) || defined (TARGET_SENTINEL)

#include "agitator_task.h"
#include <stdio.h>
#include <stdlib.h>

#define AGITATOR_ROTATE_COMPLETE (10.0f) // when the agitator has reached within this value of 
#if defined (TARGET_DRONE) || defined (TARGET_SENTINEL)
#define COMPLETE_FIRE_ROTATE (-40.0f) // the set angle value, the agitator has completed firing
#define COMPLETE_UNJAM_ROTATE (20.0f)
#elif defined (TARGET_SOLDIER) || defined (TARGET_HERO)
#define COMPLETE_FIRE_ROTATE (60.0f)
#define COMPLETE_UNJAM_ROTATE (70.0f)
#endif
#define MAX_UNJAM_WAIT_TIME (400.0f)
#define MAX_UNJAM_TIME (7000) // unjamming sequence only runs for this amount of time in milliseconds

agitator_motor_t agitator_motors[NUMBER_OF_AGITATORS]; 

static bool agitator_check_id(agitator_motor_id_t id);

uint32_t unjam_wait_start_time =0;

/**
 * @brief  Sets specified agitator count to the specified fire_ount
 * @param  id the agitator id that you would like to reference
 * @param  fire_count the number of rotations you would like the agitator to rotate
 *				 where 1 rotation is one AGITATOR_ROTATE_COMPLETE
 * @retval motor_control_error_t if initialization fails, returns an error
 */
motor_control_error_t agitator_request_fire(agitator_motor_id_t id, int8_t fire_count) {
	if (!agitator_check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	}
	agitator_motors[id].fire_count = fire_count;
	return POSITION_MOTOR_OK;
}

float rand_unjam_angle = 0.0f;

/**
 * @brief  Rotates the given agitator motor a given number of increments (fire_count), which is changed
 * 				 in agitator_request_fire
 * @param  id the agitator id you would like to reference
 * @retval motor_control_error_t if initialization fails, returns an error
 */
motor_control_error_t agitator_handler(agitator_motor_id_t id){
	if (!agitator_check_id(id)) {
		return POSITION_MOTOR_ID_ERROR;
	}
	
	switch (agitator_motors[id].agitator_fire_sequence) {
		case WAITING: 
			if (agitator_motors[id].fire_count != 0) {
				agitator_motors[id].agitator_fire_sequence = RUNNING;
				set_ang_des(agitator_motors[id].motor_id, position_motor_get_curr_ang_setpoint(agitator_motors[id].motor_id) + COMPLETE_FIRE_ROTATE);
				agitator_motors[id].prev_set_time = osKernelSysTick();
			}
			break;
		case RUNNING: 
			if (osKernelSysTick() - agitator_motors[id].prev_set_time >= agitator_motors[id].max_rotation_time
				  && (!agitator_motors[id].is_jammed 
			        || (agitator_motors[id].is_jammed && osKernelSysTick() - agitator_motors[id].jam_timestamp < MAX_UNJAM_TIME)))
			{
				if (!agitator_motors[id].is_jammed) {
					agitator_motors[id].is_jammed = true;
					agitator_motors[id].jam_timestamp = osKernelSysTick();
				}
				agitator_motors[id].agitator_fire_sequence = UNJAM;
				unjam_wait_start_time = osKernelSysTick();
				agitator_motors[id].rand_unjam_angle = 
					limit_float((float) (rand() % (int) COMPLETE_UNJAM_ROTATE), 0.0f, COMPLETE_UNJAM_ROTATE);
				agitator_motors[id].rand_unjam_wait_time = limit_float((float) (rand() % (int) MAX_UNJAM_WAIT_TIME), 
					0.0f, MAX_UNJAM_WAIT_TIME);
				set_ang_des(agitator_motors[id].motor_id,	position_motor_get_curr_ang_setpoint(agitator_motors[id].motor_id) - COMPLETE_FIRE_ROTATE - agitator_motors[id].rand_unjam_angle);
			} else if(fabs(position_motor_get_curr_ang_setpoint(agitator_motors[id].motor_id) - position_motor_get_ang_actual(agitator_motors[id].motor_id)) <= AGITATOR_ROTATE_COMPLETE) {
				agitator_motors[id].is_jammed = false;
				agitator_motors[id].agitator_fire_sequence = WAITING;
				agitator_motors[id].fire_count--;
				agitator_motors[id].prev_set_time = osKernelSysTick();
			}
			break;
		case UNJAM:
			if (
				osKernelSysTick() - unjam_wait_start_time >= agitator_motors[id].rand_unjam_wait_time
			) {
				unjam_wait_start_time = osKernelSysTick();
				set_ang_des(
					agitator_motors[id].motor_id,
					position_motor_get_curr_ang_setpoint(agitator_motors[id].motor_id) + agitator_motors[id].rand_unjam_angle
				);
				agitator_motors[id].agitator_fire_sequence = FORWARD_UNJAM;
			}
			break;
		case FORWARD_UNJAM:
		{
			if (
				osKernelSysTick() - unjam_wait_start_time >= agitator_motors[id].rand_unjam_wait_time
			) {
				set_ang_des(
					agitator_motors[id].motor_id,
					position_motor_get_curr_ang_setpoint(agitator_motors[id].motor_id) + COMPLETE_FIRE_ROTATE
				);
				agitator_motors[id].agitator_fire_sequence = RUNNING;
				agitator_motors[id].prev_set_time = osKernelSysTick();
			}
			break;
		}
	}
	return POSITION_MOTOR_OK;
}

/**
 * @brief  initializes a given agitator motor with the given parameters
 * @param  agitator_id the specified agitator
 * @param  control_motor_id the control motor that the agitator is associated with
 * @param  motor the dji motor the control motor is associated with
 * @param  kp, ki, kd pid values for the specified dji motor
 * @param  gear_ratio the position motor's gear ratio
 * @param  max_rotation_time the maximum time allowed for an agitator to rotate before unjamming occurs
 * @retval motor_control_error_t if initialization fails, returns an error
 */
motor_control_error_t agitator_motor_init(
	agitator_motor_id_t agitator_id, 
	position_control_motor_id_t control_motor_id, 
	dji_motor_t* motor, 
	float kp, 
	float ki, 
	float kd, 
	float gear_ratio, 
	uint32_t max_rotation_time
) {
	
	if (!agitator_check_id(agitator_id)) {
		return POSITION_MOTOR_ID_ERROR;
	}
	motor_control_error_t err = POSITION_MOTOR_OK;
	agitator_motors[agitator_id].motor_id = control_motor_id;
	agitator_motors[agitator_id].fire_count = 0;
	agitator_motors[agitator_id].max_rotation_time = max_rotation_time;	
	err = position_motor_init(control_motor_id, motor);
	if (err != POSITION_MOTOR_OK) {
		return err;
	}
	err = position_motor_init_pid(control_motor_id, POSITION_PID, kp, ki, kd);
	if (err != POSITION_MOTOR_OK) {
		return err;
	}
	err = position_motor_init_gear_ratio(control_motor_id, gear_ratio);
	return err;
}

/** 
 * @brief returns true if the agitator is in the process of rotating or unjamming, 
 *  			returns false otherwise
 * @param id the agitator id
 * @retval whether or not agitator is rotating
 */
bool is_agitator_firing(agitator_motor_id_t id) {
	if (!agitator_check_id(id)) {
		return false;
	}
	return agitator_motors[id].agitator_fire_sequence != WAITING;
}


/** 
 * @brief returns true if the agitator id is valid, returns false otherwise
 * @param id the agitator id
 * @retval whether or not the given agitator id is valid
 */
static bool agitator_check_id(agitator_motor_id_t id) {
	return id < NUMBER_OF_AGITATORS;
}

#endif
