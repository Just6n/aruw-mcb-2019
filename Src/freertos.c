/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "dji_motor.h"
#include "drive_task.h"
#include "sentinel_drive_task.h"
#include "engineer_specific_task.h"
#include "pwm.h"
#include "gpio.h"
#include "led_error_handler.h"
#include "imu_task.h"
#include "cv_comms.h"
#include "ref_comms.h"
#include "control_mode.h"
#include "firing_task_17.h"
#include "turret_task_17.h"
#include "turret_task_42.h"
#include "remote.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
osThreadId led_timer_id;
osThreadId drive_canTX_id;
osThreadId turret_17_id;
osThreadId turret_42_id;
osThreadId drive_id;
osThreadId sentinel_drive_id;
osThreadId engineer_specific_id;
osThreadId cv_comms_id;
osThreadId ref_comms_id;
osThreadId control_mode_id;
osThreadId imu_task_t;
osThreadId position_control_motor_id;
osThreadId agitator_17_id;
osThreadId remote_calculations_id;

//osTimerId can_timer_id;

/* USER CODE END Variables */
osThreadId defaultTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
    
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */

	
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
		
  osThreadDef(imuTask, imu_task, osPriorityNormal, 0, 128);
  imu_task_t = osThreadCreate(osThread(imuTask), NULL);
    
	osThreadDef(remote_calculations_id, remote_calc_task, osPriorityNormal, 0, 128);
	remote_calculations_id = osThreadCreate(osThread(remote_calculations_id), NULL);

  osThreadDef(dji_motor_can1_tx, can_send_dji_motor_task, osPriorityNormal, 0, 128);
  drive_canTX_id = osThreadCreate(osThread(dji_motor_can1_tx), NULL);
	
	osThreadDef(cv_comms_id, cv_comms_task, osPriorityNormal, 0, 128);
	cv_comms_id = osThreadCreate(osThread(cv_comms_id), NULL);
	
	osThreadDef(ref_comms_id, ref_comms_task, osPriorityNormal, 0, 128);
	ref_comms_id = osThreadCreate(osThread(ref_comms_id), NULL);
	
	osThreadDef(control_mode_id, control_mode_task, osPriorityNormal, 0, 128);
	control_mode_id = osThreadCreate(osThread(control_mode_id), NULL);
  
  // robot specific threads
	#if defined(TARGET_ENGINEER)
	osThreadDef(engineer_specific_id, engineer_specific_task, osPriorityNormal, 0, 128);
	engineer_specific_id = osThreadCreate(osThread(engineer_specific_id), NULL);
	#elif defined (TARGET_SOLDIER) || (TARGET_HERO)
	osThreadDef(drive_id, drive_task, osPriorityNormal, 0, 128);
	drive_id = osThreadCreate(osThread(drive_id), NULL);
	#elif defined (TARGET_SENTINEL)
	osThreadDef(sentinel_drive_id, sentinel_drive_task, osPriorityNormal, 0, 128);
	sentinel_drive_id = osThreadCreate(osThread(sentinel_drive_id), NULL);
	#endif
	#if defined (TARGET_SOLDIER) || (TARGET_DRONE) || (TARGET_SENTINEL)
  osThreadDef(turret_17_id, turret_task_17, osPriorityNormal, 0, 128);
	turret_17_id = osThreadCreate(osThread(turret_17_id), NULL);
	osThreadDef(agitator_17_id, firing_task_17, osPriorityNormal, 0, 128);
	agitator_17_id = osThreadCreate(osThread(agitator_17_id), NULL);
	#endif 
	#if defined(TARGET_HERO)
	osThreadDef(turret_42_id, turret_task_42, osPriorityNormal, 0, 128);
	turret_42_id = osThreadCreate(osThread(turret_42_id), NULL);
	#endif
	
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
	
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
		osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
