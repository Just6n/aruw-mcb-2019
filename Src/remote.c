/*
 * remote.c
 *
 * This file provides functions and variables related to the remote control
 * input that the user uses to drive the robot.
 */

#include "remote.h"
#include "stdio.h"
#include "stdlib.h"
#include "usart.h"
#include "stm32f4xx_hal.h"
#include "drive_task.h"
#include "operator_interface_bindings.h"

#define DR16_PACKET_LEN 18
#define DR16_MAX_LEN    50

#define MOUSE_X_ALPHA 1.0f
#define MOUSE_Y_ALPHA 1.0f

uint16_t num_data_to_read;
uint8_t remote_dr16_rx_buf[DR16_MAX_LEN];
remote_info_t remote_dr16;

toggle_key_t toggle_keys[16];
toggle_key_t dummy_key;

toggle_key_t get_toggle(int16_t key){
	for(int i = 0; i < 16; i++){
		if(key & toggle_keys[i].key && toggle_keys[i].key_enabled){
			return toggle_keys[i];
		}
	}
	return dummy_key;
}

bool key_toggled(uint16_t key){
  return get_toggle(key).key_enabled && get_toggle(key).key_toggled;
}

void zero_all_toggle_keys(void){
	for(int i = 0; i < 16; i++){
		toggle_keys[i].key_enabled = false;
		toggle_keys[i].key = ((uint16_t)0x01 << i);
		toggle_keys[i].key_state = NOT_PRESSED;
	}
	dummy_key.key_enabled = false;
	dummy_key.key = 0x00;
	dummy_key.key_state = NOT_PRESSED;
}

void init_toggle_key(int16_t key){
	for(int i = 0; i < 16; i++){
		if(!(toggle_keys[i].key ^ key)){
			toggle_keys[i].key_enabled = true;
		}
	}
}

void init_desired_keys(void){
	zero_all_toggle_keys();
	#if defined(TARGET_SOLDIER)
	init_toggle_key(KEY_WIGGLE);
	init_toggle_key(KEY_HOPPER);
	init_toggle_key(KEY_LOW_CHASSIS_SPEED);
	#elif defined(TARGET_ENGINEER)
	init_toggle_key(KEY_DUMP);
	init_toggle_key(KEY_TOW);
  init_toggle_key(KEY_GRAB);
  init_toggle_key(KEY_GRAB_TGGL);
	init_toggle_key(KEY_LIFT_TGGL);
  #endif
}
	
void key_toggle_handler(toggle_key_t* key){	
	switch(key->key_state){
		case NOT_PRESSED:
			key->key_toggled = false;
			key->key_state = KEY_PRESSED(remote_dr16.computer.key,key->key) ? PRESSED : key->key_state;		
			break;
		case PRESSED:
			key->key_toggled = true;
			key->key_state = !KEY_PRESSED(remote_dr16.computer.key, key->key) ? RELEASED : key->key_state;
			break;
		case RELEASED: 
			key->key_state = KEY_PRESSED(remote_dr16.computer.key, key->key) ? PRESSED_TO_UNTOGGLE : key->key_state;
			break;
		case PRESSED_TO_UNTOGGLE:
			key->key_state = !KEY_PRESSED(remote_dr16.computer.key, key->key) ? NOT_PRESSED : key->key_state;
	}
}

void toggle_handler(void){
	for(int i = 0; i < 16; i++){
		if(toggle_keys[i].key_enabled){
			key_toggle_handler(&toggle_keys[i]);
		}
	}
}

static void dr16_process_buf(uint8_t start) {
  remote_parse_buf(remote_dr16_rx_buf + start, &remote_dr16);
	toggle_handler();
  remote_dr16.update_counter++;
}

// The parsing code from DJI
int remote_parse_buf(uint8_t  *buf, remote_info_t *RC) {
  RC->rc.ch0 = (buf[0] | buf[1] << 8) & 0x07FF;
  RC->rc.ch0 -= 1024;
  RC->rc.ch1 = (buf[1] >> 3 | buf[2] << 5) & 0x07FF;
  RC->rc.ch1 -= 1024;
  RC->rc.ch2 = (buf[2] >> 6 | buf[3] << 2 | buf[4] << 10) & 0x07FF;
  RC->rc.ch2 -= 1024;
  RC->rc.ch3 = (buf[4] >> 1 | buf[5] << 7) & 0x07FF;
  RC->rc.ch3 -= 1024;

  RC->rc.s1 = ((buf[5] >> 4) & 0x000C) >> 2;
  RC->rc.s2 = (buf[5] >> 4) & 0x0003;

  if ((abs(RC->rc.ch0) > 660) || \
      (abs(RC->rc.ch1) > 660) || \
      (abs(RC->rc.ch2) > 660) || \
      (abs(RC->rc.ch3) > 660))
  {
    //memset(RC, 0, sizeof(remote_info_t));
    return 0;
  }

  RC->computer.mouse.x_velocity = (int16_t)buf[6] | ((int16_t)buf[7] << 8); // x axis
  RC->computer.mouse.y_velocity = (int16_t)buf[8] | ((int16_t)buf[9] << 8);
  RC->computer.mouse.z = buf[10] | (buf[11] << 8);
		
  RC->computer.mouse.press_l = buf[12];
  RC->computer.mouse.press_r = buf[13];

  RC->computer.key = buf[14] | buf[15] << 8; // keyboard code
  return 1;
}

void remote_uart_it_init(void) {
  __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
}

void remote_init(UART_HandleTypeDef *huart) {
	
	init_desired_keys();
	
  if (huart->RxState != HAL_UART_STATE_READY) {
    // Something is wrong, so hang
    while (1) {
			osDelay(1);
    }
  }

  // Enable IDLE interrupt
  __HAL_UART_ENABLE_IT(huart, UART_IT_IDLE);

  // Configure huart to point to dr16_rx_buf and have correct size
  huart->pRxBuffPtr = remote_dr16_rx_buf;
  huart->RxXferSize = DR16_MAX_LEN;

  huart->ErrorCode  = HAL_UART_ERROR_NONE;

  // Enable DMA to read in MAX_LEN bytes
  HAL_DMA_Start(huart->hdmarx, (uint32_t)&huart->Instance->DR,
                  (uint32_t) remote_dr16_rx_buf, DR16_MAX_LEN);
  
  SET_BIT(huart->Instance->CR3, USART_CR3_DMAR);
}

void dr16_idle_handler(UART_HandleTypeDef *huart) {
  // Clear Idle flag
  __HAL_UART_CLEAR_IDLEFLAG(huart);
  // Check which huart this is
  if (huart == &huart1) {
    // Disable DMA stream temporarily
    __HAL_DMA_DISABLE(huart->hdmarx);
    // Check how many bytes left to be read by the DMA stream
    num_data_to_read = __HAL_DMA_GET_COUNTER(huart->hdmarx);
    if ((DR16_MAX_LEN - DR16_PACKET_LEN) == num_data_to_read) {
      // Read exactly DR16_PACKET_LEN bytes
      dr16_process_buf(0);
    }
    // Reset DMA stream count
    __HAL_DMA_SET_COUNTER(huart->hdmarx, DR16_MAX_LEN);
    // Enable DMA stream
    __HAL_DMA_ENABLE(huart->hdmarx);
  }
}

const remote_info_t *dr16_get_remote() {
 return &remote_dr16;
}

void remote_calc_task(void const* argu) {
	while (1) {
		remote_dr16.computer.mouse.average_x_mouse = low_pass_filter(remote_dr16.computer.mouse.average_x_mouse, 
			(float) remote_dr16.computer.mouse.x_velocity, MOUSE_X_ALPHA);
		remote_dr16.computer.mouse.average_y_mouse = low_pass_filter(remote_dr16.computer.mouse.average_y_mouse, 
			(float) remote_dr16.computer.mouse.y_velocity, MOUSE_Y_ALPHA);
		osDelay(1);
	}
}
