#include "gpio.h"
#include "cmsis_os.h"
#include "pwm.h"
#include "gpio.h"
#include "tim.h"
#include "control_mode.h"
#include "hardware_contract.h"

#define PWM_RESOLUTION 1024

// Takes in the address of the timer, the channel of the timer, and a duty cycle, sets the certain channel to that duty cycle
void PWM_SetTimerChannel(TIM_HandleTypeDef *tim,uint32_t tim_channel,float duty){
	switch(tim_channel){	
		case TIM_CHANNEL_1: tim->Instance->CCR1 = (PWM_RESOLUTION*duty) - 1;break; // Formula from RoboMasters example
		case TIM_CHANNEL_2: tim->Instance->CCR2 = (PWM_RESOLUTION*duty) - 1;break;
		case TIM_CHANNEL_3: tim->Instance->CCR3 = (PWM_RESOLUTION*duty) - 1;break;
		case TIM_CHANNEL_4: tim->Instance->CCR4 = (PWM_RESOLUTION*duty) - 1;break;
	}
}

/* Sets PWM duty cycle for a pin
 * Pre: pins are between PIN_A - PIN_H, PIN_S - PIN_Z, 0.0f <= duty <= 1.0f
 * Duty Cycle is in float form i.e 0.50f = 50% duty cycle
 * Assigns Pins to a timer and a channel, then calls PWM_SetTimerChannel with the timer, channel and given duty
 */
void PWM_SetDuty(uint16_t pin, float duty) {
	TIM_HandleTypeDef *tim = 0x0;
	uint32_t tim_channel = 0x0;
	switch(pin) {
		case PIN_A:
			tim_channel = TIM_CHANNEL_4; break;
		case PIN_B:
			tim_channel = TIM_CHANNEL_3; break;
	  case PIN_C:
			tim_channel = TIM_CHANNEL_2; break;
		case PIN_D:
			tim_channel = TIM_CHANNEL_1; break;
		case PIN_E:
			tim_channel = TIM_CHANNEL_4; break;
		case PIN_F:
			tim_channel = TIM_CHANNEL_3; break;
	  case PIN_G:
			tim_channel = TIM_CHANNEL_2; break;
		case PIN_H:
			tim_channel = TIM_CHANNEL_1; break;
		case PIN_S:
			tim_channel = TIM_CHANNEL_1; break;
		case PIN_T:
			tim_channel = TIM_CHANNEL_2; break;
	  case PIN_U:
			tim_channel = TIM_CHANNEL_3; break;
		case PIN_V:
			tim_channel = TIM_CHANNEL_4; break;
		case PIN_W:
			tim_channel = TIM_CHANNEL_1; break;
		case PIN_X:
			tim_channel = TIM_CHANNEL_2; break;
	  case PIN_Y:
			tim_channel = TIM_CHANNEL_3; break;
		case PIN_Z:
			tim_channel = TIM_CHANNEL_4; break;
	}
	switch(pin) {
		case PIN_A:
		case PIN_B:
	  case PIN_C:
		case PIN_D:
			tim = &htim5; break;
		case PIN_E:
		case PIN_F:
	  case PIN_G:
		case PIN_H:
			tim = &htim4; break;
		case PIN_S:
		case PIN_T:
	  case PIN_U:
		case PIN_V:
			tim = &htim2; break;
		case PIN_W:
		case PIN_X:
	  case PIN_Y:
		case PIN_Z:
			tim = &htim8; break;
	}
	if (tim != 0x0) { // Do nothing if timer wasn't set due to unknown pin
		if (robot_mode == KILL_MODE) {
			// sending 0 duty deinitializes friction wheels, 0.1 stops friction wheels
			if (pin == FRICTION_WHEEL_RIGHT_GPIO || pin == FRICTION_WHEEL_LEFT_GPIO) {
				duty = 0.1;
			} else {
				duty = 0;
			}
		}
		PWM_SetTimerChannel(tim, tim_channel, duty);
	}
}

