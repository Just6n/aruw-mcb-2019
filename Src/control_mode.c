#include "control_mode.h"

robot_mode_t robot_mode;
robot_aim_mode_t robot_aim_mode;

bool robot_init[NUM_INITS] = { false };

void control_mode_init(void){
	robot_mode = BASE_CTRL_MODE;
	robot_aim_mode = AIM_MANUAL;
}


/* 
 * For updating soldier_mode, based on either remote values or computer values 
 */
void control_mode_task(void const* argu){
	while(robot_mode == INIT_MODE) {
		osDelay(1);
		if (!REMOTE_MATCHES_INIT_CONFIGURATION()) {
			continue;
		}
		bool all_init = true;
		for (int i = 0; i < NUM_INITS; i++) {
			if (!robot_init[i]) {
				all_init = false;
				break;
			}
		}
		if (all_init) {
			robot_mode = BASE_CTRL_MODE;
		}
	}


	while(1){
		#if defined(TARGET_SOLDIER)
		if(interface_bind_request_wiggle()){
			robot_mode = WIGGLE_MODE;
		} else if(interface_bind_request_turret_lock()){
			robot_mode = LOCK_TURRET_MODE;
		} else if(interface_bind_request_kill()){
			robot_mode = KILL_MODE;
		} else {
			robot_mode = BASE_CTRL_MODE;
		}
		if (interface_bind_request_auto_aim()) {
			robot_aim_mode = AIM_CV;
		} else {
			robot_aim_mode = AIM_MANUAL;
		}
		#elif defined(TARGET_ENGINEER)
		if(interface_bind_request_kill()){
			robot_mode = KILL_MODE;
		} else {
			robot_mode = BASE_CTRL_MODE;
		}
		robot_aim_mode = AIM_MANUAL;
		#elif defined (TARGET_SENTINEL)
		if (interface_bind_request_kill()) {
			robot_mode = KILL_MODE;
		} else if (interface_bind_request_sentinel_manual_mode()) {
			robot_mode = BASE_CTRL_MODE;
			robot_aim_mode = AIM_MANUAL;
		} else {
			robot_mode = AUTO_MODE;
			robot_aim_mode = AIM_CV;
		}
		#else
		robot_mode = BASE_CTRL_MODE;
		robot_aim_mode = AIM_MANUAL;
		#endif	
		osDelay(1);	
	}
}

void waiting_for_init(void) {
	while (robot_mode == INIT_MODE) {
		osDelay(1);
	}
}
