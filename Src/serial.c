/*
 * serial.c
 *
 * This file contains interfacing with USART interrupts.
 * 
 * READ for initiating a new usart (currently only interfacing with huart2)
 *  	for tx: 
 * 		- create a new serial_t struct and put under "SERIAL_T STCUTS HERE WHEN NEEDED"
 * 		- create a new extern serial_t struct in main under "EXTERN UART SERIAL STRUCTS"
 * 		- call the serial_init function in main after all uart initiation. This
 *			will start running the state machine that will be continually running.
 *		- in HAL_UART_RxCpltCallback, specify which usart you are initializing 
 * 		- put functions that you would like to call in the init method in an
 * 		  existing array or a new array
 */

#include "stm32f4xx_hal.h"
#include "usart.h"
#include <stdint.h>
#include <string.h>
#include "serial.h"
#include "led_error_handler.h"
#include "cmsis_os.h"

/*
** Descriptions: Verifies received CRC8 and CRC16 values
** Input: serial the serial struct that is used to receive messages
** Output: true if received CRC8 and CRC16 values were correct
*/
bool verify_CRC_vals(serial_state_t* serial);
uint8_t get_CRC8_check_sum(uint8_t *message, uint32_t message_length, uint8_t CRC8);
uint16_t get_CRC16_check_sum(uint8_t *message, uint32_t message_length, uint16_t CRC16);
serial_state_t known_serial_states[SERIAL_NUM_HUARTS] = { 0 };
uint8_t tx_sequence_nums[SERIAL_NUM_HUARTS];

const uint8_t CRC8_table[256] =
{
	0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83, 0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
	0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e, 0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
	0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0, 0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
	0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d, 0x7c,	0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
	0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5, 0x84, 0xda, 0x38, 0x66,	0xe5, 0xbb, 0x59, 0x07,
	0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58, 0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a, 
	0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6, 0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
	0xf8, 0xa6,	0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b, 0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
	0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f, 0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
	0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,	0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
	0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c, 0x6d, 0x33, 0xd1,	0x8f, 0x0c, 0x52, 0xb0, 0xee,
	0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1, 0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
	0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49, 0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
	0x57,	0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4, 0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
	0xe9, 0xb7, 0x55, 0x0b,	0x88, 0xd6, 0x34, 0x6a, 0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
	0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9,	0xf7, 0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35,
};

const uint16_t CRC16_table[256] =
{
	0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
	0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
	0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
	0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
	0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
	0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
	0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
	0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
	0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
	0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
	0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
	0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
	0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
	0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
	0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
	0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
	0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
	0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
	0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
	0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
	0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
	0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
	0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
	0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
	0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
	0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
	0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
	0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
	0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
	0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
	0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
	0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

void initiate_uart_receive(serial_state_t* serial, uint16_t size) {
	HAL_StatusTypeDef err = HAL_UART_Receive_IT(serial->huart, serial->buf_rx, size);
	if (err != HAL_OK) {
		led_report_hal_error (err, ERROR_AREA_SERIAL);
	}
}

void serial_transition_to_mode(serial_state_t* serial, serial_mode_t new_mode) {
	switch (new_mode) {
		case WAITING_FOR_HEAD_BYTE:
			serial->current_mode = WAITING_FOR_HEAD_BYTE;
			// search through first byte for head byte
			initiate_uart_receive(serial, 1);
			break;

		case WAITING_FOR_MESSAGE_LENGTH:
			serial->current_mode = WAITING_FOR_MESSAGE_LENGTH;
			// zero out the first 16 bits of the rx buffer so we can validate we got data
			serial->buf_rx[0] = 0;
			serial->buf_rx[1] = 0;
			// get next 2 bytes for the message length
			initiate_uart_receive(serial, 2); 
			break;

		case WAITING_FOR_MESSAGE_DATA:
			serial->current_mode = WAITING_FOR_MESSAGE_DATA;
			// get the rest of the packet (1-byte sequence, 1-byte CRC8, 2-byte message type, message data, 2-byte CRC16)
			initiate_uart_receive(serial, serial->expected_message_length + 6);
			break;
		
		default:
			led_report_general_error(ERROR_AREA_SERIAL);
			break;
	}
}

void process_receive(serial_state_t* serial) {
	switch(serial->current_mode) {
		case WAITING_FOR_HEAD_BYTE:
			if (serial->buf_rx[0] != SERIAL_HEAD_BYTE) {
				// if this byte isn't the header byte, just keep scanning
				serial_transition_to_mode(serial, WAITING_FOR_HEAD_BYTE);
				break;
			}
			serial_transition_to_mode(serial, WAITING_FOR_MESSAGE_LENGTH);
			break;
			
		case WAITING_FOR_MESSAGE_LENGTH:
			serial->expected_message_length = (serial->buf_rx[1] << 8) | serial->buf_rx[0];
			// the message length must be greater than 0 and less than the max size of the buffer	 - 9 (9 bytes are used for the packet header and CRC)
			if (serial->expected_message_length > 0 && serial->expected_message_length <= SERIAL_RX_BUF_SIZE - 9) {
				serial_transition_to_mode(serial, WAITING_FOR_MESSAGE_DATA);
			} else {
				serial->expected_message_length = 0;
				led_report_general_error(ERROR_AREA_SERIAL);
				// if the length isn't valid, throw out the message and go back to scanning for messages
				serial_transition_to_mode(serial, WAITING_FOR_HEAD_BYTE);
			}
			break;
			
		case WAITING_FOR_MESSAGE_DATA:
			serial->sequence_num =  serial->buf_rx[0];
			serial->CRC8 = serial->buf_rx[1];
			serial->message_type = (serial->buf_rx[3] << 8) | serial->buf_rx[2];
			serial->CRC16 = (serial->buf_rx[5 + serial->expected_message_length] << 8) | serial->buf_rx[4 + serial->expected_message_length];
			
			// set CRC buffer to get CRC8 and CRC16 values
			serial->buf_CRC[0] = SERIAL_HEAD_BYTE;
			*((uint16_t*) (serial->buf_CRC + 1)) = serial->expected_message_length;
			memcpy(serial->buf_CRC + 3, serial->buf_rx, 6 + serial->expected_message_length);
		  
			// if CRC checking is not enabled, process the message, otherwise check if CRC8/CRC16 are valid
			if (!serial->rx_crc_enforcement_enabled) {
				// CRC checking is disabled for this huart port
				serial->handler(serial->message_type, (serial->buf_rx + 4), serial->expected_message_length);
			} else if (verify_CRC_vals(serial)) {
				// CRC checking is enabled and CRC8/CRC16 were valid
				serial->handler(serial->message_type, (serial->buf_rx + 4), serial->expected_message_length);
			} else {
				// CRC checking is enabled but CRC8/CRC16 were invalid
				serial->expected_message_length = 0;
				led_report_general_error(ERROR_AREA_SERIAL);
			}
			
			serial_transition_to_mode(serial, WAITING_FOR_HEAD_BYTE);
			break;
			
		default:
			led_report_general_error(ERROR_AREA_SERIAL);
			break;
	}
}

UART_HandleTypeDef* serial_get_huart_handle(serial_huart_port_num_t huart_port) {
	switch (huart_port) {
		case SERIAL_HUART1:
			return &huart1;
		case SERIAL_HUART2:
			return &huart2;
		case SERIAL_HUART6:
			return &huart6;
		default:
			return NULL;
	}
}

void serial_init_listener(serial_huart_port_num_t huart_port, serial_message_handler_t message_handler) {
	if (huart_port < 1 || huart_port >= SERIAL_NUM_HUARTS) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return;
	}

	serial_state_t* serial = &(known_serial_states[huart_port]);

	if (serial->handler != NULL) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return;
	}

	UART_HandleTypeDef* target_huart = serial_get_huart_handle(huart_port);

	if (target_huart == NULL) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return;
	}

	serial->expected_message_length = 0;
	serial->handler = message_handler;
	serial->huart = target_huart;
	serial->message_type = 0;
	serial->sequence_num = 0;
	serial->CRC8 = 0;
	serial->CRC16 = 0;
	serial->rx_crc_enforcement_enabled = false;
	serial_transition_to_mode(serial, WAITING_FOR_HEAD_BYTE);

	return;
}

void serial_enable_rx_crc_enforcement(serial_huart_port_num_t huart_port) {
	if (huart_port < 1 || huart_port >= SERIAL_NUM_HUARTS) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return;
	}
	serial_state_t* serial = &(known_serial_states[huart_port]);	
	serial->rx_crc_enforcement_enabled = true;
}

/*
 * Overridden function: every time HAL_Uart_Receive_IT() is complete, this function is triggered
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	for (int i = 0; i < SERIAL_NUM_HUARTS; i++) {
		if (known_serial_states[i].huart == huart) {
			process_receive(&(known_serial_states[i]));
			return;
		}
	}

	// If we get here, we didn't find the appropriate serial state
	led_report_general_error(ERROR_AREA_SERIAL);
}

/*
* add error catches here
*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {
	// TODO: check error data from huart
	led_report_general_error(ERROR_AREA_SERIAL);
	// TODO: reset state machine
}

bool serial_transmit(serial_huart_port_num_t huart_port,
					 uint16_t message_type,
					 uint16_t length,
					 uint8_t* message_data,
					 void (*success_callback)(void)) {
	if (huart_port >= SERIAL_NUM_HUARTS) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return false;
	}

	serial_state_t* serial = &(known_serial_states[huart_port]);

	UART_HandleTypeDef* target_huart = serial_get_huart_handle(huart_port);
	if (target_huart == NULL) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return false;
	}
	
	if (target_huart->gState == HAL_UART_STATE_BUSY_TX)  // not done with previous transmission yet
		return false;

	serial->buf_tx[0] = SERIAL_HEAD_BYTE;
	serial->buf_tx[1] = length & 0xFF;
	serial->buf_tx[2] = length >> 8;
	serial->buf_tx[3] = tx_sequence_nums[huart_port];
	serial->buf_tx[4] = get_CRC8_check_sum(serial->buf_tx, 4, CRC8_INIT);
	serial->buf_tx[5] = message_type & 0xFF;
	serial->buf_tx[6] = message_type >> 8;


	uint8_t* next_tx_buf = &(serial->buf_tx[7]);

	if (next_tx_buf + length + SERIAL_FOOTER_LENGTH >= serial->buf_tx + SERIAL_TX_BUF_SIZE) {
		led_report_general_error(ERROR_AREA_SERIAL);
		return false;
	}

	for (uint16_t i = 0; i < length; i++) {
		*next_tx_buf = message_data[i];
		next_tx_buf++;
	}
	uint16_t CRC16_val = get_CRC16_check_sum(serial->buf_tx, 7 + length, CRC16_INIT);
	next_tx_buf[0] = CRC16_val & 0xFF;
	next_tx_buf[1] = CRC16_val >> 8;
	next_tx_buf += SERIAL_FOOTER_LENGTH;

	uint16_t total_size = next_tx_buf - serial->buf_tx;
	HAL_StatusTypeDef status = HAL_UART_Transmit_IT(serial->huart, serial->buf_tx, total_size);
	if (status != HAL_OK) {
		led_report_hal_error(status, ERROR_AREA_SERIAL);
		return false;
	}
	tx_sequence_nums[huart_port]++;
	if (success_callback != NULL) {
		(*success_callback)();
	}
	return true;
}
					 
bool serial_tx_msg_rate_ready(serial_tx_msg_rate* tx_msg_rate) {
	uint32_t cur_time = osKernelSysTick();
	if (tx_msg_rate->prev_tx_msg_timestamp == 0
		  || cur_time - tx_msg_rate->prev_tx_msg_timestamp > tx_msg_rate->min_time_between_tx_msgs_ms)
	{
		tx_msg_rate->prev_tx_msg_timestamp = cur_time;
		return true;
	} else {
		return false;
	}
}

/*** CRC8 Functions ***/

/*
** Descriptions: CRC8 checksum function
** Input: message the data buffer to check 
**        message_length the number of bytes in message
**        ucCRC8 = initialized checksum
** Output: CRC checksum
*/
uint8_t get_CRC8_check_sum(uint8_t *message, uint32_t message_length, uint8_t CRC8) {
	if (message == NULL) { 
		return (uint8_t) CRC8_INIT;
	}
	uint8_t curr_byte;
	while (message_length-- > 0) {
		curr_byte = CRC8^(*message++);
		CRC8 = CRC8_table[curr_byte];
	}
	return CRC8;
}

/*
** Descriptions: CRC8 Verify function
** Input: message the data buffer to verify
**        message_length the number of bytes in message
** Output: True or False (CRC Verify Result)
*/
uint32_t verify_CRC8_check_sum(uint8_t *message, uint32_t message_length) {
	uint8_t CRC8_expected = 0;
	if ((message == NULL) || (message_length <= 0)) return 0;
	CRC8_expected = get_CRC8_check_sum (message, message_length, CRC8_INIT);
	return ( CRC8_expected == message[message_length] );
}

/*** CRC16 Functions ***/

/*
** Descriptions: CRC16 checksum function
** Input: message the data buffer to check
**        message_length the number of bytes in message
**        ucCRC8 = initialized checksum
** Output: CRC checksum
*/
uint16_t get_CRC16_check_sum(uint8_t *message, uint32_t message_length, uint16_t CRC16) {
	if (message == NULL) { 
		return CRC16_INIT;
	}
	uint8_t curr_byte;
	while(message_length-- > 0) {
		curr_byte = *message++;
		CRC16 = ((uint16_t)(CRC16) >> 8) ^ CRC16_table[((uint16_t)(CRC16) ^ (uint16_t)(curr_byte)) & 0x00ff];
	}
	return CRC16;
}

/*
** Descriptions: CRC16 Verify function
** Input: message the data buffer to verify
**        message_length the number of bytes in message
** Output: True or False (CRC Verify Result)
*/
uint32_t verify_CRC16_check_sum(uint8_t *message, uint32_t message_length) {
	uint16_t CRC16_expected = 0;
	if ((message == NULL) || (message_length <= 0)) {
		return false;
	}
	CRC16_expected = get_CRC16_check_sum (message, message_length, CRC16_INIT);
	return ((CRC16_expected & 0xff) == message[message_length] && 
		((CRC16_expected >> 8) & 0xff) == message[message_length + 1]);
}

bool verify_CRC_vals(serial_state_t* serial) {
	return verify_CRC8_check_sum(serial->buf_CRC, 4) && 
		verify_CRC16_check_sum(serial->buf_CRC, 7 + serial->expected_message_length);
}
