#include "turret_task_17.h"
#include "cv_comms.h"
#include "math_user_utils.h"
#include "ref_comms.h"
#include "wiggle_task.h"
#include "sentinel_drive_task.h"

#if defined(TARGET_SOLDIER) || defined(TARGET_SENTINEL)

#define SMOOTING_FACTOR_YAW_17 (0.15f)
#define SMOOTING_FACTOR_PITCH_17 (0.15f)

#if defined (TARGET_SOLDIER)

#define PID_KP_PITCH_17 40.0f
#define PID_KI_PITCH_17 0.1f
#define PID_KD_PITCH_17 4000.0f
#define PID_ACCUM_MAX_PITCH_17 3000

#define PID_KP_YAW_17 60.0f
#define PID_KI_YAW_17 0.0f
#define PID_KD_YAW_17 4000
#define PID_ACCUM_MAX_YAW_17 4000

#define PID_KP_CV_YAW_17 60.0f
#define PID_KI_CV_YAW_17 0.4f
#define PID_KD_CV_YAW_17 3000
#define PID_ACCUM_MAX_CV_YAW_17 6000

#define PIT_ANG_MIN_17 (-15)
#define PIT_ANG_MAX_17 (25)
#define YAW_ANG_MIN_17 (-90)
#define YAW_ANG_MAX_17 (90)

#define SMOOTHING_FACTOR_YAW_17 (0.15f)
#define SMOOTHING_FACTOR_PITCH_17 (0.15f)
#define YAW_ANG_ERROR_MARGIN (5.0f)

#define YAW_ZERO_ENCODER (4750)
#define PITCH_ZERO_ENCODER (4060)

#define GRAVITATIONAL_TORQUE_CONSTANT_17 (4800)
#define TURRET_17_PITCH_INVERTED true
#define TURRET_17_YAW_INVERTED false

#elif defined (TARGET_SENTINEL) 

#define PID_KP_PITCH_17 80.0f
#define PID_KI_PITCH_17 0.3f
#define PID_KD_PITCH_17 4000.0f
#define PID_ACCUM_MAX_PITCH_17 3000

#define PID_KP_YAW_17 80.0f
#define PID_KI_YAW_17 0.3f
#define PID_KD_YAW_17 3200.0f
#define PID_ACCUM_MAX_YAW_17 4000

#define PID_KP_CV_YAW_17 PID_KP_YAW_17
#define PID_KI_CV_YAW_17 PID_KI_YAW_17
#define PID_KD_CV_YAW_17 PID_KD_YAW_17
#define PID_ACCUM_MAX_CV_YAW_17 PID_ACCUM_MAX_YAW_17

#define SMOOTHING_FACTOR_YAW_17 (0.9f)
#define SMOOTHING_FACTOR_PITCH_17 (0.9f)

#define PIT_ANG_MIN_17 (-25)
#define PIT_ANG_MAX_17 (0)
#define PIT_ANG_INCREMENT_17 (0.07f)
#define YAW_ANG_MIN_17 (-40)
#define YAW_ANG_MAX_17 (40)
#define YAW_ANG_INCREMENT_17 (0.07f)

#define YAW_ZERO_ENCODER 7960
#define PITCH_ZERO_ENCODER 6740
#define GRAVITATIONAL_TORQUE_CONSTANT_17 (4500)

#define TURRET_17_PITCH_INVERTED false
#define TURRET_17_YAW_INVERTED true
	
#define CV_SCAN_ANGLE_MARGIN (10)
#define SCAN_ERROR_ANGLE_MARGIN (5)
#define CV_AIM_DATA_SCAN_TIME (2000) // max time to scan around the last received cv aim data point

bool yaw_scanned_max = false;
bool pitch_scanned_max = false;
extern sentinel_drive_follow_direction_t sentinel_follow_direction;

static void sentinel_turret_follow_enemy_direction(void);
static void turret_17_auto_scan(void);
static void turret_17_scan_axis
(
	position_control_motor_id_t axis_motor_id,
	float* axis_angle_accumulated,
	bool* axis_scanned_max,
	float axis_angle_max,
	float axis_angle_min,
	float axis_angle_increment
);

#endif 

#define CV_MAX_WAIT_TIME (100)

uint32_t last_cv_data_timestamp = 0;
uint32_t iteration_time;
uint32_t last_iteration_time;
bool is_in_auto_aim_mode = false;
uint32_t time_entered_cv_mode = 0;
float yaw_user_acumulated = TURRET_ZERO_ANG;
float pitch_user_acumulated = TURRET_ZERO_ANG;
float yaw_cv_derivative = 0;
	
static void turret_17_manual_handler(void);
static void turret_17_request_cv_aim_data(void);
static void turret_17_auto_handler(bool* disable_pid_d);
static void turret_17_configure_pid(bool is_cv_mode);
	
#if defined(TARGET_SOLDIER)
extern bool chassis_rotate_to_turret;
extern bool chassis_auto_rotate_wait_for_turret_init;
#endif

void specific_turret_init_17(uint8_t turret_id) {
	#if defined (TARGET_SOLDIER)
	turret_enable_wiggle_add(turret_id);
	#endif

	position_motor_init_angles(
		MOTOR_YAW_TURRET_17_ID, 
		TURRET_ZERO_ANG + YAW_ANG_MAX_17, 
		TURRET_ZERO_ANG + YAW_ANG_MIN_17, 
		TURRET_ZERO_ANG
	);
	position_motor_init_angles(
		MOTOR_PITCH_TURRET_17_ID, 
		TURRET_ZERO_ANG + PIT_ANG_MAX_17,	
		TURRET_ZERO_ANG + PIT_ANG_MIN_17, 
		TURRET_ZERO_ANG
	);
	
	position_motor_init_inverted(MOTOR_PITCH_TURRET_17_ID, TURRET_17_PITCH_INVERTED);
	position_motor_init_inverted(MOTOR_YAW_TURRET_17_ID, TURRET_17_YAW_INVERTED);
	turret_17_configure_pid(false);
}

void turret_task_17(void const* argu) {
	turret_init(
		TURRET_17_ID, 
		MOTOR_YAW_TURRET_17_ID, 
		MOTOR_PITCH_TURRET_17_ID,
		get_motor(&can1, TURRET_YAW_MOTOR_17),
		get_motor(&can1, TURRET_PIT_MOTOR_17),
		YAW_ZERO_ENCODER,
		PITCH_ZERO_ENCODER,
		SMOOTHING_FACTOR_PITCH_17,
		SMOOTHING_FACTOR_YAW_17
	);

	specific_turret_init_17(TURRET_17_ID);

	robot_init[TURRET17_INIT] = true;
	waiting_for_init();
		
	while(1) {
		iteration_time = osKernelSysTick();

		bool disable_pid_d = false;
		if (robot_mode == KILL_MODE) {
			#if defined(TARGET_SOLDIER)
			chassis_auto_rotate_wait_for_turret_init = false;
			#endif
		} else if (robot_aim_mode == AIM_MANUAL) {
			turret_17_manual_handler();
		} else if (robot_aim_mode == AIM_CV) {
			turret_17_request_cv_aim_data();
			turret_17_auto_handler(&disable_pid_d);
		}
		if (turret_aim_handler(TURRET_17_ID, &yaw_user_acumulated, &pitch_user_acumulated) == TURRET_OK) {
			if (disable_pid_d) {
				position_motor_set_masked_d(MOTOR_YAW_TURRET_17_ID, disable_pid_d);
				position_motor_set_masked_d(MOTOR_PITCH_TURRET_17_ID, disable_pid_d);
			}

			turret_run_pid(TURRET_17_ID);
			position_motor_adjust_voltage_desired(MOTOR_PITCH_TURRET_17_ID,
				- (int32_t) (GRAVITATIONAL_TORQUE_CONSTANT_17 * sinf( PI / 180 * turret_get_pitch_ang_actual(TURRET_17_ID))));

			position_motor_set_masked_d(MOTOR_YAW_TURRET_17_ID, false);
			position_motor_set_masked_d(MOTOR_PITCH_TURRET_17_ID, false);
		}
		#if defined(TARGET_SOLDIER)
		if (fabs(position_motor_get_ang_actual(MOTOR_YAW_TURRET_17_ID)
			  - position_motor_get_curr_ang_setpoint(MOTOR_YAW_TURRET_17_ID))
			  < YAW_ANG_ERROR_MARGIN)
		{
			// enable auto rotation after the turret has reached the setpoint the first time
			chassis_auto_rotate_wait_for_turret_init = true;
		}
		#endif

		last_iteration_time = iteration_time;
		osDelay(1); 
	}
}

static void turret_17_manual_handler(){
	turret_17_configure_pid(false);
	yaw_user_acumulated -= interface_bind_yaw_ctrl();
	pitch_user_acumulated -= interface_bind_pitch_ctrl();

	if (is_in_auto_aim_mode) {
		is_in_auto_aim_mode = false;
		cv_comms_stop_tracking_target();
	}
}

static void turret_17_auto_handler(bool* disable_pid_d){
	turret_17_configure_pid(true);
	cv_comms_turret_aim_data_t aim_data;
	if (cv_comms_get_last_aim_data(&aim_data)
		  && aim_data.receive_timestamp >= time_entered_cv_mode
			&& aim_data.has_target
			#if defined(TARGET_SENTINEL)
			&& osKernelSysTick() - aim_data.receive_timestamp < CV_MAX_WAIT_TIME
			#endif
	){
		if (aim_data.receive_timestamp == last_cv_data_timestamp) {
			if (last_iteration_time) {
				yaw_user_acumulated += yaw_cv_derivative * (iteration_time - last_iteration_time);
			}
		} else {
			*disable_pid_d = true;
			yaw_cv_derivative = (aim_data.yaw - yaw_user_acumulated) / (aim_data.receive_timestamp - last_cv_data_timestamp);

			last_cv_data_timestamp = aim_data.receive_timestamp;
		}

		#if defined(TARGET_SOLDIER)
		if (!chassis_rotate_to_turret) {
			yaw_user_acumulated = aim_data.yaw;
			pitch_user_acumulated = aim_data.pitch;
		}
		#else
		yaw_user_acumulated = aim_data.yaw;
		pitch_user_acumulated = aim_data.pitch;
		#endif
	} else {
		#if defined (TARGET_SENTINEL)
		turret_17_auto_scan();
		sentinel_turret_follow_enemy_direction();
		#endif
	}
}

static void turret_17_configure_pid(bool is_cv_mode) {
	position_motor_init_pid(
		MOTOR_YAW_TURRET_17_ID,
		SMOOTHED_POSITION_PID,
		is_cv_mode ? PID_KP_CV_YAW_17 : PID_KP_YAW_17,
		is_cv_mode ? PID_KI_CV_YAW_17 : PID_KI_YAW_17,
		is_cv_mode ? PID_KD_CV_YAW_17 : PID_KD_YAW_17
	);
	position_motor_set_integral_accum_limit(MOTOR_YAW_TURRET_17_ID, is_cv_mode ? PID_ACCUM_MAX_CV_YAW_17 : PID_ACCUM_MAX_YAW_17);

	position_motor_init_pid(
		MOTOR_PITCH_TURRET_17_ID, 
		SMOOTHED_POSITION_PID,
		PID_KP_PITCH_17, 
		PID_KI_PITCH_17, 
		PID_KD_PITCH_17 
	);
	position_motor_set_integral_accum_limit(MOTOR_PITCH_TURRET_17_ID, PID_ACCUM_MAX_PITCH_17);
}

static void turret_17_request_cv_aim_data() {
	cv_comms_turret_aim_data_t aim_data;
	if ((!is_in_auto_aim_mode 
		  || !cv_comms_get_last_aim_data(&aim_data))
      && (osKernelSysTick() - time_entered_cv_mode > CV_MAX_WAIT_TIME))
	{
		// Request cv aim data when entering CV aim mode or 
		// again if we haven't gotten any valid aim data back in CV_MAX_WAIT_TIME ticks
		time_entered_cv_mode = osKernelSysTick();
		cv_comms_begin_tracking_target();
		is_in_auto_aim_mode = true;
	}
}

#if defined (TARGET_SENTINEL)
static void turret_17_auto_scan() {
	cv_comms_turret_aim_data_t aim_data;
	if (cv_comms_get_last_aim_data(&aim_data)
			&& osKernelSysTick() - aim_data.receive_timestamp < CV_AIM_DATA_SCAN_TIME
			&& aim_data.has_target)
	{
		// small area scan around the last cv aim data input
		turret_17_scan_axis
		(
			MOTOR_YAW_TURRET_17_ID,
			&yaw_user_acumulated,
			&yaw_scanned_max,
			(aim_data.yaw - CV_SCAN_ANGLE_MARGIN < YAW_ANG_MIN_17) ? YAW_ANG_MIN_17 : aim_data.yaw - CV_SCAN_ANGLE_MARGIN,
			(aim_data.yaw + CV_SCAN_ANGLE_MARGIN > YAW_ANG_MAX_17) ? YAW_ANG_MAX_17 : aim_data.yaw + CV_SCAN_ANGLE_MARGIN,
			YAW_ANG_INCREMENT_17
		);
		turret_17_scan_axis
		(
			MOTOR_PITCH_TURRET_17_ID,
			&pitch_user_acumulated,
			&pitch_scanned_max,
			(aim_data.pitch - CV_SCAN_ANGLE_MARGIN < PIT_ANG_MIN_17) ? PIT_ANG_MIN_17 : aim_data.pitch - CV_SCAN_ANGLE_MARGIN,
			(aim_data.pitch + CV_SCAN_ANGLE_MARGIN > PIT_ANG_MAX_17) ? PIT_ANG_MAX_17 : aim_data.pitch + CV_SCAN_ANGLE_MARGIN,
			PIT_ANG_INCREMENT_17
		);
	} else {
		// scans the full range of the turret when no targets from cv has been detected 
		// for CV_AIM_DATA_SCAN_TIME amount of times
		turret_17_scan_axis
		(
			MOTOR_YAW_TURRET_17_ID,
			&yaw_user_acumulated,
			&yaw_scanned_max,
			YAW_ANG_MIN_17,
			YAW_ANG_MAX_17,
			YAW_ANG_INCREMENT_17
		);
		turret_17_scan_axis
		(
			MOTOR_PITCH_TURRET_17_ID,
			&pitch_user_acumulated,
			&pitch_scanned_max,
			PIT_ANG_MIN_17,
			PIT_ANG_MAX_17,
			PIT_ANG_INCREMENT_17
		);
	}
}

static void turret_17_scan_axis
(
	position_control_motor_id_t axis_motor_id,
	float* axis_angle_accumulated,
	bool* axis_scanned_max,
	float axis_angle_min,
	float axis_angle_max,
	float axis_angle_increment
){
	if (*axis_scanned_max) {
		*axis_angle_accumulated = position_motor_get_curr_ang_setpoint(axis_motor_id) - axis_angle_increment;
		if (position_motor_get_ang_actual(axis_motor_id) < TURRET_ZERO_ANG + axis_angle_min + SCAN_ERROR_ANGLE_MARGIN) {
			*axis_scanned_max = false;
		}
	} else {
		*axis_angle_accumulated = position_motor_get_curr_ang_setpoint(axis_motor_id) + axis_angle_increment;
		if (position_motor_get_ang_actual(axis_motor_id) > TURRET_ZERO_ANG + axis_angle_max - SCAN_ERROR_ANGLE_MARGIN) {
			*axis_scanned_max = true;
		}
	}
}

// sets the direction for the sentinel drive to follow
static void sentinel_turret_follow_enemy_direction() {
	cv_comms_turret_aim_data_t aim_data;
	if (cv_comms_get_last_aim_data(&aim_data)
			&& osKernelSysTick() - aim_data.receive_timestamp < CV_AIM_DATA_SCAN_TIME
			&& aim_data.has_target)
	{
		if (aim_data.yaw > TURRET_ZERO_ANG) {
			sentinel_follow_direction = FOLLOW_ENEMY_RIGHT;
		} else {
			sentinel_follow_direction = FOLLOW_ENEMY_LEFT;
		}
	}
}
#endif
#endif
