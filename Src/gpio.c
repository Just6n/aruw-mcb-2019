/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */
#include "adc.h"
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

typedef struct {
    uint32_t pin_1;
    uint32_t pin_2;
    uint32_t pin;
} pin_vals;

pin_vals analog_values;

// Private function that helps reduce redundancy with the GPIO_Analog_Read function
float GPIO_Analog_Read_Helper(ADC_HandleTypeDef* hadc, int pin);
/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
     PC0   ------> SharedAnalog_PC0
     PC1   ------> SharedAnalog_PC1
     PC3   ------> SharedAnalog_PC3
     PA4   ------> SharedAnalog_PA4
     PC4   ------> SharedAnalog_PC4
     PA5   ------> SharedAnalog_PA5
     PC5   ------> SharedAnalog_PC5
     PB1   ------> SharedAnalog_PB1
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_12, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOH, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, GPIO_PIN_8|GPIO_PIN_7|GPIO_PIN_6|GPIO_PIN_5 
                          |GPIO_PIN_4|GPIO_PIN_3|GPIO_PIN_2|GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE4 PE5 PE6 PE12 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PI9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : PF0 PF1 PF6 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PH2 PH3 PH4 PH5 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : PG8 PG7 PG6 PG5 
                           PG4 PG3 PG2 PG1 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_7|GPIO_PIN_6|GPIO_PIN_5 
                          |GPIO_PIN_4|GPIO_PIN_3|GPIO_PIN_2|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : PF10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC1 PC3 PC4 
                           PC5 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_4 
                          |GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PC2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA4 PA5 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 2 */
void GPIO_Digital_Write(uint16_t pin, GPIO_PinState PinState) {
    switch(pin) {
        case PIN_I1: 
            HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1,PinState);
            break;
        case PIN_I2:
			HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0,PinState);
			break;
        case PIN_J1: 
            HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5,PinState);
            break;
        case PIN_J2:
            HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,PinState);
            break;
        case PIN_K1: 
            HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6,PinState);
            break;
        case PIN_K2:
            HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12,PinState);
            break;
        default:
            break;
	}
}

void GPIO_Digital_Toggle(uint16_t pin) {
	switch(pin) {
		case PIN_I1: 
			HAL_GPIO_TogglePin(GPIOF, GPIO_PIN_1);
			break;
		case PIN_I2:
			HAL_GPIO_TogglePin(GPIOF, GPIO_PIN_0);
			break;
		case PIN_J1: 
			HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_5);
			break;
		case PIN_J2:
			HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_4);
			break;
		case PIN_K1: 
			HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_6);
			break;
		case PIN_K2:
			HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_12);
            break;
		default:
			break;
	}
}


GPIO_PinState GPIO_Digital_Read(uint16_t pin) {
    GPIO_PinState bit_status;
    switch(pin) {
		case PIN_L1: 
            bit_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);
            break;
		case PIN_L2:
            bit_status = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0);
            break;
		case PIN_Q1: 
            bit_status = HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_10);
            break;
		case PIN_Q2:
            bit_status = HAL_GPIO_ReadPin(GPIOI, GPIO_PIN_9);
            break;
		default:
            break;
	}
    return bit_status;
}

// returns the voltage read by the pin
// takes in a 16 bit integer corresponding to a pin
float GPIO_Analog_Read(uint16_t pin) {
    if(pin == PIN_M2) {         // ADC1
        return GPIO_Analog_Read_Helper(&hadc1, 1);
    } else if (pin == PIN_M1) { // ADC1
        return GPIO_Analog_Read_Helper(&hadc1, 2);
    } else if(pin == PIN_N1) {  // ADC2
        return GPIO_Analog_Read_Helper(&hadc2, 1);
    } else if(pin == PIN_O1){   // ADC2
        return GPIO_Analog_Read_Helper(&hadc2, 2);
    } else if(pin == PIN_N2) {  // ADC 3
        return GPIO_Analog_Read_Helper(&hadc3, 1);
    } else if (pin == PIN_O2) { // ADC 3
        return GPIO_Analog_Read_Helper(&hadc3, 2);
    } else {
        return 0;
    }
}

float GPIO_Analog_Read_Helper(ADC_HandleTypeDef* hadc, int pin) {
    HAL_ADC_Start(hadc);
    HAL_ADC_PollForConversion(hadc, 100);
    analog_values.pin_1 = HAL_ADC_GetValue(hadc);
    HAL_ADC_PollForConversion(hadc, 100);
    analog_values.pin_2 = HAL_ADC_GetValue(hadc);
    HAL_ADC_Stop(hadc);
    if(pin == 1) {
        analog_values.pin = analog_values.pin_1;
    } else if (pin == 2) {
        analog_values.pin = analog_values.pin_2;
    } else {
        return 0;
    }
		/*
		if (analog_values.pin > 4086) {
			return 3300;
		} 
		*/
		return analog_values.pin/ANALOG_PRESCALER;
}

/* USER CODE END 2 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
