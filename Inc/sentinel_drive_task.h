#ifndef _SENTINEL_DRIVE_TASK_H_
#define _SENTINEL_DRIVE_TASK_H_

#include "stdlib.h"
#include "ir_distance_sensor.h"
#include "position_motor_control.h"
#include "ref_comms.h"
#include "gpio.h"
#include "math_user_utils.h"
#include "cmsis_os.h"
#include "operator_interface_bindings.h"
#include "math.h"
#include "control_mode.h"


#if defined (TARGET_SENTINEL)

#define POWER_LIMIT 20.0f  // power limit in Watts
#define SOFT_MAX_ENERGY_BUFF 100.0f // standard max energy in joules to be used in the buffer
#define HARD_MAX_ENERGY_BUFF 200.0f  // actual max energy in joules to be used in the buffer when the robot has been hit

typedef enum {
  FOLLOW_ENEMY_LEFT = 0,
  FOLLOW_ENEMY_RIGHT = 1,
  NOT_FOLLOWING_ENEMIES = 2,
} sentinel_drive_follow_direction_t;

typedef struct {
  dji_motor_t *motor;
  bool is_inverted;
  int32_t calibrated_encoder_start_value;
  pid_t position_pid;
} sentinel_drive_state_t;

void sentinel_drive_task(void const* argu);

#endif

#endif //_SENTINEL_DRIVE_TASK_H__
