#ifndef _TURRET_TASK_42_H_
#define _TURRET_TASK_42_H_

#include "turret_task.h"

#if defined(TARGET_HERO)

void turret_task_42(void const* argu);

#endif

#endif
