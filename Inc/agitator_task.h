#ifndef _AGITATOR_TASK_H_
#define _AGITATOR_TASK_H_

#if !defined(TARGET_ENGINEER)

#include "operator_interface_bindings.h"
#include "position_motor_control.h"
#include "math.h"
#include "cmsis_os.h"

typedef enum {
	#if !defined(TARGET_ENGINEER)
	AGITATOR_17 = 0,
	NUMBER_OF_AGITATORS = 1,
	#endif
} agitator_motor_id_t;

typedef struct{
	
	position_control_motor_id_t motor_id; // motor associated with the agitator
	agitator_motor_id_t agitator_id;	// id, defined in the agitator_motor_id_t enum in the .h file
																		// an agitator for something else
	uint32_t max_rotation_time; // for unjamming, if the agitator takes longer than this to rotate, 
															//unjamming will commence, units are milliseconds
	uint32_t jam_timestamp; // starting time that the last jam was detected
	uint32_t prev_set_time; // used to calculate how long the agitator has taken to rotate.
	float setpoint_distance_error; // the error incurred when the agitator unjams, this will be 
																 //added back to the agitator setpoint when foward rotation has commenced	
	enum{
		WAITING = 0,
		RUNNING = 1,
		UNJAM = 2,
		FORWARD_UNJAM = 3,
	} agitator_fire_sequence;

	int16_t fire_count; // the number of rotation increments left for the agitator to rotate. For example, if 
											// fire count = 1, the agitator will increment the setpoint to COMPLETE_FIRE_ROTATE 
											// in the agitator_handler
	
	float rand_unjam_angle;
	float rand_unjam_wait_time;
	bool is_jammed; // true if the agitator is jammed, false otherwise
} agitator_motor_t;

motor_control_error_t agitator_handler(agitator_motor_id_t id);

motor_control_error_t agitator_request_fire(agitator_motor_id_t id, int8_t fire_count);

motor_control_error_t agitator_motor_init(
	agitator_motor_id_t agitator_id, 
	position_control_motor_id_t control_motor_id, 
	dji_motor_t* motor, 
	float kp, 
	float ki, 
	float kd, 
	float gear_ratio, 
	uint32_t max_rotation_time
);

bool is_agitator_firing(agitator_motor_id_t id);

#endif 

#endif
