#ifndef __OPERATOR_INTERFACE_BINDINGS_H__
#define __OPERATOR_INTERFACE_BINDINGS_H__

#include "stm32f4xx_hal.h"
#include "stdbool.h"
#include "control_mode.h"
#include "remote.h"
#include "math_user_utils.h"

#define rc_s1 dr16_get_remote()->rc.s1
#define rc_s2 dr16_get_remote()->rc.s2

// SOLDIER SPECIFIC KEY DEFINES
#if defined(TARGET_SOLDIER)
#define KEY_BASE_CTRL 0
#define KEY_WIGGLE KEY_PRESSED_OFFSET_V
#define KEY_TURRET_LOCK	0
#define KEY_KILL 0
#define KEY_HOPPER KEY_PRESSED_OFFSET_R
#define KEY_DISABLE_AUTO_ROTATE KEY_PRESSED_OFFSET_C
#define KEY_UNJAM_REQUEST_PRESSED() KEY_PRESSED(KEY_PRESSED_OFFSET_F, dr16_get_remote()->computer.key)
#define REMOTE_CTRL_BASE_CTRL() (dr16_get_remote()->rc.s1 == 3)
#define REMOTE_CTRL_WIGGLE() (dr16_get_remote()->rc.s1 == 1)
#define REMOTE_CTRL_LOCK_TURRET() (0)
#endif

#if !defined(TARGET_ENGINEER)
// AGITATOR
#define COMPUTER_FIRE_SINGLE() (0)
#define COMPUTER_FIRE_BURST() (0)
#define COMPUTER_FIRE_AUTO() (dr16_get_remote()->computer.mouse.press_l)

#define REMOTE_CTRL_FIRE_SINGLE() 0
#define REMOTE_CTRL_FIRE_BURST() 0
#define REMOTE_CTRL_FIRE_FULL() (dr16_get_remote()->rc.s2 == 2) 

#define KEY_LOW_FIRE_SPEED_PRESSED() KEY_PRESSED(KEY_PRESSED_OFFSET_SHIFT, dr16_get_remote()->computer.key)

#define REMOTE_CONTROLLING_CV_FIRE() 0 //(dr16_get_remote()->rc.s1 == 3)
#endif

// GENERIC TURRET DEFINES 
#if defined(TARGET_SOLDIER) || defined(TARGET_HERO) || defined(TARGET_SENTINEL)
#define COMPUTER_CTRL_YAW() (dr16_get_remote()->computer.mouse.average_x_mouse)
#define COMPUTER_CTRL_PITCH() (dr16_get_remote()->computer.mouse.average_y_mouse)

#define REMOTE_CTRL_YAW() ((float) dr16_get_remote()->rc.ch0)
#define REMOTE_CTRL_PITCH() ((float) dr16_get_remote()->rc.ch1)
#endif

// ENGINEER SPECIFIC KEY DEFINES
#if defined(TARGET_ENGINEER)
#define REMOTE_CTRL_COLLECT_SIGN(ch) (sign((rc_s1 == 3) * dr16_get_remote()->rc.ch))
#define REMOTE_CTRL_DUMP() (rc_s1 == 3 && rc_s2 == 2)
#define REMOTE_CTRL_TOW() (rc_s1 == 2 && rc_s2 == 1)
#define REMOTE_CTRL_GRAB() (rc_s1 == 3 && rc_s2 == 1)
#define REMOTE_CTRL_KILL() ((rc_s1 == 1 && rc_s2 == 1) || (rc_s1 == 2 && rc_s2 == 2))

#define KEY_DUMP KEY_PRESSED_OFFSET_R
#define KEY_TOW KEY_PRESSED_OFFSET_C
#define KEY_KILL KEY_PRESSED_OFFSET_B
#define KEY_GRAB KEY_PRESSED_OFFSET_G
#define KEY_GRAB_TGGL KEY_PRESSED_OFFSET_F
#define KEY_LIFT_TGGL KEY_PRESSED_OFFSET_E
#endif

// SENTINEL SPECIFIC KEY DEFINES
#if defined(TARGET_SENTINEL)
#define REMOTE_CTRL_SENTINEL_MANUAL_MODE() (rc_s1 == 3)
#endif

// GENERIC KEY DEFINES FOR ALL ROBOTS
#define KEY_LOW_CHASSIS_SPEED KEY_PRESSED_OFFSET_CTRL
#define KEY_FOWARD_PRESSED() (KEY_PRESSED(KEY_PRESSED_OFFSET_W, dr16_get_remote()->computer.key))
#define KEY_BACKWARD_PRESSED() (KEY_PRESSED(KEY_PRESSED_OFFSET_S, dr16_get_remote()->computer.key))
#define KEY_ROTATE_CLOCK_PRESSED() (KEY_PRESSED(KEY_PRESSED_OFFSET_E, dr16_get_remote()->computer.key))
#define KEY_ROTATE_COUNTERCLOCK_PRESSED() (KEY_PRESSED(KEY_PRESSED_OFFSET_Q, dr16_get_remote()->computer.key))
#define KEY_LEFT_PRESSED() (KEY_PRESSED(KEY_PRESSED_OFFSET_A, dr16_get_remote()->computer.key))
#define KEY_RIGHT_PRESSED() (KEY_PRESSED(KEY_PRESSED_OFFSET_D, dr16_get_remote()->computer.key))
#define REMOTE_MATCHES_INIT_CONFIGURATION() (interface_bind_request_base_ctrl_mode() && REMOTE_CONTROLLING_CHASSIS_ROTATION())

// GENERIC REMOTE CONTROL DEFINES
#if defined(TARGET_SENTINEL)
#define REMOTE_CONTROLLING_TURRET() (robot_mode == BASE_CTRL_MODE)
#else
#define REMOTE_CONTROLLING_TURRET() (dr16_get_remote()->rc.s2 == 1)
#endif
#define REMOTE_CONTROLLING_CHASSIS_ROTATION() (dr16_get_remote()->rc.s2 == 3)
#define REMOTE_CTRL_CHASSIS_HORIZONTAL() ((float) dr16_get_remote()->rc.ch2)
#define REMOTE_CTRL_CHASSIS_VERTICAL() ((float) dr16_get_remote()->rc.ch3)
#define REMOTE_CTRL_ROTATE() ((float) dr16_get_remote()->rc.ch0)

#define REMOTE_CONTROLLING_AUTO_AIM() (dr16_get_remote()->rc.s2 == 3)
#define COMPUTER_CONTROLLING_AUTO_AIM() (dr16_get_remote()->computer.mouse.press_r)

// GENERIC MOVEMENT FUNCTIONS
float interface_bind_move_horizontal(void);
float interface_bind_move_vertical(void);
float interface_bind_rotate_chassis(void);
bool interface_bind_request_kill(void);

#if defined(TARGET_ENGINEER)
bool interface_bind_request_tow(void);
bool interface_bind_request_dump(void);
bool interface_bind_request_grab(void);
bool interface_bind_request_grab_toggle(void);
bool interface_bind_request_lift_toggle(void);
#endif

#if defined(TARGET_SENTINEL)
bool interface_bind_request_sentinel_manual_mode(void);
#endif

#if !defined(TARGET_ENGINEER)
float interface_bind_yaw_ctrl(void);
float interface_bind_pitch_ctrl(void);
bool interface_bind_request_auto_aim(void);
bool interface_bind_request_wiggle(void);
bool interface_bind_request_turret_lock(void);
#endif

#if !defined(TARGET_ENGINEER)
// AGITATOR FUNCTIONS
bool interface_bind_request_fire_single_17(void);
bool interface_bind_request_fire_burst_17(void);
bool interface_bind_request_fire_full_17(void);
bool interface_bind_request_auto_fire(void);
bool interface_bind_request_fire_slow_speed(void);
// HOPPER FUNCTIONS
bool interface_bind_request_open_hopper(void);
#endif

bool interface_bind_request_base_ctrl_mode(void);
float interface_bind_keyboard_slow_drive(void);
bool interface_bind_toggle_disable_auto_rotate_chassis(void);

#if defined(TARGET_SOLDIER) || defined(TARGET_SENTINEL)
#define REMOTE_CTRL_KILL() (dr16_get_remote()->rc.s1 == 2)
#endif

#if defined(TARGET_SOLDIER) || defined(TARGET_SENTINEL)
#define REMOTE_CTRL_KILL() (dr16_get_remote()->rc.s1 == 2)
#endif

// safety undeclared bindings
#ifndef REMOTE_CTRL_KILL
#define REMOTE_CTRL_KILL() true
#endif

#ifndef KEY_KILL
#define KEY_KILL true
#define REMOTE_CTRL_BASE_CTRL()
#endif

#ifndef REMOTE_CTRL_BASE_CTRL
#define REMOTE_CTRL_BASE_CTRL() false
#endif

#ifndef KEY_BASE_CTRL
#define KEY_BASE_CTRL false
#endif

#endif // __OPERATOR_INTERFACE_BINDINGS_H__
