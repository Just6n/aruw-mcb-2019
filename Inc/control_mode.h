#ifndef __TASK_MODE_H__
#define __TASK_MODE_H__

#include "stm32f4xx_hal.h"
#include "stdbool.h"
#include "operator_interface_bindings.h"
#include "cmsis_os.h"

typedef enum {
	INIT_MODE = 0,         // initialize everything we need 
	BASE_CTRL_MODE = 1,    // manual control, no extra features enabled
	WIGGLE_MODE = 2,       // automatically wiggle the chassis, you can still drive the chassis
	KILL_MODE = 3,         // send nothing to chassis and turret
	LOCK_TURRET_MODE = 4,  // lock the turret in place with manual chassis control
	#if defined (TARGET_SENTINEL)
	AUTO_MODE = 5,              // sets the robot features to be autonomous
	#endif
} robot_mode_t;

typedef enum{
	#if defined (TARGET_SENTINEL) || (TARGET_DRONE)
	TURRET17_INIT = 0,
	NUM_INITS = 1,
	#elif defined(TARGET_SOLDIER)
	DRIVE_INIT = 0,
	TURRET17_INIT = 1,
	NUM_INITS = 2, 
	#elif defined(TARGET_HERO)
	DRIVE_INIT = 0,
	TURRET17_INIT = 1,
	TURRET42_INIT = 2,
	NUM_INITS = 3,
	#elif defined(TARGET_ENGINEER)
	DRIVE_INIT = 0,
	GRABBER_INIT = 1,
	LIFT_INIT = 2,
	NUM_INITS = 3,
	#else 
	NUM_INITS = 1
	#endif
} robot_init_t;

extern bool robot_init[NUM_INITS];

typedef enum {
	AIM_MANUAL,
	AIM_CV
} robot_aim_mode_t;

extern robot_mode_t robot_mode;
extern robot_aim_mode_t robot_aim_mode;

void control_mode_init(void);
void control_mode_task(void const* argu);
void waiting_for_init(void);

#endif
