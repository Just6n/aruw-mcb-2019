#ifndef _POSITION_MOTOR_CONTROL_H_
#define _POSITION_MOTOR_CONTROL_H_

#include "stm32f4xx_hal.h"
#include "dji_motor.h"
#include "stdbool.h"
#include "hardware_contract.h"
#include "led_error_handler.h"
#include "math_user_utils.h"

/*
 * dji_motor_t -> position_motor_control_t -> turret_t and agitator_t
 */

typedef enum {
	#if defined(TARGET_ENGINEER)
	MOTOR_LIFT_ID = 0,
	MOTOR_GRABBER_ID = 1,
	NUMBER_POSITION_MOTORS = 2,
	#elif defined(TARGET_HERO)
	MOTOR_YAW_TURRET_42_ID = 0,
	MOTOR_PITCH_TURRET_42_ID = 1,
	NUMBER_POSITION_MOTORS = 2,
	#elif defined(TARGET_SOLDIER) || defined(TARGET_DRONE) || defined(TARGET_SENTINEL)
	MOTOR_YAW_TURRET_17_ID = 0, 
	MOTOR_PITCH_TURRET_17_ID = 1,
	MOTOR_AGITATOR_ID = 2,
	NUMBER_POSITION_MOTORS = 3,
	#endif
} position_control_motor_id_t;

void position_motor_control_task(void const* argu);

// init functions
motor_control_error_t position_motor_init(position_control_motor_id_t id, dji_motor_t* dji_motor); // inialize all variables to their base value

motor_control_error_t init_shift_ang(position_control_motor_id_t id, int16_t zero_encoder);

motor_control_error_t position_motor_run_pid(position_control_motor_id_t id); // position_motor/set to angle motor functions

motor_control_error_t set_ang_des(position_control_motor_id_t id, float ang_setpoint); // THIS IS FOR SETTING MOTOR ANGLE VALUES

motor_control_error_t position_motor_init_angles(position_control_motor_id_t id, int32_t max_ang, int32_t min_ang, int32_t zero_angle); // initialize relevant angle settings

motor_control_error_t position_motor_init_inverted(position_control_motor_id_t id, bool inverted); // tell the motor whether or not to flip the way angle setpoints are calcualted (for pitch motors in particular)

motor_control_error_t position_motor_init_gear_ratio(position_control_motor_id_t id, uint8_t gear_ratio);

motor_control_error_t position_motor_init_pid(position_control_motor_id_t id, uint32_t pid_mode, float p, float i, float d);

motor_control_error_t position_motor_set_masked_d(position_control_motor_id_t id, bool mask_d);

motor_control_error_t position_motor_set_integral_accum_limit(position_control_motor_id_t id, uint32_t accum_limit);

motor_control_error_t position_motor_adjust_voltage_desired(position_control_motor_id_t id, int32_t offset_amount);

motor_control_error_t position_motor_init_avg_output_factor(position_control_motor_id_t id, float factor);

// getter functions
int16_t position_motor_get_encoder_wrapped(position_control_motor_id_t id);	// get the motor's wrapped encoder value

float position_motor_get_curr_ang_setpoint(position_control_motor_id_t id);

int16_t position_motor_get_max_ang(position_control_motor_id_t id);

int16_t position_motor_get_min_ang(position_control_motor_id_t id);

int16_t position_motor_get_current_actual(position_control_motor_id_t id);

float position_motor_get_ang_actual(position_control_motor_id_t id);		// THIS IS THE FUNCTION YOU WANT TO CALL AND IT GIVES YOU CURRENT MOTOR ANGLE

bool position_motor_get_motor_inverted(position_control_motor_id_t id);

bool position_motor_get_motor_online(position_control_motor_id_t id);

int16_t position_motor_get_rpm(position_control_motor_id_t id);

#endif
