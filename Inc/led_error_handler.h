#ifndef __LED_ERROR_HANDLER_H__
#define __LED_ERROR_HANDLER_H__

#include "stm32f4xx_hal.h"

/**
 * Uses the onboard LEDs to report a HAL error.
 * 
 * The "source_area" should be one of the known ERROR_AREA_* values; hal_error is
 * the error code returned from a HAL call.
 * 
 * The bottom two LEDs are used to display the HAL error code:
 * 
 *   - HAL_ERROR: 0x01
 *   - HAL_BUSY: 0x02
 *   - HAL_TIMEOUT: 0x03
 * 
 * The top 6 bits display the area.
 */
void led_report_hal_error(HAL_StatusTypeDef hal_error, uint8_t source_area);

/**
 * Uses the onboard LEDs to report a "general" error.
 * 
 * The "source_area" should be one of the known ERROR_AREA_* values.
 * 
 * The top 6 bits display the area. The bottom two LEDs are zeroed (off).
 */
void led_report_general_error(uint8_t source_area);

// legacy helper for CAN
void can_handle_error(HAL_StatusTypeDef error_code);

/*
 *=======================USER DEFINED ERRORS BELOW=====================================================
 */

#define ERROR_AREA_CAN 0x01
#define ERROR_AREA_SERIAL 0x02
#define ERROR_AREA_CV_COMMS 0x03
#define ERROR_AREA_REF_COMMS 0x04

typedef enum {
	POSITION_MOTOR_OK = 0,
	POSITION_MOTOR_ID_ERROR = 1,
	POSITION_MOTOR_INIT_ERROR = 2,
	POSITION_MOTOR_SET_ANGLE_ERROR = 3, 
	TURRET_OK = 4,
	TURRET_ID_ERROR = 5,
	TURRET_INIT_ERROR = 6,
	TURRET_NO_POWER = 7,
} motor_control_error_t;

#endif
