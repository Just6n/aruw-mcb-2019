#ifndef __serial_h_
#define __serial_h_

#include "stm32f4xx_hal.h"
#include "stdbool.h"

#define SERIAL_RX_BUF_SIZE 256
#define SERIAL_TX_BUF_SIZE 256

#define SERIAL_HEAD_BYTE 0xA5
#define SERIAL_FOOTER_LENGTH 2

#define CRC8_INIT 0xff
#define CRC16_INIT 0xffff

/**
 * serial packet protocol
 * [Head byte = 0xA5 (1-byte), message length (2-byte), sequence number (1-byte), 
 *  CRC8 (1-byte), message type (2-byte), message (message_length-byte), CRC16 (2-byte)]
 * 
 */

typedef void (*serial_message_handler_t)(uint16_t message_type, uint8_t* data_buffer, uint16_t length);

typedef enum {
	WAITING_FOR_HEAD_BYTE = 0, 			// head byte (1-byte)
	WAITING_FOR_MESSAGE_LENGTH = 1,	// length of message (2-byte)
	WAITING_FOR_MESSAGE_DATA = 2,		// rest of data in packet [1-byte sequence num, 1-byte CRC8, message_length-byte message, 2-byte CRC16]
} serial_mode_t;

typedef enum {
	SERIAL_HUART1 = 0,
	SERIAL_HUART2 = 1,
	SERIAL_HUART6 = 2,
	SERIAL_NUM_HUARTS = 3
} serial_huart_port_num_t;

typedef struct {
	// tx/rx buffers
	uint8_t buf_rx[SERIAL_RX_BUF_SIZE];
	uint8_t buf_tx[SERIAL_TX_BUF_SIZE];
	
	// state information
	serial_mode_t current_mode;

	// data read from an incoming message header
	uint16_t expected_message_length;
	uint16_t message_type;
	
	// handle electrical noise
	bool rx_crc_enforcement_enabled;
	uint8_t sequence_num;
	uint8_t CRC8;
	uint16_t CRC16;
	uint8_t buf_CRC[SERIAL_RX_BUF_SIZE];
	
	// reference to associated hardware UART
	UART_HandleTypeDef* huart;
	
	// message handler
	serial_message_handler_t handler;
} serial_state_t;

typedef struct {
	// time in ms of the last message transmitted
	uint32_t prev_tx_msg_timestamp;
	// minimum time in milliseconds between transmitting a message
	uint32_t min_time_between_tx_msgs_ms; 
} serial_tx_msg_rate;

/**
 * Registers a listener to be called whenever a message is received on the provided HUART.
 * 
 * This may only be called once per HUART. huart_port can be any of the SERIAL_HUART* constants.
 */
void serial_init_listener(serial_huart_port_num_t huart_port, serial_message_handler_t message_handler);

/**
 * Enables CRC noise handling for messages received on the provided HUART.
 * This may only be called once per HUART. huart_port can be any of the SERIAL_HUART* constants.
 */
void serial_enable_rx_crc_enforcement(serial_huart_port_num_t huart_port);

/**
 * Transmits data over the provided UART port.
 * @return true if transmit was successful, false otherwise
 */
bool serial_transmit(serial_huart_port_num_t huart_port,
					 uint16_t message_type,
					 uint16_t length,
					 uint8_t* message_data,
					 void (*success_callback)(void));

/**
 * @return true if the prev_tx_msg_timestamp is more than or equal to the
 *         min_time_between_tx_msgs_ms and sets prev_tx_msg_timestamp to the current time,
 *         false otherwise
 */
bool serial_tx_msg_rate_ready(serial_tx_msg_rate* tx_msg_rate);

#endif
