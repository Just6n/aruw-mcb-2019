#ifndef _ENGINEER_SPECIFIC_TASK_H_
#define _ENGINEER_SPECIFIC_TASK_H_

#include "stm32f4xx_hal.h"

void engineer_specific_task(void const* argu);

#endif //_ENGINEER_SPECIFIC_TASK_H_
