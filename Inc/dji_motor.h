#ifndef _DJI_MOTOR_H_
#define _DJI_MOTOR_H_

#include "stm32f4xx_hal.h"
#include "pid.h"
#include "stdbool.h"

#define ENC_MAX 8192

#define MOTOR_1 0x201
#define MOTOR_2 0x202
#define MOTOR_3 0x203
#define MOTOR_4 0x204
#define MOTOR_5 0x205
#define MOTOR_6 0x206
#define MOTOR_7 0x207
#define MOTOR_8 0x208

#define MOTOR_CURRENT_TO_AMPS (0.00122070312f) // multiplier to convert what motor says is current to the true current in ammps
										       // we get current reading in between -2^14 to 2^14, which maps onto -20 to 20 amps, this value is 20/2^14
typedef struct {
	uint16_t motor_id;
	int16_t voltage_desired; // this is actually VOLUTAGE_DESIRED
	int16_t current_actual;	
	int16_t rpm; // actual wheel axle rpm is this value * gear ratio (~19 for M3508)
	uint16_t encoder_wrapped;
	pid_t pid;
	uint8_t in_use;
	int16_t rotation_count; // number motor rotations from initialization
	int32_t encoder_unwrapped; // absolute encoder_wrapped value from starting encoder_wrapped position, dji_motor_t->encoder_wrapped + dji_motor_t->rotation_count * ENC_MAX
	int16_t encoder_wrapped_prev; // last wrapped encoder value
	bool online; // true if the motor has power and has sent a message back to the MCB
} dji_motor_t;

typedef struct{ 
	
	dji_motor_t dji_motor_store[8];
	
	// data buffers
	uint8_t data_dji_motor_low_tx[8];
	uint8_t data_dji_motor_high_tx[8];
	
	// filter
	CAN_FilterTypeDef can_filter;
	
} can_motor_storage_t;

extern can_motor_storage_t can1;
extern can_motor_storage_t can2;

// freertos thread
void can_send_dji_motor_task(void const * argument);
// System functions
void can_task(void);
// initializes can1 and can2
void can_dji_motor_init(void);
// sets the dji_motor output to the given output
void can_dji_motor_set(dji_motor_t* motor, float output);
// returns a motor in specified can_motor_storage_t
dji_motor_t* get_motor(can_motor_storage_t* can, int motor_number);

#endif // _DJI_MOTOR_H
