/*
 * remote.h
 *
 * This file provides functions declarations and type definitions related to the remote control
 * input that the user uses to drive the robot.
 */
 
#ifndef _REMOTE_H_
#define _REMOTE_H_

#include "stdint.h"
#include "usart.h"
#include "stdbool.h"

#define REMOTE_DR16_PACKET_LEN 18

#define KEY_PRESSED_OFFSET_W              ((uint16_t)0x01<<0)
#define KEY_PRESSED_OFFSET_S              ((uint16_t)0x01<<1)
#define KEY_PRESSED_OFFSET_A              ((uint16_t)0x01<<2)
#define KEY_PRESSED_OFFSET_D              ((uint16_t)0x01<<3)
#define KEY_PRESSED_OFFSET_SHIFT          ((uint16_t)0x01<<4)
#define KEY_PRESSED_OFFSET_CTRL           ((uint16_t)0x01<<5)
#define KEY_PRESSED_OFFSET_Q              ((uint16_t)0x01<<6)
#define KEY_PRESSED_OFFSET_E              ((uint16_t)0x01<<7)
#define KEY_PRESSED_OFFSET_R              ((uint16_t)0x01<<8)
#define KEY_PRESSED_OFFSET_F              ((uint16_t)0x01<<9)
#define KEY_PRESSED_OFFSET_G              ((uint16_t)0x01<<10)
#define KEY_PRESSED_OFFSET_Z              ((uint16_t)0x01<<11)
#define KEY_PRESSED_OFFSET_X              ((uint16_t)0x01<<12)
#define KEY_PRESSED_OFFSET_C              ((uint16_t)0x01<<13)
#define KEY_PRESSED_OFFSET_V              ((uint16_t)0x01<<14)
#define KEY_PRESSED_OFFSET_B              ((uint16_t)0x01<<15)
#define KEY_PRESSED(v, k) ((v) & (k))

typedef struct {
    struct { 
        int16_t ch0;  // right horizontal
        int16_t ch1;  // right vertical
        int16_t ch2;  // left horizontal
        int16_t ch3;  // left vertical
        uint8_t s1;
        uint8_t s2;
    } rc;
		struct {
			struct {
					int16_t x_velocity;
					int16_t y_velocity;
					int16_t z;
					uint8_t press_l;
					uint8_t press_r;
					float average_x_mouse;
					float average_y_mouse;
			} mouse; // Polling Rate of mouse should be 1000Hz
			uint16_t key;
		} computer;
    volatile uint32_t update_counter;
} remote_info_t; // (taken from 2017 code)



typedef struct{
	int16_t key;
	bool key_enabled;
	bool key_toggled;
	enum{
		NOT_PRESSED = 0,
		PRESSED = 1,
		RELEASED = 3,
		PRESSED_TO_UNTOGGLE = 4,
	} key_state;
} toggle_key_t;

// Parses a buffer the size of an remote_info_t into RC.
// Returns 1 on success, 0 on failure
int remote_parse_buf(uint8_t *buf, remote_info_t *RC);
void dr16_idle_handler(UART_HandleTypeDef *huart);
// Enable IDLE interrupts
void remote_uart_it_init(void);
void remote_init(UART_HandleTypeDef *huart);

static void dr16_process_buf(uint8_t start);
const remote_info_t *dr16_get_remote(void);
bool key_toggled(uint16_t key);

void remote_calc_task(void const* argu);
#endif // _REMOTE_H_
