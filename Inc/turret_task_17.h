#ifndef _TURRET_TASK_17_H_
#define _TURRET_TASK_17_H_

#if defined(TARGET_SOLDIER) || defined(TARGET_SENTINEL)

#include "turret_task.h"

void turret_task_17(void const* argu);

#endif 

#endif
