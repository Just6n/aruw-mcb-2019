#ifndef _TURRET_TASK_H_
#define _TURRET_TASK_H_

#if !defined(TARGET_ENGINEER)

#include "stm32f4xx_hal.h"
#include "position_motor_control.h"
#include "operator_interface_bindings.h"
#include "cmsis_os.h"
#include "math_user_utils.h"
#include "math.h"

#define ENC_MAX 8192
#define TURRET_ZERO_ANG (90.0f)

typedef enum {
	#if defined (TARGET_HERO)
	TURRET_42_ID = 0,
	NUMBER_OF_TURRETS = 1, 
	#elif defined (TARGET_SOLDIER) || defined(TARGET_DRONE) || defined(TARGET_SENTINEL)
	TURRET_17_ID = 0,
	NUMBER_OF_TURRETS = 1,
	#endif
} turret_id_t;

motor_control_error_t turret_init(
	turret_id_t turret_id, 
	position_control_motor_id_t yaw_position_control_motor, 
	position_control_motor_id_t pitch_position_control_motor,
	dji_motor_t* yaw_motor, 
	dji_motor_t* pitch_motor,
	int16_t yaw_zero_encoder,
	int16_t pitch_zero_encoder, 
	float motor_smoothing_factor_yaw,
	float motor_smoothing_factor_pitch
);

motor_control_error_t turret_aim_handler(turret_id_t turret_id, float* yaw_ang, float* pitch_ang);
motor_control_error_t turret_run_pid(turret_id_t turret_id);
float turret_get_yaw_ang_setpoint(turret_id_t turret_id);
float turret_get_pitch_ang_setpoint(turret_id_t turret_id);
bool turret_check_id(turret_id_t turret_id);
int16_t turret_get_max_ang_yaw(turret_id_t turret_id);
int16_t turret_get_min_ang_yaw(turret_id_t turret_id);
int16_t turret_get_max_ang_pitch(turret_id_t turret_id);
int16_t turret_get_min_ang_pitch(turret_id_t turret_id);
float turret_get_yaw_ang_actual(turret_id_t turret_id);
float turret_get_pitch_ang_actual(turret_id_t turret_id);
motor_control_error_t turret_set_yaw_motor_ang(turret_id_t turret_id, float ang);
motor_control_error_t turret_set_pitch_motor_ang(turret_id_t turret_id, float ang);

#endif
#endif
