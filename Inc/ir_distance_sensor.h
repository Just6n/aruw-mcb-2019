#ifndef _IR_DISTANCE_SENSOR_H_
#define _IR_DISTANCE_SENSOR_H_

#include "stm32f4xx_hal.h"
typedef enum {
	// SHARP GP2Y0A41SK0F distance sensor voltage conversion to mm [range: 40 - 300mm]
	SHORT_IR_SENSOR = 0,
	// SHARP GP2Y0A60SZLF distance sensor voltage conversion to mm [range: 100 - 1500mm]
	LONG_IR_SENSOR = 1,
} ir_distance_sensor_t;

float ir_get_distance(uint16_t pin, ir_distance_sensor_t ir_sensor);
#endif
