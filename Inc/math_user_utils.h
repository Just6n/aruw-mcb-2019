#ifndef _MATH_USER_UTILS_H_
#define _MATH_USER_UTILS_H_

#include "stm32f4xx_hal.h"

typedef struct {
	float *data;
	size_t size;
	size_t head;
} circular_buffer_t;

#define PI (3.14159265359f)
#define MINUTES_PER_SECOND (1.0f / 60.0f)
#define MV_TO_V_SCALER (0.001f) // scaler for millivolts to volts

#define RAD_TO_DEG(ang) ((ang) * 180.0f / PI)
#define DEG_TO_RAD(ang) ((ang) * PI / 180.0f)
#define RAND(min, max) ((rand() % ((int) (max * 100) + 1 - (int) (min * 100)) + (int) (min * 100)) / 100.0f)

// documentation in .c file 

void rotation_matrix_2d(float* x, float* y, float angle);

int8_t sign(float num);

float limit_float(float val, float min, float max);

/**
 * Initializes a circular buffer and fills it with the provided initial_value.
 */
void circular_buffer_init(circular_buffer_t *buffer, float initial_value);

/**
 * Sets the next index in the circular buffer to the provided value, overwriting
 * the old value.
 */
void circular_buffer_append(circular_buffer_t *buffer, float new_value);

/*
 * Calculates and returns the arithmetic mean of the values in the circular
 * buffer.
 */
float circular_buffer_mean(circular_buffer_t *buffer);

float low_pass_filter(float prev_value, float new_value, float alpha);

float find_shortest_wrapped_distance(float upper_bound, float lower_bound, float val1, float val2);	

float wrap_around_angle(float angle_to_wrap, float angle_to_wrap_around);

float limit_float(float val, float min, float max);

#endif
