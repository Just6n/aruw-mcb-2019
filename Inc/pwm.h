#ifndef __PWM_H__
#define __PWM_H__

#include "tim.h"

void PWM_SetTimerChannel(TIM_HandleTypeDef *tim,uint32_t tim_channel,float duty);

/* Sets PWM duty cycle for a pin
 * Pre: pins are between PIN_A - PIN_H, PIN_S - PIN_Z, 0.0f <= duty <= 1.0f
 * Duty Cycle is in float form i.e 0.50f = 50% duty cycle
 * Assigns Pins to a timer and a channel, then calls PWM_SetTimerChannel with the timer, channel and given duty
 */
void PWM_SetDuty(uint16_t pin, float duty);
#endif
