/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */

// prescaler = (5000mV)/(4086) <- 12 bits
#define ANALOG_PRESCALER     (1.223690651);
     
// The values are arbitrary numbers to distinguish the pins
#define PIN_I1      ((uint16_t)0x0001)  // Digital output
#define PIN_I2		((uint16_t)0x0002)  // Digital output
#define PIN_J1		((uint16_t)0x0003)  // Digital output
#define PIN_J2		((uint16_t)0x0004)  // Digital output
#define PIN_K1	    ((uint16_t)0x0005)  // Digital output
#define PIN_K2		((uint16_t)0x0006)  // Digital output
#define PIN_L1		((uint16_t)0x0007)  // Digital intput
#define PIN_L2		((uint16_t)0x0008)  // Digital input
#define PIN_M1	    ((uint16_t)0x0009)  // Analog input
#define PIN_M2		((uint16_t)0x000A)  // Analog input
#define PIN_N1		((uint16_t)0x000B)  // Analog input
#define PIN_N2		((uint16_t)0x000C)  // Analog input
#define PIN_O1		((uint16_t)0x000D)  // Analog input
#define PIN_O2		((uint16_t)0x000E)  // Analog input
#define PIN_P1		((uint16_t)0x000F)  // Analog output
#define PIN_P2		((uint16_t)0x0010)  // Analog output
#define PIN_Q1		((uint16_t)0x0011)  // Digital input
#define PIN_Q2		((uint16_t)0x0012)  // Digital input
#define PIN_A		((uint16_t)0x0013)  // PWM output
#define PIN_B		((uint16_t)0x0014)  // PWM output
#define PIN_C	    ((uint16_t)0x0015)  // PWM output
#define PIN_D		((uint16_t)0x0016)  // PWM output
#define PIN_E		((uint16_t)0x0017)  // PWM output
#define PIN_F		((uint16_t)0x0018)  // PWM output
#define PIN_G		((uint16_t)0x0019)  // PWM output
#define PIN_H		((uint16_t)0x001A)  // PWM output
#define PIN_S		((uint16_t)0x001B)  // PWM output
#define PIN_T		((uint16_t)0x001C)  // PWM output
#define PIN_U		((uint16_t)0x001D)  // PWM output
#define PIN_V		((uint16_t)0x001E)  // PWM output
#define PIN_W		((uint16_t)0x001F)  // PWM output
#define PIN_X		((uint16_t)0x0020)  // PWM output
#define PIN_Y		((uint16_t)0x0021)  // PWM output
#define PIN_Z		((uint16_t)0x0022)  // PWM output
/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */

// Takes in pin labels, and pin state (GPIO_PIN_RESET or GPIO_PIN_SET) then sets the
// Digital output pin to the Pinstate value.
void GPIO_Digital_Write(uint16_t pin, GPIO_PinState PinState);

// Takes in pin labels and toggles the state of the pin
void GPIO_Digital_Toggle(uint16_t pin);

// takes in given pin and returns the bit status of the pin
GPIO_PinState GPIO_Digital_Read(uint16_t pin);

//takes in a given analog input pin and returns the voltage in (mV) given as a float
float GPIO_Analog_Read(uint16_t pin);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
