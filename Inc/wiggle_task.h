#ifndef _WIGGLE_TASK_H_
#define _WIGGLE_TASK_H_

#include "turret_task.h"
#include "imu_task.h"
#include "drive_task.h"

#if defined (TARGET_SOLDIER) || defined (TARGET_HERO)

#define get_wig_direction(state) (int16_t)((int16_t)(state) - 1)

void chassis_lock_handler(void);
// function that rotates the turret yaw motor based on how chassis is wiggling
float turret_wiggle_handler(uint16_t id, float user_ang_desired_changed, float yaw_ang_desired);
// function that keeps track of chassis angle and handles other wiggle calculations
void wiggle_init_turret(void);
// function that rotates the chassis based on the angles calculated in chassis_lock_handler
void chassis_wiggle_handler(float chassis_desired_wheel_rpm_rotation);

motor_control_error_t turret_enable_wiggle_add(uint16_t id);

void wiggle_turret_lock(uint16_t id, bool is_locked);

void set_chassis_wiggling(bool is_wiggling);

void start_turret_lock(uint16_t id);

void end_turret_lock(uint16_t id);

float get_wiggle_drive_speed_factor(void);

float calc_chassis_rel_init(turret_id_t turret_id);

#endif 

#endif
