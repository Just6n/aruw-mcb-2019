#ifndef _cv_comms_h_
#define _cv_comms_h_

#include "stdbool.h"
#include "stdint.h"

// TX message headers
// CV_MESSAGE_TYPEs for transmission should be defined incrementally from 0x01
#define CV_MESSAGE_TYPE_IMU 0x02
#define CV_MESSAGE_TYPE_ROBOT_ID 0x04
#define CV_MESSAGE_TYPE_AUTO_AIM_REQUEST 0x05
#if !defined (TARGET_ENGINEER)
#define CV_MESSAGE_TYPE_TURRET_TELEMETRY 0x01
#else
#define CV_MESSAGE_TYPE_REQUEST_TASK 0x03
#endif
#define CV_MESSAGE_TYPE_SIZE 4

// RX message headers
#define TIME_OFFLINE_CV_AIM_DATA_MS (5000) // time in ms since last CV aim data was received before deciding CV is offline
#if !defined (TARGET_ENGINEER)
#define CV_MESSAGE_TYPE_TURRET_AIM 0x01
#else
#define CV_MESSAGE_TYPE_ALIGN_CONTROL 0x02
#define CV_MESSAGE_TYPE_ALIGN_COMPLETE 0x03
#endif

// Engineer task types
#if defined (TARGET_ENGINEER)
#define CV_TASK_TYPE_TOWING 0x01
#define CV_TASK_TYPE_ONE_AMMO_BOX 0x02
#define CV_TASK_TYPE_ALL_AMMO_BOX 0x03
#endif

// Structs and public functions
#if !defined (TARGET_ENGINEER)
typedef struct {
	bool has_target;
	float pitch;
	float yaw;
	uint32_t receive_timestamp;
} cv_comms_turret_aim_data_t;

bool cv_comms_get_last_aim_data(cv_comms_turret_aim_data_t* out);
void cv_comms_begin_tracking_target(void);
void cv_comms_stop_tracking_target(void);
#else
typedef struct {
	int16_t x;
	int16_t y;
	int16_t r;
} cv_comms_align_data_t;

bool cv_comms_get_last_control(cv_comms_align_data_t* out);
bool cv_comms_is_align_complete(void);
void cv_comms_new_task_request(char task);
#endif

void cv_comms_task(void const* argu);

#endif
