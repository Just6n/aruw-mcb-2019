#ifndef _DRIVE_TASK_H_
#define _DRIVE_TASK_H_

#include "stm32f4xx_hal.h"
#include "dji_motor.h"

#if defined (TARGET_SOLDIER) || defined (TARGET_HERO)

// chassis power limit defines
#if defined(TARGET_SOLDIER)	// soldier specific chassis power constraints
#define POWER_LIMIT 80.0f	// power limit in Watts
#define MAX_ENERGY_BUFF 60.0f	// max energy that is stored in the buffer, in joules
#elif defined (TARGET_HERO)
#define POWER_LIMIT 80.0f // hero has same power consumption as soldier
#define MAX_ENERGY_BUFF 60.0f
#endif

void drive_task(void const* argu);

// initialize drive_motor_t 
void drive_init(void);
// initialize pid values
void drive_pid_init(dji_motor_t *motor);

bool get_chassis_user_rotating(void); // return if the user is rotating the chassis or not
float chassis_get_rotation_rpm(void); // return chassis rpm desired that we are outputting to the motors
void chassis_set_rotate_ang(float ang);
void chassis_rotate_angle(void); // rotates the chassis to the specified angle. If the angle being sent is changed, a new angle will be set
void reset_chassis_rotation_pid_out(void);
#endif

#endif //_DRIVE_TASK_H__

