#ifndef _FIRING_TASK_17_H_
#define _FIRING_TASK_17_H_

#if !defined(TARGET_ENGINEER)

#include "agitator_task.h"
#include "cmsis_os.h"
#include "gpio.h"
#include "pwm.h"
#include "position_motor_control.h"
#include "operator_interface_bindings.h"

void firing_task_17(void const* argu);

#endif

#endif
