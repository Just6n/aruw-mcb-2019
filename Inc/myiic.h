#include "stm32f4xx_hal.h"

void IIC_Delay(void);
//初始化IIC
void IIC_Init(void);
//产生IIC起始信号
void IIC_Start(void);
//产生IIC停止信号
void IIC_Stop(void);
//等待应答信号到来
//返回值：1，接收应答失败
//        0，接收应答成功
uint8_t IIC_Wait_Ack(void);
//产生ACK应答
void IIC_Ack(void);
//不产生ACK应答
void IIC_NAck(void);
//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答
void IIC_Send_Byte(uint8_t txd);
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK
uint8_t IIC_Read_Byte(unsigned char ack);

void IIC_Init(void);
//iic读写寄存器
uint8_t IIC_Write_Reg(uint8_t devAddr, uint8_t reg, uint8_t data);
uint8_t IIC_Read_Reg(uint8_t devAddr, uint8_t reg);

//从制定的器件寄存器地址连续读len个字节的数据。相当于reg， reg+1， 。。。，reg+len
uint8_t IIC_Write_Bytes(uint8_t devAddr, uint8_t reg, uint8_t* buff, uint8_t len);
uint8_t IIC_Read_Bytes(uint8_t devAddr, uint8_t reg, uint8_t* buff, uint8_t len);

//for inv_dmp.c call use
uint8_t IIC_Read(uint8_t addr, uint8_t reg, uint8_t len, uint8_t* buf);
uint8_t IIC_Write(uint8_t addr, uint8_t reg, uint8_t len, uint8_t* buf);
